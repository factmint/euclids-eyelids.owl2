package com.factmint.graphhub.sail.owl2sail.rules;

import static com.factmint.graphhub.sail.owl2sail.sesame.utility.Constants.SYMMETRIC_PROPERTY_RULE;
import info.aduna.iteration.CloseableIteration;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.openrdf.model.Resource;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.sail.SailException;
import org.openrdf.sail.inferencer.InferencerConnection;

public class SymmetricPropertyRule extends Owl2Rule {

	public SymmetricPropertyRule(InferencerConnection connection, Map<String, List<String>> activeRulesDependencies) {
		super(connection, activeRulesDependencies);
	}

	@Override
	public Collection<String> apply(Set<Statement> addedStatementsThisIteration, Set<Statement> removedStatementsThisIteration, Set<String> rulesToRunThisIteration) throws SailException {
		Collection<String> rulesThatMadeChanges = new ArrayList<String>();
		
		if (rulesToRunThisIteration.contains(SYMMETRIC_PROPERTY_RULE)) {
			int count = symmetricPropertyRule(addedStatementsThisIteration, removedStatementsThisIteration);
			logger.debug("{} statement changes after applying {}.", count, SYMMETRIC_PROPERTY_RULE);
			if (count > 0) {
				rulesThatMadeChanges.add(SYMMETRIC_PROPERTY_RULE);
			}
		}
		
		return buildRulesToRunNextIteration(rulesThatMadeChanges);
	}

	private int symmetricPropertyRule(Set<Statement> addedStatementsThisIteration, Set<Statement> removedStatementsThisIteration) throws SailException {
		int count = 0;
		if (addedStatementsThisIteration != null) {
			count += applyOnAdded_1(addedStatementsThisIteration);
			count += applyOnAdded_2(addedStatementsThisIteration);
		}
		if (removedStatementsThisIteration != null) {
			count += applyOnRemoved_1(removedStatementsThisIteration);
			count += applyOnRemoved_2(removedStatementsThisIteration);
		}
		return count;
	}
	
	// adding: (s, p, o)
	// if existing: (p, rdf:type, owl:SymmetricProperty)
	// then inferring: (o, p, s)
	private int applyOnAdded_1(Set<Statement> addedStatementsThisIteration) throws SailException {
		Set<Statement> inferredStatements = new HashSet<Statement>();

		for (Statement addedStatement : addedStatementsThisIteration) {
			if (! hasSymmetricPropertyType(addedStatement.getPredicate()) || ! isResource(addedStatement.getObject())) {
				continue;
			}
			
			Resource objectAsUri = (Resource) addedStatement.getObject();
			Statement inferredStatement = valueFactory.createStatement(objectAsUri, addedStatement.getPredicate(), addedStatement.getSubject(), addedStatement.getContext());
			inferredStatements.add(inferredStatement);
		}

		return addInferred(inferredStatements, SYMMETRIC_PROPERTY_RULE);
	}	

	// adding: (p, rdf:type, owl:SymmetricProperty)
	// if existing: (s, p, o)
	// then inferring: (o, p, s)
	private int applyOnAdded_2(Set<Statement> addedStatementsThisIteration) throws SailException {
		Set<Statement> inferredStatements = new HashSet<Statement>();

		for (Statement addedStatement : addedStatementsThisIteration) {
			if (! isSymmetricPropertyStatement(addedStatement) || ! isURI(addedStatement.getSubject())) {
				continue;
			}
			
			CloseableIteration<? extends Statement, SailException> existingStatements = connection.getStatements(null, (URI) addedStatement.getSubject(), null, true);
			
			while (existingStatements.hasNext()) {
				Statement existingStatement = existingStatements.next();					
				if (! isResource(existingStatement.getObject())) {
					continue;
				}
				
				Resource objectAsUri = (Resource) existingStatement.getObject();
				Statement inferredStatement = valueFactory.createStatement(objectAsUri, existingStatement.getPredicate(), existingStatement.getSubject(), existingStatement.getContext());
				inferredStatements.add(inferredStatement);
			}
		}

		return addInferred(inferredStatements, SYMMETRIC_PROPERTY_RULE);
	}

	// removing: (s, p, o)
	// if existing: (p, rdf:type, owl:SymmetricProperty)
	// then removing inferred: (o, p, s)
	private int applyOnRemoved_1(Set<Statement> removedStatementsThisIteration) throws SailException {
		Set<Statement> inferredStatements = new HashSet<Statement>();

		for (Statement removedStatement : removedStatementsThisIteration) {
			if (! hasSymmetricPropertyType(removedStatement.getPredicate()) || ! isResource(removedStatement.getObject())) {
				continue;
			}
			
			Resource objectAsUri = (Resource) removedStatement.getObject();
			Statement inferredStatement = valueFactory.createStatement(objectAsUri, removedStatement.getPredicate(), removedStatement.getSubject(), removedStatement.getContext());
			inferredStatements.add(inferredStatement);
		}

		return removeInferred(inferredStatements, SYMMETRIC_PROPERTY_RULE);
	}

	// removing: (p, rdf:type, owl:SymmetricProperty)
	// if existing: (s, p, o)
	// then removing inferred: (o, p, s)
	private int applyOnRemoved_2(Set<Statement> removedStatementsThisIteration) throws SailException {
		Set<Statement> inferredStatements = new HashSet<Statement>();

		for (Statement removedStatement : removedStatementsThisIteration) {
			if (! isSymmetricPropertyStatement(removedStatement) || ! isURI(removedStatement.getSubject())) {
				continue;
			}
			CloseableIteration<? extends Statement, SailException> existingStatements = connection.getStatements(null, (URI) removedStatement.getSubject(), null, true);
			
			while (existingStatements.hasNext()) {
				Statement existingStatement = existingStatements.next();					
				if (! isResource(existingStatement.getObject())) {
					continue;
				}
				
				Resource objectAsUri = (Resource) existingStatement.getObject();
				Statement inferredStatement = valueFactory.createStatement(objectAsUri, existingStatement.getPredicate(), existingStatement.getSubject(), existingStatement.getContext());
				inferredStatements.add(inferredStatement);
			}
		}

		return removeInferred(inferredStatements, SYMMETRIC_PROPERTY_RULE);
	}

}
