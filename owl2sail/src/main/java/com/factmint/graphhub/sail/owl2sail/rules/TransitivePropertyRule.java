package com.factmint.graphhub.sail.owl2sail.rules;

import static com.factmint.graphhub.sail.owl2sail.sesame.utility.Constants.TRANSITIVE_PROPERTY_RULE;
import info.aduna.iteration.CloseableIteration;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.openrdf.model.Resource;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.Value;
import org.openrdf.sail.SailException;
import org.openrdf.sail.inferencer.InferencerConnection;

public class TransitivePropertyRule extends Owl2Rule {

	public TransitivePropertyRule(InferencerConnection connection, Map<String, List<String>> activeRulesDependencies) {
		super(connection, activeRulesDependencies);
	}
	
	@Override
	public Collection<String> apply(Set<Statement> addedStatementsThisIteration, Set<Statement> removedStatementsThisIteration, Set<String> rulesToRunThisIteration) throws SailException {
		Collection<String> rulesThatMadeChanges = new ArrayList<String>();
		
		if (rulesToRunThisIteration.contains(TRANSITIVE_PROPERTY_RULE)) {
			int count = transitivePropertyRule(addedStatementsThisIteration, removedStatementsThisIteration);
			logger.debug("{} statement changes after applying {}.", count, TRANSITIVE_PROPERTY_RULE);
			if (count > 0) {
				rulesThatMadeChanges.add(TRANSITIVE_PROPERTY_RULE);
			}
		}
		
		return buildRulesToRunNextIteration(rulesThatMadeChanges);
	}

	private int transitivePropertyRule(Set<Statement> addedStatementsThisIteration,	Set<Statement> removedStatementsThisIteration) throws SailException {
		int count = 0;
		if (addedStatementsThisIteration != null) {
			count += applyOnAdded_1(addedStatementsThisIteration);
			count += applyOnAdded_2(addedStatementsThisIteration);
			count += applyOnAdded_3(addedStatementsThisIteration);
		}
		if (removedStatementsThisIteration != null) {
			count += applyOnRemoved_1(removedStatementsThisIteration);
			count += applyOnRemoved_2(removedStatementsThisIteration);
			count += applyOnRemoved_3(removedStatementsThisIteration);
		}
		return count;
	}

	// adding: (p, rdf:type, owl:TransitiveProperty)
	// if existing: {(s, p, s'), (s', p, s'')}
	// then inferring: (s, p, s'')
	private int applyOnAdded_1(Set<Statement> addedStatementsThisIteration) throws SailException {
		Set<Statement> inferredStatements = new HashSet<Statement>();

		for (Statement addedStatement : addedStatementsThisIteration) {
			if (isTransitivePropertyStatement(addedStatement)
					&& isURI(addedStatement.getSubject())) {
				
				URI transitiveProperty = (URI) addedStatement.getSubject();
				
				CloseableIteration<? extends Statement, SailException> existingStatements = connection.getStatements(null, transitiveProperty, null, true);
				
				while (existingStatements.hasNext()) {
					Statement existingStatement = existingStatements.next();
					
					if (! isResource(existingStatement.getObject())) {
						continue;
					}
					
					Resource transitiveStatementSubject = existingStatement.getSubject();
					
					CloseableIteration<? extends Statement, SailException> existingStatementsWithObjectAsSubject = connection.getStatements((Resource) existingStatement.getObject(), transitiveProperty, null, true);
				
					while (existingStatementsWithObjectAsSubject.hasNext()) {
						Statement existingStatementWithObjectAsSubject = existingStatementsWithObjectAsSubject.next();						
						Value transitiveStatementObject = existingStatementWithObjectAsSubject.getObject();						
						Statement inferredStatement = valueFactory.createStatement(transitiveStatementSubject, transitiveProperty, transitiveStatementObject);
						inferredStatements.add(inferredStatement);
					}
				}
			}
		}

		return addInferred(inferredStatements, TRANSITIVE_PROPERTY_RULE);
	}
	
	// adding: (s, p, s')
	// if existing: {(p, rdf:type, owl:TransitiveProperty), (s', p, s'')}
	// then inferring: (s, p, s'')
	private int applyOnAdded_2(Set<Statement> addedStatementsThisIteration) throws SailException {
		Set<Statement> inferredStatements = new HashSet<Statement>();

		for (Statement addedStatement : addedStatementsThisIteration) {
			if (! isTransitivePropertyStatement(addedStatement)
					&& hasTransitivePropertyType(addedStatement.getPredicate())
					&& isResource(addedStatement.getObject())) {
				
				Resource transitiveStatementSubject = addedStatement.getSubject();
				URI transitiveProperty = addedStatement.getPredicate();
				
				CloseableIteration<? extends Statement, SailException> existingStatementsWithObjectAsSubject = connection.getStatements((Resource) addedStatement.getObject(), transitiveProperty, null, true);
				
				while (existingStatementsWithObjectAsSubject.hasNext()) {
					Statement existingStatementWithObjectAsSubject = existingStatementsWithObjectAsSubject.next();	
					if (existingStatementWithObjectAsSubject.equals(addedStatement)) {
						continue;
					}
					
					Value transitiveStatementObject = existingStatementWithObjectAsSubject.getObject();						
					Statement inferredStatement = valueFactory.createStatement(transitiveStatementSubject, transitiveProperty, transitiveStatementObject);
					inferredStatements.add(inferredStatement);
				}
			}
		}

		return addInferred(inferredStatements, TRANSITIVE_PROPERTY_RULE);
	}
	
	// adding: (s', p, s'')
	// if existing: {(p, rdf:type, owl:TransitiveProperty), (s, p, s')}
	// then inferring: (s, p, s'')
	private int applyOnAdded_3(Set<Statement> addedStatementsThisIteration) throws SailException {
		Set<Statement> inferredStatements = new HashSet<Statement>();

		for (Statement addedStatement : addedStatementsThisIteration) {
			if (! isTransitivePropertyStatement(addedStatement)
					&& hasTransitivePropertyType(addedStatement.getPredicate())) {
				
				URI transitiveProperty = addedStatement.getPredicate();
				Value transitiveStatementObject = addedStatement.getObject();
				
				CloseableIteration<? extends Statement, SailException> existingStatementsWithSubjectAsObject = connection.getStatements(null, transitiveProperty, addedStatement.getSubject(), true);
				
				while (existingStatementsWithSubjectAsObject.hasNext()) {
					Statement existingStatementWithSubjectAsObject = existingStatementsWithSubjectAsObject.next();	
					if (existingStatementWithSubjectAsObject.equals(addedStatement)) {
						continue;
					}
					
					Resource transitiveStatementSubject = existingStatementWithSubjectAsObject.getSubject();						
					Statement inferredStatement = valueFactory.createStatement(transitiveStatementSubject, transitiveProperty, transitiveStatementObject);
					inferredStatements.add(inferredStatement);
				}
			}
		}

		return addInferred(inferredStatements, TRANSITIVE_PROPERTY_RULE);
	}
	
	// removing: (p, rdf:type, owl:TransitiveProperty)
	// if existing: {(s, p, s'), (s', p, s'')}
	// then removing inferred: (s, p, s'')
	private int applyOnRemoved_1(Set<Statement> removedStatementsThisIteration) throws SailException {
		Set<Statement> inferredStatements = new HashSet<Statement>();

		for (Statement removedStatement : removedStatementsThisIteration) {
			if (isTransitivePropertyStatement(removedStatement)
					&& isURI(removedStatement.getSubject())) {
				URI transitiveProperty = (URI) removedStatement.getSubject();
				
				CloseableIteration<? extends Statement, SailException> existingStatements = connection.getStatements(null, transitiveProperty, null, true);
				
				while (existingStatements.hasNext()) {
					Statement existingStatement = existingStatements.next();
					
					if (! isResource(existingStatement.getObject())) {
						continue;
					}
					
					Resource transitiveStatementSubject = existingStatement.getSubject();
					
					CloseableIteration<? extends Statement, SailException> existingStatementsWithObjectAsSubject = connection.getStatements((Resource) existingStatement.getObject(), transitiveProperty, null, true);
				
					while (existingStatementsWithObjectAsSubject.hasNext()) {
						Statement existingStatementWithObjectAsSubject = existingStatementsWithObjectAsSubject.next();
						Value transitiveStatementObject = existingStatementWithObjectAsSubject.getObject();						
						Statement inferredStatement = valueFactory.createStatement(transitiveStatementSubject, transitiveProperty, transitiveStatementObject);
						inferredStatements.add(inferredStatement);
					}
				}
			}
		}

		return removeInferred(inferredStatements, TRANSITIVE_PROPERTY_RULE);
	}
	
	// removing: (s, p, s')
	// if existing: {(p, rdf:type, owl:TransitiveProperty), (s', p, s'')}
	// then removing inferred: (s, p, s'')
	private int applyOnRemoved_2(Set<Statement> removedStatementsThisIteration) throws SailException {
		Set<Statement> inferredStatements = new HashSet<Statement>();

		for (Statement removedStatement : removedStatementsThisIteration) {
			if (! isTransitivePropertyStatement(removedStatement)
					&& hasTransitivePropertyType(removedStatement.getPredicate())
					&& isResource(removedStatement.getObject())) {
				
				Resource transitiveStatementSubject = removedStatement.getSubject();
				URI transitiveProperty = removedStatement.getPredicate();
				
				CloseableIteration<? extends Statement, SailException> existingStatementsWithObjectAsSubject = connection.getStatements((Resource) removedStatement.getObject(), transitiveProperty, null, true);
				
				while (existingStatementsWithObjectAsSubject.hasNext()) {
					Statement existingStatementWithObjectAsSubject = existingStatementsWithObjectAsSubject.next();			
					if (existingStatementWithObjectAsSubject.equals(removedStatement)) {
						continue;
					}
					
					Value transitiveStatementObject = existingStatementWithObjectAsSubject.getObject();						
					Statement inferredStatement = valueFactory.createStatement(transitiveStatementSubject, transitiveProperty, transitiveStatementObject);
					inferredStatements.add(inferredStatement);
				}
			}
		}

		return removeInferred(inferredStatements, TRANSITIVE_PROPERTY_RULE);
	}
	
	// removing: (s, p, s')
	// if existing: {(p, rdf:type, owl:TransitiveProperty), (s', p, s'')}
	// then removing inferred: (s, p, s'')
	private int applyOnRemoved_3(Set<Statement> removedStatementsThisIteration) throws SailException {
		Set<Statement> inferredStatements = new HashSet<Statement>();

		for (Statement removedStatement : removedStatementsThisIteration) {
			if (! isTransitivePropertyStatement(removedStatement)
					&& hasTransitivePropertyType(removedStatement.getPredicate())) {
				
				Value transitiveStatementObject = removedStatement.getObject();
				URI transitiveProperty = removedStatement.getPredicate();
				
				CloseableIteration<? extends Statement, SailException> existingStatementsWithSubjectAsObject = connection.getStatements(null, transitiveProperty, removedStatement.getSubject(), true);
				
				while (existingStatementsWithSubjectAsObject.hasNext()) {
					Statement existingStatementWithSubjectAsObject = existingStatementsWithSubjectAsObject.next();	
					if (existingStatementWithSubjectAsObject.equals(removedStatement)) {
						continue;
					}
					
					Resource transitiveStatementSubject = existingStatementWithSubjectAsObject.getSubject();						
					Statement inferredStatement = valueFactory.createStatement(transitiveStatementSubject, transitiveProperty, transitiveStatementObject);
					inferredStatements.add(inferredStatement);
				}
			}
		}

		return removeInferred(inferredStatements, TRANSITIVE_PROPERTY_RULE);
	}
	
}
