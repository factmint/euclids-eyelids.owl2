package com.factmint.graphhub.sail.owl2sail.configuration;

public class DomainRuleConfiguration {

	private boolean applyDomainRule = true;
	
	public DomainRuleConfiguration() {
	}
	
	public DomainRuleConfiguration(boolean applyDomainRule) {
		this.applyDomainRule = applyDomainRule;
	}

	public boolean isApplyDomainRule() {
		return applyDomainRule;
	}

	public void setApplyDomainRule(boolean applyDomainRule) {
		this.applyDomainRule = applyDomainRule;
	}
	
}
