package com.factmint.graphhub.sail.owl2sail.rules;

import static com.factmint.graphhub.sail.owl2sail.sesame.utility.Constants.RDF_PROPERTY_RULE;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.openrdf.model.Resource;
import org.openrdf.model.Statement;
import org.openrdf.model.vocabulary.RDF;
import org.openrdf.sail.SailException;
import org.openrdf.sail.inferencer.InferencerConnection;

public class RdfPropertyRule extends Owl2Rule {

	public RdfPropertyRule(InferencerConnection connection,	Map<String, List<String>> activeRulesDependencies) {
		super(connection, activeRulesDependencies);
	}

	@Override
	public Collection<String> apply(Set<Statement> addedStatementsThisIteration, Set<Statement> removedStatementsThisIteration,	Set<String> rulesToRunThisIteration) throws SailException {
		Collection<String> rulesThatMadeChanges = new ArrayList<String>();
		
		if (rulesToRunThisIteration.contains(RDF_PROPERTY_RULE)) {
			int count = rdfPropertyRule(addedStatementsThisIteration, removedStatementsThisIteration);
			logger.debug("{} statement changes after applying {}.", count, RDF_PROPERTY_RULE);
			if (count > 0) {
				rulesThatMadeChanges.add(RDF_PROPERTY_RULE);
			}
		}
		
		return buildRulesToRunNextIteration(rulesThatMadeChanges);
	}

	private int rdfPropertyRule(Set<Statement> addedStatementsThisIteration, Set<Statement> removedStatementsThisIteration) throws SailException {
		int count = 0;
		if (addedStatementsThisIteration != null) {
			count += rdfPropertyRuleOnAdded(addedStatementsThisIteration);
		}
		if (removedStatementsThisIteration != null) {
			count += rdfPropertyRuleOnRemoved(removedStatementsThisIteration);
		}
		return count;
	}
	
	// adding: (s, p, o)
	// then inferring: (p, rdf:type, rdf:Property)
	private int rdfPropertyRuleOnAdded(Set<Statement> addedStatementsThisIteration) throws SailException {
		Set<Statement> inferredStatements = new HashSet<Statement>();

		for (Statement addedStatement : addedStatementsThisIteration) {
			Statement inferredStatement = valueFactory.createStatement((Resource) addedStatement.getPredicate(), RDF.TYPE, RDF.PROPERTY, addedStatement.getContext());
			inferredStatements.add(inferredStatement);
		}

		return addInferred(inferredStatements, RDF_PROPERTY_RULE);
	}

	// removing: (s, p, o)
	// if not existing told: (s', p, o')
	// then removing inferred: (p, rdf:type, rdf:Property)
	private int rdfPropertyRuleOnRemoved(Set<Statement> removedStatementsThisIteration) throws SailException {
		Set<Statement> inferredStatements = new HashSet<Statement>();

		for (Statement removedStatement : removedStatementsThisIteration) {
			if (! hasStatement(null, removedStatement.getPredicate(), null, false)) {
				Statement inferredStatement = valueFactory.createStatement((Resource) removedStatement.getPredicate(), RDF.TYPE, RDF.PROPERTY, removedStatement.getContext());
				inferredStatements.add(inferredStatement);
			}
		}

		return removeInferred(inferredStatements, RDF_PROPERTY_RULE);
	}

}
