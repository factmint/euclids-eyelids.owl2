package com.factmint.graphhub.sail.owl2sail.configuration;

public class RangeRuleConfiguration {
	
	private boolean applyRangeRule = true;
	
	public RangeRuleConfiguration() {
	}
	
	public RangeRuleConfiguration(boolean applyRangeRule) {
		this.setApplyRangeRule(applyRangeRule);
	}

	public boolean isApplyRangeRule() {
		return applyRangeRule;
	}

	public void setApplyRangeRule(boolean applyRangeRule) {
		this.applyRangeRule = applyRangeRule;
	}
	
}
