package com.factmint.graphhub.sail.owl2sail.configuration;

public class ReflexivePropertyRuleConfiguration {

	private boolean applyReflexivePropertyRule = false;
	
	public ReflexivePropertyRuleConfiguration() {
	}
	
	public ReflexivePropertyRuleConfiguration(boolean applyReflexivePropertyRule) {
		this.applyReflexivePropertyRule = applyReflexivePropertyRule;
	}

	public boolean isApplyReflexivePropertyRule() {
		return applyReflexivePropertyRule;
	}

	public void setApplyReflexivePropertyRule(boolean applyReflexivePropertyRule) {
		this.applyReflexivePropertyRule = applyReflexivePropertyRule;
	}
	
}
