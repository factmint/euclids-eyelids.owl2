package com.factmint.graphhub.sail.owl2sail.exception;

public class ComplementOfRuleException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1368540527137583727L;

	public ComplementOfRuleException(Exception e) {
		super(e);
	}
	
	public ComplementOfRuleException(String message) {
		super(message);
	}
	
}
