package com.factmint.graphhub.sail.owl2sail.rules;

import static com.factmint.graphhub.sail.owl2sail.sesame.utility.Constants.REFLEXIVE_PROPERTY_RULE;
import info.aduna.iteration.CloseableIteration;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.openrdf.model.Resource;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.Value;
import org.openrdf.model.vocabulary.RDF;
import org.openrdf.sail.SailException;
import org.openrdf.sail.inferencer.InferencerConnection;

import com.factmint.graphhub.sail.owl2sail.axioms.OWL2;
import com.factmint.graphhub.sail.owl2sail.axioms.OWL2AxiomaticStatements;
import com.factmint.graphhub.sail.owl2sail.axioms.RDFSAxiomaticStatements;

public class ReflexivePropertyRule extends Owl2Rule {

	public ReflexivePropertyRule(InferencerConnection connection, Map<String, List<String>> activeRulesDependencies) {
		super(connection, activeRulesDependencies);
	}

	@Override
	public Collection<String> apply(Set<Statement> addedStatementsThisIteration, Set<Statement> removedStatementsThisIteration,	Set<String> rulesToRunThisIteration) throws SailException {
		Collection<String> rulesThatMadeChanges = new ArrayList<String>();
		
		if (rulesToRunThisIteration.contains(REFLEXIVE_PROPERTY_RULE)) {
			int count = reflexivePropertyRule(addedStatementsThisIteration, removedStatementsThisIteration);
			logger.debug("{} statement changes after applying {}.", count, REFLEXIVE_PROPERTY_RULE);
			if (count > 0) {
				rulesThatMadeChanges.add(REFLEXIVE_PROPERTY_RULE);
			}
		}
		
		return buildRulesToRunNextIteration(rulesThatMadeChanges);
	}

	private int reflexivePropertyRule(Set<Statement> addedStatementsThisIteration, Set<Statement> removedStatementsThisIteration) throws SailException {
		int count = 0;
		if (addedStatementsThisIteration != null) {
			count += applyOnAdded_1(addedStatementsThisIteration);
			count += applyOnAdded_2(addedStatementsThisIteration);
		}
		if (removedStatementsThisIteration != null) {
			count += applyOnRemoved_1(removedStatementsThisIteration);
			count += applyOnRemoved_2(removedStatementsThisIteration);
		}
		return count;
	}

	// adding: (s, p, o)
	// if existing: (r, rdf:type, owl:ReflexiveProperty)
	// then inferring: {(s, r, s), (p, r, p), (o, r, o)}
	private int applyOnAdded_1(Set<Statement> addedStatementsThisIteration) throws SailException {
		Set<Statement> inferredStatements = new HashSet<Statement>();

		List<Statement> reflexivePropertyStatements = null;
		
		for (Statement addedStatement : addedStatementsThisIteration) {
			if (reflexivePropertyStatements == null) {
				reflexivePropertyStatements = new ArrayList<Statement>();
				
				CloseableIteration<? extends Statement, SailException> reflexivePropertyStatementsIteration = connection.getStatements(null, RDF.TYPE, OWL2.REFLEXIVEPROPERTY, true);
				while (reflexivePropertyStatementsIteration.hasNext()) {
					Statement reflexivePropertyStatement = reflexivePropertyStatementsIteration.next();
					reflexivePropertyStatements.add(reflexivePropertyStatement);
					
					if (! isURI(reflexivePropertyStatement.getSubject())) {
						continue;
					}
					
					applyOnAdded(inferredStatements, addedStatement, reflexivePropertyStatement);
				}
			} else {
				for (Statement reflexivePropertyStatement : reflexivePropertyStatements) {
					if (! isURI(reflexivePropertyStatement.getSubject())) {
						continue;
					}
					
					applyOnAdded(inferredStatements, addedStatement, reflexivePropertyStatement);
				}
			}
		}

		return addInferred(inferredStatements, REFLEXIVE_PROPERTY_RULE);
	}
	
	// adding: (r, rdf:type, owl:ReflexiveProperty)
	// if existing: (s, p, o)
	// then inferring: (s, r, s), (p, r, p), (o, r, o)
	private int applyOnAdded_2(Set<Statement> addedStatementsThisIteration) throws SailException {
		Set<Statement> inferredStatements = new HashSet<Statement>();

		CloseableIteration<? extends Statement, SailException> allStatements = null;
		
		for (Statement addedStatement : addedStatementsThisIteration) {
			if (! isTypeReflexivePropertyStatement(addedStatement)) {
				continue;
			}
			
			if (allStatements == null) {
				allStatements = connection.getStatements(null, null, null, true);
			}
			
			while (allStatements.hasNext()) {
				Statement statement = allStatements.next();
				applyOnAdded(inferredStatements, statement, addedStatement);
			}			
		}

		return addInferred(inferredStatements, REFLEXIVE_PROPERTY_RULE);
	}
	
	private void applyOnAdded(Set<Statement> inferredStatements, Statement statement, Statement reflexivePropertyStatement) {
		URI reflexiveProperty = (URI) reflexivePropertyStatement.getSubject();
		
		if (isURI(statement.getSubject())) {
			Statement subjectReflexiveStatement = valueFactory.createStatement(statement.getSubject(), reflexiveProperty, statement.getSubject(), statement.getContext());
			inferredStatements.add(subjectReflexiveStatement);
		}
		
		Statement predicateReflexiveStatement = valueFactory.createStatement(statement.getPredicate(), reflexiveProperty, statement.getPredicate(), statement.getContext());
		inferredStatements.add(predicateReflexiveStatement);
		
		if (isURI(statement.getObject())) {
			Statement objectReflexiveStatement = valueFactory.createStatement((URI) statement.getObject(), reflexiveProperty, (URI) statement.getObject(), statement.getContext());
			inferredStatements.add(objectReflexiveStatement);
		}
	}

	// removing: (s, p, o)
	// if existing: (r, rdf:type, owl:ReflexiveProperty)
	// then removing inferred, if inferred: {(s, r, s), (p, r, p), (o, r, o)}
	private int applyOnRemoved_1(Set<Statement> removedStatementsThisIteration) throws SailException {
		Set<Statement> inferredStatements = new HashSet<Statement>();

		for (Statement removedStatement : removedStatementsThisIteration) {
			List<Statement> reflexivePropertyStatements =  new ArrayList<Statement>();
				
			CloseableIteration<? extends Statement, SailException> reflexivePropertyStatementsIteration = connection.getStatements(null, RDF.TYPE, OWL2.REFLEXIVEPROPERTY, true);
			while (reflexivePropertyStatementsIteration.hasNext()) {
				Statement reflexivePropertyStatement = reflexivePropertyStatementsIteration.next();
				reflexivePropertyStatements.add(reflexivePropertyStatement);
				
				if (! isURI(reflexivePropertyStatement.getSubject())) {
					continue;
				}
				
				applyOnRemoved(inferredStatements, removedStatement, reflexivePropertyStatement);
			}
			
			for (Statement reflexivePropertyStatement : reflexivePropertyStatements) {
				if (! isURI(reflexivePropertyStatement.getSubject())) {
					continue;
				}
				
				applyOnRemoved(inferredStatements, removedStatement, reflexivePropertyStatement);
			}
		}

		return removeInferred(inferredStatements, REFLEXIVE_PROPERTY_RULE);
	}
	
	// removing: (r, rdf:type, owl:ReflexiveProperty)
	// if existing: (s, p, o)
	// then removing inferred: (s, r, s), (p, r, p), (o, r, o)
	private int applyOnRemoved_2(Set<Statement> removedStatementsThisIteration) throws SailException {
		Set<Statement> inferredStatements = new HashSet<Statement>();

		for (Statement removedStatement : removedStatementsThisIteration) {
			if (! isTypeReflexivePropertyStatement(removedStatement) || 
					! isURI(removedStatement.getSubject())) {
				continue;
			}
			
			CloseableIteration<? extends Statement, SailException> results = connection.getStatements(null, (URI) removedStatement.getSubject(), null, true, removedStatement.getContext());
			while (results.hasNext()) {
				inferredStatements.add(results.next());
			}
		}

		return removeInferred(inferredStatements, REFLEXIVE_PROPERTY_RULE);
	}
	
	private void applyOnRemoved(Set<Statement> inferredStatements, Statement removedStatement, Statement reflexivePropertyStatement) throws SailException {
		URI reflexiveProperty = (URI) reflexivePropertyStatement.getSubject();
		
		if (isURI(removedStatement.getSubject()) && isReflexiveStatementOrphaned((URI) removedStatement.getSubject())) {
			Statement reflexiveStatement = valueFactory.createStatement(removedStatement.getSubject(), reflexiveProperty, removedStatement.getSubject());
			if (! isAxiomaticStatement(reflexiveStatement)) {
				inferredStatements.add(reflexiveStatement);
			}
		}
		
		if (isReflexiveStatementOrphaned(removedStatement.getPredicate())) {
			Statement reflexiveStatement = valueFactory.createStatement(removedStatement.getPredicate(), reflexiveProperty, removedStatement.getPredicate());
			if (! isAxiomaticStatement(reflexiveStatement)) {
				inferredStatements.add(reflexiveStatement);
			}
		}
		
		if (isURI(removedStatement.getObject()) && isReflexiveStatementOrphaned((URI) removedStatement.getObject())) {
			Statement reflexiveStatement = valueFactory.createStatement((URI) removedStatement.getObject(), reflexiveProperty, removedStatement.getObject());
			if (! isAxiomaticStatement(reflexiveStatement)) {
				inferredStatements.add(reflexiveStatement);
			}
		}
	}
	
	private boolean isAxiomaticStatement(Statement reflexiveStatement) {
		RDFSAxiomaticStatements rdfsAxiomaticStatements = new RDFSAxiomaticStatements();
		if (rdfsAxiomaticStatements.contains(reflexiveStatement)) {
			return true;
		}
		
		OWL2AxiomaticStatements owl2AxiomaticStatements = new OWL2AxiomaticStatements();
		if (owl2AxiomaticStatements.contains(reflexiveStatement)) {
			return true;
		}
		
		return false;
	}

	private boolean isReflexiveStatementOrphaned(URI uri) throws SailException {
		if (hasNonReflexiveStatement(uri, null, null, false)) {
			return false;
		}
		if (hasNonReflexiveStatement(null, uri, null, false)) {
			return false;
		}
		if (hasNonReflexiveStatement(null, null, uri, false)) {
			return false;
		}
		
		return true;
	}

	private boolean hasNonReflexiveStatement(Resource subject, URI predicate, Value object, boolean includeInferred, Resource... contexts) throws SailException {
		CloseableIteration<? extends Statement, SailException> results = connection.getStatements(subject, predicate, object, includeInferred, contexts);
		while (results.hasNext()) {
			Statement result = results.next();
			if (! isReflexiveStatement(result)) {
				return true;
			}
		}
		
		return false;
	}

}
