package com.factmint.graphhub.sail.owl2sail.rules;

import static com.factmint.graphhub.sail.owl2sail.sesame.utility.Constants.SUBCLASS_OF_CLASS_AXIOM_RULE;
import static com.factmint.graphhub.sail.owl2sail.sesame.utility.Constants.SUBCLASS_OF_DOMAIN_RULE;
import static com.factmint.graphhub.sail.owl2sail.sesame.utility.Constants.SUBCLASS_OF_EQUIVALENT_CLASS_RULE;
import static com.factmint.graphhub.sail.owl2sail.sesame.utility.Constants.SUBCLASS_OF_RANGE_RULE;
import info.aduna.iteration.CloseableIteration;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.openrdf.model.Resource;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.Value;
import org.openrdf.model.vocabulary.RDF;
import org.openrdf.model.vocabulary.RDFS;
import org.openrdf.sail.SailException;
import org.openrdf.sail.inferencer.InferencerConnection;

import com.factmint.graphhub.sail.owl2sail.axioms.OWL2;

public class SubClassOfRule extends Owl2Rule {

	public SubClassOfRule(InferencerConnection connection, Map<String, List<String>> activeRulesDependencies) {
		super(connection, activeRulesDependencies);
	}

	@Override
	public Collection<String> apply(Set<Statement> addedStatementsThisIteration, Set<Statement> removedStatementsThisIteration,	Set<String> rulesToRunThisIteration) throws SailException {
		Collection<String> rulesThatMadeChanges = new ArrayList<String>();
		
		if (rulesToRunThisIteration.contains(SUBCLASS_OF_CLASS_AXIOM_RULE)) {
			int count = subClassOfClassAxiomRule(addedStatementsThisIteration, removedStatementsThisIteration);
			logger.debug("{} statement changes after applying {}.", count, SUBCLASS_OF_CLASS_AXIOM_RULE);
			if (count > 0) {
				rulesThatMadeChanges.add(SUBCLASS_OF_CLASS_AXIOM_RULE);
			}
		}

		if (rulesToRunThisIteration.contains(SUBCLASS_OF_EQUIVALENT_CLASS_RULE)) {
			int count = subClassOfEquivalentClassRule(addedStatementsThisIteration, removedStatementsThisIteration);
			logger.debug("{} statement changes after applying {}.", count, SUBCLASS_OF_EQUIVALENT_CLASS_RULE);
			if (count > 0) {
				rulesThatMadeChanges.add(SUBCLASS_OF_EQUIVALENT_CLASS_RULE);
			}
		}
		
		if (rulesToRunThisIteration.contains(SUBCLASS_OF_DOMAIN_RULE)) {
			int count = subClassOfDomainRule(addedStatementsThisIteration, removedStatementsThisIteration);
			logger.debug("{} statement changes after applying {}.", count, SUBCLASS_OF_DOMAIN_RULE);
			if (count > 0) {
				rulesThatMadeChanges.add(SUBCLASS_OF_DOMAIN_RULE);
			}
		}
		
		if (rulesToRunThisIteration.contains(SUBCLASS_OF_RANGE_RULE)) {
			int count = subClassOfRangeRule(addedStatementsThisIteration, removedStatementsThisIteration);
			logger.debug("{} statement changes after applying {}.", count, SUBCLASS_OF_RANGE_RULE);
			if (count > 0) {
				rulesThatMadeChanges.add(SUBCLASS_OF_RANGE_RULE);
			}
		}
		
		return buildRulesToRunNextIteration(rulesThatMadeChanges);
	}

	private int subClassOfClassAxiomRule(Set<Statement> addedStatementsThisIteration, Set<Statement> removedStatementsThisIteration) throws SailException {
		int count = 0;
		if (addedStatementsThisIteration != null) {
			count += classAxiomRuleOnAdded_1(addedStatementsThisIteration);
			count += classAxiomRuleOnAdded_2(addedStatementsThisIteration);
		}
		if (removedStatementsThisIteration != null) {
			count += classAxiomRuleOnRemoved_1(removedStatementsThisIteration);
			count += classAxiomRuleOnRemoved_2(removedStatementsThisIteration);
		}
		return count;
	}
	
	private int subClassOfEquivalentClassRule(Set<Statement> addedStatementsThisIteration, Set<Statement> removedStatementsThisIteration) throws SailException {
		int count = 0;
		if (addedStatementsThisIteration != null) {
			count += equivalentClassRuleOnAdded(addedStatementsThisIteration);
		}
		if (removedStatementsThisIteration != null) {
			count += equivalentClassRuleOnRemoved(removedStatementsThisIteration);
		}
		return count;
	}

	private int subClassOfDomainRule(Set<Statement> addedStatementsThisIteration, Set<Statement> removedStatementsThisIteration) throws SailException {
		int count = 0;
		if (addedStatementsThisIteration != null) {
			count += domainRuleOnAdded_1(addedStatementsThisIteration);
			count += domainRuleOnAdded_2(addedStatementsThisIteration);
		}
		if (removedStatementsThisIteration != null) {
			count += domainRuleOnRemoved_1(removedStatementsThisIteration);
			count += domainRuleOnRemoved_2(removedStatementsThisIteration);
		}
		return count;
	}

	private int subClassOfRangeRule(Set<Statement> addedStatementsThisIteration, Set<Statement> removedStatementsThisIteration) throws SailException {
		int count = 0;
		if (addedStatementsThisIteration != null) {
			count += rangeRuleOnAdded_1(addedStatementsThisIteration);
			count += rangeRuleOnAdded_2(addedStatementsThisIteration);
		}
		if (removedStatementsThisIteration != null) {
			count += rangeRuleOnRemoved_1(removedStatementsThisIteration);
			count += rangeRuleOnRemoved_2(removedStatementsThisIteration);
		}
		return count;
	}
	
	// adding: (c1, rdfs:subClassOf, c2)
	// if existing: (s, rdf:type, c1)
	// then inferring: (s, rdf:type, c2)
	private int classAxiomRuleOnAdded_1(Set<Statement> addedStatementsThisIteration) throws SailException {
		Set<Statement> inferredStatements = new HashSet<Statement>();
		Set<Statement> allTypeStatements = null;
		
		for (Statement addedStatement : addedStatementsThisIteration) {
			if (! isSubclassOfStatement(addedStatement)) {
				continue;
			}
			
			if (allTypeStatements == null) {
				allTypeStatements = getAllTypeStatements();
				if (allTypeStatements.isEmpty()) {
					return 0;
				}
			}
			
			for (Statement typeStatement : allTypeStatements) {
				if (typeStatement.getObject().equals(addedStatement.getSubject())) {
					Statement inferredStatement = valueFactory.createStatement(typeStatement.getSubject(), RDF.TYPE, addedStatement.getObject(), typeStatement.getContext());
					inferredStatements.add(inferredStatement);
				}
			}
		}

		return addInferred(inferredStatements, SUBCLASS_OF_CLASS_AXIOM_RULE);
	}

	// adding: (s, rdf:type, c1)
	// if existing: (c1, rdfs:subClassOf, c2)
	// then inferring: (s, rdf:type, c2)
	private int classAxiomRuleOnAdded_2(Set<Statement> addedStatementsThisIteration) throws SailException {
		Set<Statement> inferredStatements = new HashSet<Statement>();
		Set<Statement> allSubclassOfStatements = null;
		
		for (Statement addedStatement : addedStatementsThisIteration) {
			if (! isTypeStatement(addedStatement) || ! isResource(addedStatement.getObject())) {
				continue;
			}
			
			if (allSubclassOfStatements == null) {
				allSubclassOfStatements = getAllSubclassOfStatements();
				if (allSubclassOfStatements.isEmpty()) {
					return 0;
				}
			}
			
			for (Statement subclassOfStatement : allSubclassOfStatements) {
				if (subclassOfStatement.getSubject().equals(addedStatement.getObject())) {
					Statement inferredStatement = valueFactory.createStatement(addedStatement.getSubject(), RDF.TYPE, subclassOfStatement.getObject(), subclassOfStatement.getContext());
					inferredStatements.add(inferredStatement);
				}
			}
		}

		return addInferred(inferredStatements, SUBCLASS_OF_CLASS_AXIOM_RULE);
	}

	// removing: (c1, rdfs:subClassOf, c2)
	// if existing: (s, rdf:type, c1)
	// then removing inferred: (s, rdf:type, c2)
	// UNLESS:
	//   existing: {(s, p, o), (p, domain, c2)}
	//   OR
	//   existing: {(s2, p, s), (p, range, c2)}
	private int classAxiomRuleOnRemoved_1(Set<Statement> removedStatementsThisIteration) throws SailException {
		Set<Statement> inferredStatements = new HashSet<Statement>();
		Set<Statement> allTypeStatements = null;
		Set<Statement> allDomainStatements = null;
		Set<Statement> allRangeStatements = null;
		
		for (Statement removedStatement : removedStatementsThisIteration) {
			if (! isSubclassOfStatement(removedStatement)) {
				continue;
			}
			
			if (allTypeStatements == null) {
				allTypeStatements = getAllTypeStatements();
				if (allTypeStatements.isEmpty()) {
					return 0;
				}
			}
			
			if (allDomainStatements == null) {
				allDomainStatements = getAllDomainStatements();
			}
			
			if (allRangeStatements == null) {
				allRangeStatements = getAllRangeStatements();
			}
			
			for (Statement typeStatement : allTypeStatements) {
				if (typeStatement.getObject().equals(removedStatement.getSubject())) {
					Value candidateSuperClass = removedStatement.getObject();

					boolean retainStatement = false;
					
					// Check for domain exception
					Set<URI> predicatesToCheckDomain = getAllPredicatesFromSubject(typeStatement.getSubject());
					for (Statement domainStatement : allDomainStatements) {
						if (! candidateSuperClass.equals(domainStatement.getObject())) continue;
						if (! predicatesToCheckDomain.contains(domainStatement.getSubject())) continue;
						
						retainStatement = true;
						break;
					}
					
					if (retainStatement) continue;
					
					// Check for range exception
					Set<URI> predicatesToCheckRange = getAllPredicatesFromObject(typeStatement.getSubject());
					for (Statement rangeStatement : allRangeStatements) {
						if (! candidateSuperClass.equals(rangeStatement.getObject())) continue;
						if (! predicatesToCheckRange.contains(rangeStatement.getSubject())) continue;
						
						retainStatement = true;
						break;
					}
					
					if (retainStatement) continue;
					
					Statement inferredStatement = valueFactory.createStatement(typeStatement.getSubject(), RDF.TYPE, candidateSuperClass, typeStatement.getContext());
					inferredStatements.add(inferredStatement);
				}
			}
		}

		return removeInferred(inferredStatements, SUBCLASS_OF_CLASS_AXIOM_RULE);
	}

	// removing: (s, rdf:type, c1)
	// if existing: (c1, rdfs:subClassOf, c2)
	// then removing inferred: (s, rdf:type, c2)
	// UNLESS:
	//   existing: {(s, p, o), (p, domain, c2)}
	//   OR
	//   existing: {(s2, p, s), (p, range, c2)}
	private int classAxiomRuleOnRemoved_2(Set<Statement> removedStatementsThisIteration) throws SailException {
		Set<Statement> inferredStatements = new HashSet<Statement>();
		Set<Statement> allSubclassOfStatements = null;
		Set<Statement> allDomainStatements = null;
		Set<Statement> allRangeStatements = null;
		
		for (Statement removedStatement : removedStatementsThisIteration) {
			if (! isTypeStatement(removedStatement) || ! isResource(removedStatement.getObject())) {
				continue;
			}
			
			if (allSubclassOfStatements == null) {
				allSubclassOfStatements = getAllSubclassOfStatements();
				if (allSubclassOfStatements.isEmpty()) {
					return 0;
				}
			}
			
			if (allDomainStatements == null) {
				allDomainStatements = getAllDomainStatements();
			}
			
			if (allRangeStatements == null) {
				allRangeStatements = getAllRangeStatements();
			}
			
			for (Statement subclassOfStatement : allSubclassOfStatements) {
				if (subclassOfStatement.getSubject().equals(removedStatement.getObject())) {
					Value candidateSuperClass = subclassOfStatement.getObject();

					boolean retainStatement = false;
					
					// Check for domain exception
					Set<URI> predicatesToCheckDomain = getAllPredicatesFromSubject(removedStatement.getSubject());
					for (Statement domainStatement : allDomainStatements) {
						if (! candidateSuperClass.equals(domainStatement.getObject())) continue;
						if (! predicatesToCheckDomain.contains(domainStatement.getSubject())) continue;
						
						retainStatement = true;
						break;
					}
					
					if (retainStatement) continue;
					
					// Check for range exception
					Set<URI> predicatesToCheckRange = getAllPredicatesFromObject(removedStatement.getSubject());
					for (Statement rangeStatement : allRangeStatements) {
						if (! candidateSuperClass.equals(rangeStatement.getObject())) continue;
						if (! predicatesToCheckRange.contains(rangeStatement.getSubject())) continue;
						
						retainStatement = true;
						break;
					}
					
					if (retainStatement) continue;
					
					Statement inferredStatement = valueFactory.createStatement(removedStatement.getSubject(), RDF.TYPE, candidateSuperClass, subclassOfStatement.getContext());
					inferredStatements.add(inferredStatement);
				}
			}
		}

		return removeInferred(inferredStatements, SUBCLASS_OF_CLASS_AXIOM_RULE);
	}

	// adding: (c1, rdfs:subClassOf, c2)
	// if existing: (c2, rdfs:subClassOf, c1)
	// then inferring: {(c1, owl:equivalentClass, c2), (c2, owl:equivalentClass, c1)}
	private int equivalentClassRuleOnAdded(Set<Statement> addedStatementsThisIteration) throws SailException {
		Set<Statement> inferredStatements = new HashSet<Statement>();
		
		for (Statement addedStatement : addedStatementsThisIteration) {
			if (! isSubclassOfStatement(addedStatement) || ! isResource(addedStatement.getObject())) {
				continue;
			}
					
			CloseableIteration<? extends Statement, SailException> symmetricSubclassOfStatements = connection.getStatements((Resource) addedStatement.getObject(), RDFS.SUBCLASSOF, addedStatement.getSubject(), true);
			while (symmetricSubclassOfStatements.hasNext()) {
				Statement symmetricSubclassOfStatement = symmetricSubclassOfStatements.next();
				Statement inferredStatement = valueFactory.createStatement(addedStatement.getSubject(), OWL2.EQUIVALENTCLASS, addedStatement.getObject(), symmetricSubclassOfStatement.getContext());
				inferredStatements.add(inferredStatement);
				Statement symmetricInferredStatement = valueFactory.createStatement((URI) addedStatement.getObject(), OWL2.EQUIVALENTCLASS, addedStatement.getSubject(), symmetricSubclassOfStatement.getContext());
				inferredStatements.add(symmetricInferredStatement);
			}
		}

		return addInferred(inferredStatements, SUBCLASS_OF_EQUIVALENT_CLASS_RULE);
	}
	
	// removing: (c1, rdfs:subClassOf, c2)
	// if existing: (c2, rdfs:subClassOf, c1)
	// then removing inferred: {(c1, owl:equivalentClass, c2), (c2, owl:equivalentClass, c1)}
	private int equivalentClassRuleOnRemoved(Set<Statement> removedStatementsThisIteration) throws SailException {
		Set<Statement> inferredStatements = new HashSet<Statement>();

		for (Statement removedStatement : removedStatementsThisIteration) {
			if (! isSubclassOfStatement(removedStatement) || ! isResource(removedStatement.getObject())) {
				continue;
			}
			CloseableIteration<? extends Statement, SailException> symmetricSubclassOfStatements = connection.getStatements((Resource) removedStatement.getObject(), RDFS.SUBCLASSOF, removedStatement.getSubject(), true);
			while (symmetricSubclassOfStatements.hasNext()) {
				Statement symmetricSubclassOfStatement = symmetricSubclassOfStatements.next();
				Statement inferredStatement = valueFactory.createStatement(removedStatement.getSubject(), OWL2.EQUIVALENTCLASS, removedStatement.getObject(), symmetricSubclassOfStatement.getContext());
				inferredStatements.add(inferredStatement);
				Statement symmetricInferredStatement = valueFactory.createStatement((Resource) removedStatement.getObject(), OWL2.EQUIVALENTCLASS, removedStatement.getSubject(), symmetricSubclassOfStatement.getContext());
				inferredStatements.add(symmetricInferredStatement);
			}
		}

		return removeInferred(inferredStatements, SUBCLASS_OF_EQUIVALENT_CLASS_RULE);
	}
	
	// adding: (p, rdfs:domain, c1)
	// if existing: (c1, rdfs:subClassOf, c2)
	// then inferring: (p, rdfs:domain, c2)
	private int domainRuleOnAdded_1(Set<Statement> addedStatementsThisIteration) throws SailException {
		Set<Statement> inferredStatements = new HashSet<Statement>();
		Set<Statement> allSubclassOfStatements = null;
		
		for (Statement addedStatement : addedStatementsThisIteration) {
			if (! isDomainStatement(addedStatement) || ! isResource(addedStatement.getObject())) {
				continue;
			}
			
			if (allSubclassOfStatements == null) {
				allSubclassOfStatements = getAllSubclassOfStatements();
				if (allSubclassOfStatements.isEmpty()) {
					return 0;
				}
			}
			
			for (Statement subclassOfStatement : allSubclassOfStatements) {
				if (subclassOfStatement.getSubject().equals(addedStatement.getObject())) {
					Statement inferredStatement = valueFactory.createStatement(addedStatement.getSubject(), RDFS.DOMAIN, subclassOfStatement.getObject(), subclassOfStatement.getContext());
					inferredStatements.add(inferredStatement);
				}
			}
		}

		return addInferred(inferredStatements, SUBCLASS_OF_DOMAIN_RULE);
	}

	// adding: (c1, rdfs:subClassOf, c2)
	// if existing: (p, rdfs:domain, c1)
	// then inferring: (p, rdfs:domain, c2)
	private int domainRuleOnAdded_2(Set<Statement> addedStatementsThisIteration) throws SailException {
		Set<Statement> inferredStatements = new HashSet<Statement>();
		Set<Statement> allDomainStatements = null;		
		
		for (Statement addedStatement : addedStatementsThisIteration) {
			if (! isSubclassOfStatement(addedStatement)) {
				continue;
			}
			
			if (allDomainStatements == null) {
				allDomainStatements = getAllDomainStatements();
				if (allDomainStatements.isEmpty()) {
					return 0;
				}
			}
			
			for (Statement domainStatement : allDomainStatements) {
				if (domainStatement.getObject().equals(addedStatement.getSubject())) {
					Statement inferredStatement = valueFactory.createStatement(domainStatement.getSubject(), RDFS.DOMAIN, addedStatement.getObject(), domainStatement.getContext());
					inferredStatements.add(inferredStatement);
				}
			}
		}

		return addInferred(inferredStatements, SUBCLASS_OF_DOMAIN_RULE);
	}
	
	// removing: (p, rdfs:domain, c1)
	// if existing: (c1, rdfs:subClassOf, c2)
	// and no existing: {(c3, rdfs:subClassOf, c2), (p, rdfs:domain, c3)}
	// then removing inferred: (p, rdfs:domain, c2)	
	private int domainRuleOnRemoved_1(Set<Statement> removedStatementsThisIteration) throws SailException {
		Set<Statement> inferredStatements = new HashSet<Statement>();
		Set<Statement> allSubclassOfStatements = null;
		
		for (Statement removedStatement : removedStatementsThisIteration) {
			if (! isDomainStatement(removedStatement) || ! isResource(removedStatement.getObject())) {
				continue;
			}
			
			if (allSubclassOfStatements == null) {
				allSubclassOfStatements = getAllSubclassOfStatements();
				if (allSubclassOfStatements.isEmpty()) {
					return 0;
				}
			}
			
			Set<Resource> otherDomains = findOtherDomainsForProperty(removedStatement);
			for (Statement subclassOfStatement : allSubclassOfStatements) {
				if (subclassOfStatement.getSubject().equals(removedStatement.getObject())) {
					if (isResource(subclassOfStatement.getObject())) {
						Resource superClass = (Resource) subclassOfStatement.getObject();
						if (! otherDomains.isEmpty() && containsSubclassOfClass(otherDomains, superClass)) {
							continue;
						}
					}
					
					Statement inferredStatement = valueFactory.createStatement(removedStatement.getSubject(), RDFS.DOMAIN, subclassOfStatement.getObject(), subclassOfStatement.getContext());
					inferredStatements.add(inferredStatement);	
				}
			}
		}

		return removeInferred(inferredStatements, SUBCLASS_OF_DOMAIN_RULE);
	}

	// removing: (c1, rdfs:subClassOf, c2)
	// if existing: (p, rdfs:domain, c1)
	// and no existing: {(c3, rdfs:subClassOf, c2), (p, rdfs:domain, c3)}
	// then removing inferred: (p, rdfs:domain, c2)	
	private int domainRuleOnRemoved_2(Set<Statement> removedStatementsThisIteration) throws SailException {
		Set<Statement> inferredStatements = new HashSet<Statement>();
		Set<Statement> allDomainStatements = null;
		
		for (Statement removedStatement : removedStatementsThisIteration) {
			if (! isSubclassOfStatement(removedStatement) || ! isResource(removedStatement.getObject())) {
				continue;
			}
			
			if (allDomainStatements == null) {
				allDomainStatements = getAllDomainStatements();
				if (allDomainStatements.isEmpty()) {
					return 0;
				}
			}
			
			for (Statement domainStatement : allDomainStatements) {
				if (domainStatement.getObject().equals(removedStatement.getSubject())) {
					Set<Resource> otherDomains = findOtherDomainsForProperty(domainStatement);
					Resource superClass = (Resource) removedStatement.getObject();
					if (! otherDomains.isEmpty() && containsSubclassOfClass(otherDomains, superClass)) {
						continue;
					}
					
					Statement inferredStatement = valueFactory.createStatement(domainStatement.getSubject(), RDFS.DOMAIN, removedStatement.getObject(), domainStatement.getContext());
					inferredStatements.add(inferredStatement);
				}				
			}
		}

		return removeInferred(inferredStatements, SUBCLASS_OF_DOMAIN_RULE);
	}

	// removing: (x, a, c2)
	// if existing: (c2, rdfs:subClassOf, c1)
	// and no existing: {(x, p, y), (p, rdfs:domain, c1)}
	// then removing inferred: (p, a, c1)	
	/*
	private int domainRuleOnRemoved_3(Set<Statement> removedStatementsThisIteration) throws SailException {
		Set<Statement> inferredStatements = new HashSet<Statement>();
		Set<Statement> allDomainStatements = null;
		
		for (Statement removedStatement : removedStatementsThisIteration) {
			if (! isTypeStatement(removedStatement)) {
				continue;
			}
			
			if (allDomainStatements == null) {
				allDomainStatements = getAllDomainStatements();
				if (allDomainStatements.isEmpty()) {
					return 0;
				}
			}
			
			Set<URI> predicates = getAllPredicatesFromSubject(removedStatement.getSubject());
			for (Resource superClass : getSuperClasses((Resource) removedStatement.getObject())) {
				
				for (Statement domainStatement : allDomainStatements) {
					if (domainStatement.getObject().equals(superClass) && ! predicates.contains(domainStatement.getSubject())) {
						inferredStatements.add(removedStatement);
					}				
				}
			}
		}

		return removeInferred(inferredStatements, SUBCLASS_OF_DOMAIN_RULE);
	}*/

	// adding: (p, rdfs:range, c1)
	// if existing: (c1, rdfs:subClassOf, c2)
	// then inferring: (p, rdfs:range, c2)
	private int rangeRuleOnAdded_1(Set<Statement> addedStatementsThisIteration) throws SailException {
		Set<Statement> inferredStatements = new HashSet<Statement>();
		Set<Statement> allSubclassOfStatements = null;
		
		for (Statement addedStatement : addedStatementsThisIteration) {
			if (! isRangeStatement(addedStatement) || ! isResource(addedStatement.getObject())) {
				continue;
			}
			
			if (allSubclassOfStatements == null) {
				allSubclassOfStatements = getAllSubclassOfStatements();
				if (allSubclassOfStatements.isEmpty()) {
					return 0;
				}
			}
			
			for (Statement subclassOfStatement : allSubclassOfStatements) {
				if (subclassOfStatement.getSubject().equals(addedStatement.getObject())) {
					Statement inferredStatement = valueFactory.createStatement(addedStatement.getSubject(), RDFS.RANGE, subclassOfStatement.getObject(), subclassOfStatement.getContext());
					inferredStatements.add(inferredStatement);
				}
			}
		}

		return addInferred(inferredStatements, SUBCLASS_OF_RANGE_RULE);
	}
	
	// adding: (c1, rdfs:subClassOf, c2)
	// if existing: (p, rdfs:range, c1)
	// then inferring: (p, rdfs:range, c2)
	private int rangeRuleOnAdded_2(Set<Statement> addedStatementsThisIteration) throws SailException {
		Set<Statement> inferredStatements = new HashSet<Statement>();
		Set<Statement> allRangeStatements = null;

		for (Statement addedStatement : addedStatementsThisIteration) {
			if (! isSubclassOfStatement(addedStatement)) {
				continue;
			}
			
			if (allRangeStatements == null) {
				allRangeStatements = getAllRangeStatements();
				if (allRangeStatements.isEmpty()) {
					return 0;
				}
			}
			
			for (Statement rangeStatement : allRangeStatements) {
				if (rangeStatement.getObject().equals(addedStatement.getSubject())) {
					Statement inferredStatement = valueFactory.createStatement(rangeStatement.getSubject(), RDFS.RANGE, addedStatement.getObject(), rangeStatement.getContext());
					inferredStatements.add(inferredStatement);					
				}
			}
		}

		return addInferred(inferredStatements, SUBCLASS_OF_RANGE_RULE);
	}

	// removing: (p, rdfs:range, c1)
	// if existing: (c1, rdfs:subClassOf, c2)
	// and no existing: {(c3, rdfs:subClassOf, c2), (p, rdfs:range, c3)}
	// then removing inferred: (p, rdfs:range, c2)
	private int rangeRuleOnRemoved_1(Set<Statement> removedStatementsThisIteration) throws SailException {
		Set<Statement> inferredStatements = new HashSet<Statement>();
		Set<Statement> allSubclassOfStatements = null;
		
		for (Statement removedStatement : removedStatementsThisIteration) {
			if (! isRangeStatement(removedStatement) || ! isResource(removedStatement.getObject())) {
				continue;
			}
			
			Set<Resource> otherRanges = findOtherRangesForProperty(removedStatement);
			
			if (allSubclassOfStatements == null) {
				allSubclassOfStatements = getAllSubclassOfStatements();
				if (allSubclassOfStatements.isEmpty()) {
					return 0;
				}
			}
			
			for (Statement subclassOfStatement : allSubclassOfStatements) {
				if (! isResource(subclassOfStatement.getObject())) {
					continue;
				}

				if (removedStatement.getObject().equals(subclassOfStatement.getSubject())) {
					Resource superClass = (Resource) subclassOfStatement.getObject();
					if (! otherRanges.isEmpty() && containsSubclassOfClass(otherRanges, superClass)) {
						continue;
					}
								
					Statement inferredStatement = valueFactory.createStatement(removedStatement.getSubject(), RDFS.RANGE, subclassOfStatement.getObject());
					inferredStatements.add(inferredStatement);					
				}
			}
		}

		return removeInferred(inferredStatements, SUBCLASS_OF_RANGE_RULE);
	}
	
	// removing: (c1, rdfs:subClassOf, c2)
	// if existing: (p, rdfs:range, c1)
	// and no existing: {(c3, rdfs:subClassOf, c2), (p, rdfs:range, c3)}
	// then removing inferred: (p, rdfs:range, c2)	
	private int rangeRuleOnRemoved_2(Set<Statement> removedStatementsThisIteration) throws SailException {
		Set<Statement> inferredStatements = new HashSet<Statement>();
		Set<Statement> allRangeStatements = null;

		for (Statement removedStatement : removedStatementsThisIteration) {
			if (! isSubclassOfStatement(removedStatement) || ! isResource(removedStatement.getObject())) {
				continue;
			}
			
			if (allRangeStatements == null) {
				allRangeStatements = getAllRangeStatements();
				if (allRangeStatements.isEmpty()) {
					return 0;
				}
			}
			
			for (Statement rangeStatement : allRangeStatements) {
				if (rangeStatement.getObject().equals(removedStatement.getSubject())) {
					Set<Resource> otherRanges = findOtherRangesForProperty(rangeStatement);
					Resource superClass = (Resource) removedStatement.getObject();
					if (containsSubclassOfClass(otherRanges, superClass)) {
						continue;
					}
					
					Statement inferredStatement = valueFactory.createStatement(rangeStatement.getSubject(), RDFS.RANGE, removedStatement.getObject());
					inferredStatements.add(inferredStatement);
				}
			}
		}

		return removeInferred(inferredStatements, SUBCLASS_OF_RANGE_RULE);
	}
	
	private Set<Resource> findOtherDomainsForProperty(Statement domainStatement) throws SailException {
		Set<Resource> otherDomains = new HashSet<Resource>();
		CloseableIteration<? extends Statement, SailException> otherDomainStatements = connection.getStatements(domainStatement.getSubject(), RDFS.DOMAIN, null, true);
		while (otherDomainStatements.hasNext()) {
			Statement otherDomainStatement = otherDomainStatements.next();
			if ( isURI(otherDomainStatement.getObject()) && ! otherDomainStatement.getObject().equals(domainStatement.getObject())) {
				otherDomains.add((Resource) otherDomainStatement.getObject());
			}
		}		
		return otherDomains;
	}
	
	private Set<Resource> findOtherRangesForProperty(Statement rangeStatement) throws SailException {
		Set<Resource> otherRanges = new HashSet<Resource>();
		CloseableIteration<? extends Statement, SailException> otherRangeStatements = connection.getStatements(rangeStatement.getSubject(), RDFS.RANGE, null, true);
		while (otherRangeStatements.hasNext()) {
			Statement otherRangeStatement = otherRangeStatements.next();
			if ( isURI(otherRangeStatement.getObject()) && ! otherRangeStatement.getObject().equals(rangeStatement.getObject())) {
				otherRanges.add((Resource) otherRangeStatement.getObject());
			}
		}		
		return otherRanges;
	}
	
	private boolean containsSubclassOfClass(Set<Resource> classUris, Resource superClass) throws SailException {
		for (Resource classUri : classUris) {
			if (hasStatement(classUri, RDFS.SUBCLASSOF, superClass, true)) {
				return true;
			}
		}
		return false;
	}
	
}
