package com.factmint.graphhub.sail.owl2sail.configuration;

public class InverseOfRuleConfiguration {
	
	private boolean applyInverseOf = true;

	public boolean isApplyInverseOf() {
		return applyInverseOf;
	}

	public void setApplyInverseOf(boolean applyInverseOf) {
		this.applyInverseOf = applyInverseOf;
	}
	
	

}
