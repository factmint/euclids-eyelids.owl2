package com.factmint.graphhub.sail.owl2sail.sesame;

import org.openrdf.sail.NotifyingSail;
import org.openrdf.sail.SailException;
import org.openrdf.sail.helpers.NotifyingSailWrapper;
import org.openrdf.sail.inferencer.InferencerConnection;

import com.factmint.graphhub.sail.owl2sail.configuration.Owl2RulesConfiguration;

public class Owl2Sail extends NotifyingSailWrapper {
	
	private Boolean inForegroundInferencing = true;
	private Owl2RulesConfiguration rulesConfiguration;

	public Owl2Sail(NotifyingSail baseSail, Boolean inForegroundInferencing) {
		super(baseSail);
		this.inForegroundInferencing = inForegroundInferencing;
	}

	@Override
	public Owl2SailConnection getConnection() throws SailException {
		try {
			InferencerConnection connection = (InferencerConnection) super.getConnection();
			
			if (rulesConfiguration != null) {
				return new Owl2SailConnection(connection, inForegroundInferencing, rulesConfiguration);
			} else {
				return new Owl2SailConnection(connection, inForegroundInferencing);
			}
		} catch (ClassCastException e) {
			throw new SailException(e.getMessage(), e);
		}
	}

	@Override
	public void initialize() throws SailException {
		super.initialize();

		Owl2SailConnection connection = getConnection();
		try {
			connection.begin();
			connection.addAxiomStatements();
			connection.commit();
		} finally {
			connection.close();
		}
	}

	public Owl2RulesConfiguration getRulesConfiguration() {
		return rulesConfiguration;
	}

	public void setRulesConfiguration(Owl2RulesConfiguration rulesConfiguration) {
		this.rulesConfiguration = rulesConfiguration;
	}
	
}
