package com.factmint.graphhub.sail.owl2sail.rules;

import static com.factmint.graphhub.sail.owl2sail.sesame.utility.Constants.SUBPROPERTY_OF_DOMAIN_RULE;
import static com.factmint.graphhub.sail.owl2sail.sesame.utility.Constants.SUBPROPERTY_OF_EQUIVALENT_PROPERTY_RULE;
import static com.factmint.graphhub.sail.owl2sail.sesame.utility.Constants.SUBPROPERTY_OF_PROPERTY_AXIOM_RULE;
import static com.factmint.graphhub.sail.owl2sail.sesame.utility.Constants.SUBPROPERTY_OF_RANGE_RULE;
import info.aduna.iteration.CloseableIteration;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.openrdf.model.Resource;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.Value;
import org.openrdf.model.vocabulary.RDFS;
import org.openrdf.sail.SailException;
import org.openrdf.sail.inferencer.InferencerConnection;

import com.factmint.graphhub.sail.owl2sail.axioms.OWL2;

public class SubPropertyOfRule extends Owl2Rule {

	public SubPropertyOfRule(InferencerConnection connection, Map<String, List<String>> activeRulesDependencies) {
		super(connection, activeRulesDependencies);
	}

	@Override
	public Collection<String> apply(Set<Statement> addedStatementsThisIteration, Set<Statement> removedStatementsThisIteration,	Set<String> rulesToRunThisIteration) throws SailException {
		Collection<String> rulesThatMadeChanges = new ArrayList<String>();
		
		if (rulesToRunThisIteration.contains(SUBPROPERTY_OF_PROPERTY_AXIOM_RULE)) {
			int count = subPropertyOfPropertyAxiomRule(addedStatementsThisIteration, removedStatementsThisIteration);
			logger.debug("{} statement changes after applying {}.", count, SUBPROPERTY_OF_PROPERTY_AXIOM_RULE);
			if (count > 0) {
				rulesThatMadeChanges.add(SUBPROPERTY_OF_PROPERTY_AXIOM_RULE);
			}
		}

		if (rulesToRunThisIteration.contains(SUBPROPERTY_OF_EQUIVALENT_PROPERTY_RULE)) {
			int count = subPropertyOfEquivalentPropertyRule(addedStatementsThisIteration, removedStatementsThisIteration);
			logger.debug("{} statement changes after applying {}.", count, SUBPROPERTY_OF_EQUIVALENT_PROPERTY_RULE);
			if (count > 0) {
				rulesThatMadeChanges.add(SUBPROPERTY_OF_EQUIVALENT_PROPERTY_RULE);
			}
		}
		
		if (rulesToRunThisIteration.contains(SUBPROPERTY_OF_DOMAIN_RULE)) {
			int count = subPropertyOfDomainRule(addedStatementsThisIteration, removedStatementsThisIteration);
			logger.debug("{} statement changes after applying {}.", count, SUBPROPERTY_OF_DOMAIN_RULE);
			if (count > 0) {
				rulesThatMadeChanges.add(SUBPROPERTY_OF_DOMAIN_RULE);
			}
		}
		
		if (rulesToRunThisIteration.contains(SUBPROPERTY_OF_RANGE_RULE)) {
			int count = subPropertyOfRangeRule(addedStatementsThisIteration, removedStatementsThisIteration);
			logger.debug("{} statement changes after applying {}.", count, SUBPROPERTY_OF_RANGE_RULE);
			if (count > 0) {
				rulesThatMadeChanges.add(SUBPROPERTY_OF_RANGE_RULE);
			}
		}
		
		return buildRulesToRunNextIteration(rulesThatMadeChanges);
	}
	
	private int subPropertyOfPropertyAxiomRule(Set<Statement> addedStatementsThisIteration,	Set<Statement> removedStatementsThisIteration) throws SailException {
		int count = 0;
		if (addedStatementsThisIteration != null) {
			count += propertyAxiomRuleOnAdded_1(addedStatementsThisIteration);
			count += propertyAxiomRuleOnAdded_2(addedStatementsThisIteration);
		}
		if (removedStatementsThisIteration != null) {
			count += propertyAxiomRuleOnRemoved_1(removedStatementsThisIteration);
			count += propertyAxiomRuleOnRemoved_2(removedStatementsThisIteration);
		}
		return count;
	}
	
	private int subPropertyOfEquivalentPropertyRule(Set<Statement> addedStatementsThisIteration, Set<Statement> removedStatementsThisIteration) throws SailException {
		int count = 0;
		if (addedStatementsThisIteration != null) {
			count += equivalentPropertyRuleOnAdded(addedStatementsThisIteration);
		}
		if (removedStatementsThisIteration != null) {
			count += equivalentPropertyRuleOnRemoved(removedStatementsThisIteration);
		}
		return count;
	}

	private int subPropertyOfDomainRule(Set<Statement> addedStatementsThisIteration, Set<Statement> removedStatementsThisIteration) throws SailException {
		int count = 0;
		if (addedStatementsThisIteration != null) {
			count += domainRuleOnAdded_1(addedStatementsThisIteration);
			count += domainRuleOnAdded_2(addedStatementsThisIteration);
		}
		if (removedStatementsThisIteration != null) {
			count += domainRuleOnRemoved_1(removedStatementsThisIteration);
			count += domainRuleOnRemoved_2(removedStatementsThisIteration);
		}
		return count;
	}

	private int subPropertyOfRangeRule(Set<Statement> addedStatementsThisIteration,	Set<Statement> removedStatementsThisIteration) throws SailException {
		int count = 0;
		if (addedStatementsThisIteration != null) {
			count += rangeRuleOnAdded_1(addedStatementsThisIteration);
			count += rangeRuleOnAdded_2(addedStatementsThisIteration);
		}
		if (removedStatementsThisIteration != null) {
			count += rangeRuleOnRemoved_1(removedStatementsThisIteration);
			count += rangeRuleOnRemoved_2(removedStatementsThisIteration);
		}
		return count;
	}

	// adding: (p1, rdfs:subPropertyOf, p2)
	// if existing: (s, p1, o)
	// then inferring: (s, p2, o)
	private int propertyAxiomRuleOnAdded_1(Set<Statement> addedStatementsThisIteration) throws SailException {
		Set<Statement> inferredStatements = new HashSet<Statement>();

		for (Statement addedStatement : addedStatementsThisIteration) {
			if (! isSubPropertyOfStatement(addedStatement) 
					|| ! isURI(addedStatement.getSubject())
					|| ! isURI(addedStatement.getObject())) {
				continue;
			}
			CloseableIteration<? extends Statement, SailException> subPropertyStatements = connection.getStatements(null, (URI) addedStatement.getSubject(), null, true);
			while (subPropertyStatements.hasNext()) {
				Statement subPropertyStatement = subPropertyStatements.next();
				Statement inferredStatement = valueFactory.createStatement(subPropertyStatement.getSubject(), (URI) addedStatement.getObject(), subPropertyStatement.getObject(), subPropertyStatement.getContext());
				inferredStatements.add(inferredStatement);
			}
		}

		return addInferred(inferredStatements, SUBPROPERTY_OF_PROPERTY_AXIOM_RULE);
	}

	// adding: (s, p1, o)
	// if existing: (p1, rdfs:subPropertyOf, p2)
	// then inferring: (s, p2, o)
	private int propertyAxiomRuleOnAdded_2(Set<Statement> addedStatementsThisIteration) throws SailException {
		Set<Statement> inferredStatements = new HashSet<Statement>();
		Set<Statement> allSubpropertyOfStatements = null;
		
		for (Statement addedStatement : addedStatementsThisIteration) {
			if (isSubPropertyOfStatement(addedStatement)) {
				continue;
			}
			
			if (allSubpropertyOfStatements == null) {
				allSubpropertyOfStatements = getAllSubpropertyOfStatements();
				if (allSubpropertyOfStatements.isEmpty()) {
					return 0;
				}
			}
			
			for (Statement subpropertyOfStatement : allSubpropertyOfStatements) {
				if (subpropertyOfStatement.getSubject().equals(addedStatement.getPredicate())) {
					if (! isURI(subpropertyOfStatement.getObject())) {
						continue;
					}
					
					Statement inferredStatement = valueFactory.createStatement(addedStatement.getSubject(), (URI) subpropertyOfStatement.getObject(), addedStatement.getObject());
					inferredStatements.add(inferredStatement);
				}
			}
		}

		return addInferred(inferredStatements, SUBPROPERTY_OF_PROPERTY_AXIOM_RULE);
	}

	// removing: (p1, rdfs:subPropertyOf, p2)
	// if existing: (s, p1, o)
	// then removing inferred: (s, p2, o)
	private int propertyAxiomRuleOnRemoved_1(Set<Statement> removedStatementsThisIteration) throws SailException {
		Set<Statement> inferredStatements = new HashSet<Statement>();

		for (Statement removedStatement : removedStatementsThisIteration) {
			if (! isSubPropertyOfStatement(removedStatement) 
					|| ! isURI(removedStatement.getSubject())
					|| ! isURI(removedStatement.getObject())) {
				continue;
			}
			CloseableIteration<? extends Statement, SailException> subPropertyStatements = connection.getStatements(null, (URI) removedStatement.getSubject(), null, true);
			while (subPropertyStatements.hasNext()) {
				Statement subPropertyStatement = subPropertyStatements.next();
				Statement inferredStatement = valueFactory.createStatement(subPropertyStatement.getSubject(), (URI) removedStatement.getObject(), subPropertyStatement.getObject(), subPropertyStatement.getContext());
				inferredStatements.add(inferredStatement);
			}
		}

		return removeInferred(inferredStatements, SUBPROPERTY_OF_PROPERTY_AXIOM_RULE);
	}

	// removing: (s, p1, o)
	// if existing: (p1, rdfs:subPropertyOf, p2)
	// then removing inferred: (s, p2, o)
	private int propertyAxiomRuleOnRemoved_2(Set<Statement> removedStatementsThisIteration) throws SailException {
		Set<Statement> inferredStatements = new HashSet<Statement>();
		Set<Statement> allSubpropertyOfStatements = null;

		for (Statement removedStatement : removedStatementsThisIteration) {
			
			if (allSubpropertyOfStatements == null) {
				allSubpropertyOfStatements = getAllSubpropertyOfStatements();
				if (allSubpropertyOfStatements.isEmpty()) {
					return 0;
				}
			}
			
			for (Statement subpropertyOfStatement : allSubpropertyOfStatements) {
				if (subpropertyOfStatement.getSubject().equals(removedStatement.getPredicate())) {
					if (! isURI(subpropertyOfStatement.getObject())) {
						continue;
					}
					
					Statement inferredStatement = valueFactory.createStatement(removedStatement.getSubject(), (URI) subpropertyOfStatement.getObject(), removedStatement.getObject());
					inferredStatements.add(inferredStatement);
				}
			}
		}

		return removeInferred(inferredStatements, SUBPROPERTY_OF_PROPERTY_AXIOM_RULE);
	}
	
	// adding: (p1, rdfs:subPropertyOf, p2)
	// if existing: (p2, rdfs:subPropertyOf, p1)
	// then inferring: {(p1, owl:equivalentProperty, p2), (p2, owl:equivalentProperty, p1)}
	private int equivalentPropertyRuleOnAdded(Set<Statement> addedStatementsThisIteration) throws SailException {
		Set<Statement> inferredStatements = new HashSet<Statement>();

		for (Statement addedStatement : addedStatementsThisIteration) {
			if (! isSubPropertyOfStatement(addedStatement) || ! isURI(addedStatement.getObject())) {
				continue;
			}
			CloseableIteration<? extends Statement, SailException> symmetricSubPropertyOfStatements = connection.getStatements((URI) addedStatement.getObject(), RDFS.SUBPROPERTYOF, addedStatement.getSubject(), true);
			while (symmetricSubPropertyOfStatements.hasNext()) {
				Statement symmetricSubPropertyOfStatement = symmetricSubPropertyOfStatements.next();
				Statement inferredStatement = valueFactory.createStatement(addedStatement.getSubject(), OWL2.EQUIVALENTPROPERTY, addedStatement.getObject(), symmetricSubPropertyOfStatement.getContext());
				inferredStatements.add(inferredStatement);
				Statement symmetricInferredStatement = valueFactory.createStatement((URI) addedStatement.getObject(), OWL2.EQUIVALENTPROPERTY, addedStatement.getSubject(), symmetricSubPropertyOfStatement.getContext());
				inferredStatements.add(symmetricInferredStatement);
			}
		}

		return addInferred(inferredStatements, SUBPROPERTY_OF_EQUIVALENT_PROPERTY_RULE);
	}

	// removing: (p1, rdfs:subPropertyOf, p2)
	// if existing: (p2, rdfs:subPropertyOf, p1)
	// then removing inferred: {(p1, owl:equivalentProperty, p2), (p2, owl:equivalentProperty, p1)}
	private int equivalentPropertyRuleOnRemoved(Set<Statement> removedStatementsThisIteration) throws SailException {
		Set<Statement> inferredStatements = new HashSet<Statement>();

		for (Statement removedStatement : removedStatementsThisIteration) {
			if (! isSubPropertyOfStatement(removedStatement) || ! isURI(removedStatement.getObject())) {
				continue;
			}
			CloseableIteration<? extends Statement, SailException> symmetricSubPropertyOfStatements = connection.getStatements((URI) removedStatement.getObject(), RDFS.SUBPROPERTYOF, removedStatement.getSubject(), true);
			while (symmetricSubPropertyOfStatements.hasNext()) {
				Statement symmetricSubPropertyOfStatement = symmetricSubPropertyOfStatements.next();
				Statement inferredStatement = valueFactory.createStatement(removedStatement.getSubject(), OWL2.EQUIVALENTPROPERTY, removedStatement.getObject(), symmetricSubPropertyOfStatement.getContext());
				inferredStatements.add(inferredStatement);
				Statement symmetricInferredStatement = valueFactory.createStatement((URI) removedStatement.getObject(), OWL2.EQUIVALENTPROPERTY, removedStatement.getSubject(), symmetricSubPropertyOfStatement.getContext());
				inferredStatements.add(symmetricInferredStatement);
			}
		}

		return removeInferred(inferredStatements, SUBPROPERTY_OF_EQUIVALENT_PROPERTY_RULE);
	}
	
	// adding: (p2, rdfs:domain, c)
	// if existing: (p1, rdfs:subPropertyOf, p2)
	// then inferring: (p1, rdfs:domain, c)
	private int domainRuleOnAdded_1(Set<Statement> addedStatementsThisIteration) throws SailException {
		Set<Statement> inferredStatements = new HashSet<Statement>();
		Set<Statement> allSubpropertyOfStatements = null;
		
		for (Statement addedStatement : addedStatementsThisIteration) {
			if (! isDomainStatement(addedStatement)) {
				continue;
			}
			
			if (allSubpropertyOfStatements == null) {
				allSubpropertyOfStatements = getAllSubpropertyOfStatements();
				if (allSubpropertyOfStatements.isEmpty()) {
					return 0;
				}
			}
			
			for (Statement subpropertyOfStatement : allSubpropertyOfStatements) {
				if (subpropertyOfStatement.getObject().equals(addedStatement.getSubject())) {
					Statement inferredStatement = valueFactory.createStatement(subpropertyOfStatement.getSubject(), RDFS.DOMAIN, addedStatement.getObject());
					inferredStatements.add(inferredStatement);
				}
			}
		}

		return addInferred(inferredStatements, SUBPROPERTY_OF_DOMAIN_RULE);
	}

	// adding: (p1, rdfs:subPropertyOf, p2)
	// if existing: (p2, rdfs:domain, c)
	// then inferring: (p1, rdfs:domain, c)
	private int domainRuleOnAdded_2(Set<Statement> addedStatementsThisIteration) throws SailException {
		Set<Statement> inferredStatements = new HashSet<Statement>();
		Set<Statement> allDomainStatements = null; 
		
		for (Statement addedStatement : addedStatementsThisIteration) {
			if (! isSubPropertyOfStatement(addedStatement) || ! isResource(addedStatement.getObject())) {
				continue;
			}
			
			if (allDomainStatements == null) {
				allDomainStatements = getAllDomainStatements();
				if (allDomainStatements.isEmpty()) {
					return 0;
				}
			}
			
			for (Statement domainStatement : allDomainStatements) {
				if (domainStatement.getSubject().equals(addedStatement.getObject())) {
					Statement inferredStatement = valueFactory.createStatement(addedStatement.getSubject(), RDFS.DOMAIN, domainStatement.getObject());
					inferredStatements.add(inferredStatement);
				}
			}
		}

		return addInferred(inferredStatements, SUBPROPERTY_OF_DOMAIN_RULE);
	}

	// removing: (p2, rdfs:domain, c)
	// if existing: (p1, rdfs:subPropertyOf, p2)
	// and no existing: {(p1, rdfs:subPropertyOf, p3), (p3, rdfs:domain, c)}
	// then removing inferred: (p1, rdfs:domain, c)
	private int domainRuleOnRemoved_1(Set<Statement> removedStatementsThisIteration) throws SailException {
		Set<Statement> inferredStatements = new HashSet<Statement>();
		Set<Statement> allSubpropertyOfStatements = null;
		
		for (Statement removedStatement : removedStatementsThisIteration) {
			if (! isDomainStatement(removedStatement)) {
				continue;
			}
			
			if (allSubpropertyOfStatements == null) {
				allSubpropertyOfStatements = getAllSubpropertyOfStatements();
				if (allSubpropertyOfStatements.isEmpty()) {
					return 0;
				}
			}
			
			for (Statement subpropertyOfStatement : allSubpropertyOfStatements) {
				if (subpropertyOfStatement.getObject().equals(removedStatement.getSubject())) {
					boolean removeStatement = ! subPropertyInheritsDomain(removedStatement.getObject(), subpropertyOfStatement);
					
					if (removeStatement) {
						Statement inferredStatement = valueFactory.createStatement(subpropertyOfStatement.getSubject(), RDFS.DOMAIN, removedStatement.getObject());
						inferredStatements.add(inferredStatement);
					}
				}
			}
		}

		return removeInferred(inferredStatements, SUBPROPERTY_OF_DOMAIN_RULE);
	}

	// removing: (p1, rdfs:subPropertyOf, p2)
	// if existing: (p2, rdfs:domain, c)
	// and no existing: {(p1, rdfs:subPropertyOf, p3), (p3, rdfs:domain, c)}
	// then inferring: (p1, rdfs:domain, c)
	private int domainRuleOnRemoved_2(Set<Statement> removedStatementsThisIteration) throws SailException {
		Set<Statement> inferredStatements = new HashSet<Statement>();
		Set<Statement> allDomainStatements = null;
		
		for (Statement removedStatement : removedStatementsThisIteration) {
			if (! isSubPropertyOfStatement(removedStatement) || ! isResource(removedStatement.getObject())) {
				continue;
			}
			
			if (allDomainStatements == null) {
				allDomainStatements = getAllDomainStatements();
				if (allDomainStatements.isEmpty()) {
					return 0;
				}
			}
			
			for (Statement domainStatement : allDomainStatements) {
				if (domainStatement.getSubject().equals(removedStatement.getObject())) {
					boolean removeStatement = ! subPropertyInheritsDomain(domainStatement.getObject(), removedStatement);
					
					if (removeStatement) {
						Statement inferredStatement = valueFactory.createStatement(removedStatement.getSubject(), RDFS.DOMAIN, domainStatement.getObject());
						inferredStatements.add(inferredStatement);
					}
				}
			}
		}

		return removeInferred(inferredStatements, SUBPROPERTY_OF_DOMAIN_RULE);
	}
	
	// adding: (p2, rdfs:range, c)
	// if existing: (p1, rdfs:subPropertyOf, p2)
	// then inferring: (p1, rdfs:range, c)
	private int rangeRuleOnAdded_1(Set<Statement> addedStatementsThisIteration) throws SailException {
		Set<Statement> inferredStatements = new HashSet<Statement>();
		Set<Statement> allSubpropertyOfStatements = null;
		
		for (Statement addedStatement : addedStatementsThisIteration) {
			if (! isRangeStatement(addedStatement)) {
				continue;
			}
			
			if (allSubpropertyOfStatements == null) {
				allSubpropertyOfStatements = getAllSubpropertyOfStatements();
				if (allSubpropertyOfStatements.isEmpty()) {
					return 0;
				}
			} 
			
			for (Statement subpropertyOfStatement : allSubpropertyOfStatements) {
				if (subpropertyOfStatement.getObject().equals(addedStatement.getSubject())) {
					Statement inferredStatement = valueFactory.createStatement(subpropertyOfStatement.getSubject(), RDFS.RANGE, addedStatement.getObject());
					inferredStatements.add(inferredStatement);
				}
			}
		}

		return addInferred(inferredStatements, SUBPROPERTY_OF_RANGE_RULE);
	}

	// adding: (p1, rdfs:subPropertyOf, p2)
	// if existing: (p2, rdfs:range, c)
	// then inferring: (p1, rdfs:range, c)
	private int rangeRuleOnAdded_2(Set<Statement> addedStatementsThisIteration) throws SailException {
		Set<Statement> inferredStatements = new HashSet<Statement>();
		Set<Statement> allRangeStatements = null;
		
		for (Statement addedStatement : addedStatementsThisIteration) {
			if (! isSubPropertyOfStatement(addedStatement) || ! isResource(addedStatement.getObject())) {
				continue;
			}
			
			if (allRangeStatements == null) {
				allRangeStatements = getAllRangeStatements();
				if (allRangeStatements.isEmpty()) {
					return 0;
				}
			}
			
			for (Statement rangeStatement : allRangeStatements) {
				if (rangeStatement.getSubject().equals(addedStatement.getObject())) {
					Statement inferredStatement = valueFactory.createStatement(addedStatement.getSubject(), RDFS.RANGE, rangeStatement.getObject());
					inferredStatements.add(inferredStatement);
				}
			}
		}

		return addInferred(inferredStatements, SUBPROPERTY_OF_RANGE_RULE);
	}

	// removing: (p2, rdfs:range, c)
	// if existing: (p1, rdfs:subPropertyOf, p2)
	// and no existing: {(p1, rdfs:subPropertyOf, p3), (p3, rdfs:range, c)}
	// then removing inferred: (p1, rdfs:range, c)
	private int rangeRuleOnRemoved_1(Set<Statement> removedStatementsThisIteration) throws SailException {
		Set<Statement> inferredStatements = new HashSet<Statement>();
		Set<Statement> allSubpropertyOfStatements = null;
		
		for (Statement removedStatement : removedStatementsThisIteration) {
			if (! isRangeStatement(removedStatement)) {
				continue;
			}
			
			if (allSubpropertyOfStatements == null) {
				allSubpropertyOfStatements = getAllSubpropertyOfStatements();
				if (allSubpropertyOfStatements.isEmpty()) {
					return 0;
				}
			} 
			
			for (Statement subpropertyOfStatement : allSubpropertyOfStatements) {
				if (subpropertyOfStatement.getObject().equals(removedStatement.getSubject())) {
					boolean removeStatement = ! subPropertyInheritsRange(removedStatement.getObject(), subpropertyOfStatement);
					
					if (removeStatement) {
						Statement inferredStatement = valueFactory.createStatement(subpropertyOfStatement.getSubject(), RDFS.RANGE, removedStatement.getObject());
						inferredStatements.add(inferredStatement);
					}
				}
			}
		}

		return removeInferred(inferredStatements, SUBPROPERTY_OF_RANGE_RULE);
	}
	
	// removing: (p1, rdfs:subPropertyOf, p2)
	// if existing: (p2, rdfs:range, c)
	// and no existing: {(p1, rdfs:subPropertyOf, p3), (p3, rdfs:range, c)}
	// then inferring: (p1, rdfs:range, c)
	private int rangeRuleOnRemoved_2(Set<Statement> removedStatementsThisIteration) throws SailException {
		Set<Statement> inferredStatements = new HashSet<Statement>();
		Set<Statement> allRangeStatements = null;		
		
		for (Statement removedStatement : removedStatementsThisIteration) {
			if (! isSubPropertyOfStatement(removedStatement) || ! isResource(removedStatement.getObject())) {
				continue;
			}
			
			if (allRangeStatements == null) {
				allRangeStatements = getAllRangeStatements();
				if (allRangeStatements.isEmpty()) {
					return 0;
				}
			}
			
			for (Statement rangeStatement : allRangeStatements) {
				if (rangeStatement.getSubject().equals(removedStatement.getObject())) {
					boolean removeStatement = ! subPropertyInheritsRange(rangeStatement.getObject(), removedStatement);
					
					if (removeStatement) {
						Statement inferredStatement = valueFactory.createStatement(removedStatement.getSubject(), RDFS.RANGE, rangeStatement.getObject());
						inferredStatements.add(inferredStatement);
					}
				}
			}
		}

		return removeInferred(inferredStatements, SUBPROPERTY_OF_RANGE_RULE);
	}
	
	private boolean subPropertyInheritsDomain(Value domain, Statement subPropertyOfStatement) throws SailException {
		boolean inheritsDomain = false;
		
		Resource subProperty = subPropertyOfStatement.getSubject();
		CloseableIteration<? extends Statement, SailException> results = connection.getStatements(subProperty, RDFS.SUBPROPERTYOF, null, true);
		while (results.hasNext()) {
			Statement result = results.next();
			if (result.equals(subPropertyOfStatement) || ! isResource(result.getObject())) {
				continue;
			}
			
			CloseableIteration<? extends Statement, SailException> domainStatements = connection.getStatements((Resource) result.getObject(), RDFS.DOMAIN, null, true);
			while (domainStatements.hasNext()) {
				Statement domainStatement = domainStatements.next();
				if (domainStatement.getObject().equals(domain)) {
					inheritsDomain = true;
					break;
				}						
			}
			
			if (inheritsDomain) {
				break;
			}
		}
		
		return inheritsDomain;
	}
	
	private boolean subPropertyInheritsRange(Value range, Statement subPropertyOfStatement) throws SailException {
		boolean inheritsRange = false;
		
		Resource subProperty = subPropertyOfStatement.getSubject();
		CloseableIteration<? extends Statement, SailException> results = connection.getStatements(subProperty, RDFS.SUBPROPERTYOF, null, true);
		while (results.hasNext()) {
			Statement result = results.next();
			if (result.equals(subPropertyOfStatement) || ! isResource(result.getObject())) {
				continue;
			}
			
			CloseableIteration<? extends Statement, SailException> rangeStatements = connection.getStatements((Resource) result.getObject(), RDFS.RANGE, null, true);
			while (rangeStatements.hasNext()) {
				Statement rangeStatement = rangeStatements.next();
				if (rangeStatement.getObject().equals(range)) {
					inheritsRange = true;
					break;
				}						
			}
			
			if (inheritsRange) {
				break;
			}
		}
		
		return inheritsRange;
	}
	
}
