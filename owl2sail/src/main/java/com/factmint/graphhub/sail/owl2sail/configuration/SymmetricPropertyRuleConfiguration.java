package com.factmint.graphhub.sail.owl2sail.configuration;

public class SymmetricPropertyRuleConfiguration {

	private boolean applySymmetricPropertyRule = true;
	
	public SymmetricPropertyRuleConfiguration() {
	}
	
	public SymmetricPropertyRuleConfiguration(boolean applySymmetricPropertyRule) {
		this.applySymmetricPropertyRule = applySymmetricPropertyRule;
	}

	public boolean isApplySymmetricPropertyRule() {
		return applySymmetricPropertyRule;
	}

	public void setApplySymmetricPropertyRule(boolean applySymmetricPropertyRule) {
		this.applySymmetricPropertyRule = applySymmetricPropertyRule;
	}
	
}
