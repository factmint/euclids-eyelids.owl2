package com.factmint.graphhub.sail.owl2sail.configuration;

public class RdfPropertyRuleConfiguration {

	private boolean applyRdfPropertyRule = true;
	
	public RdfPropertyRuleConfiguration() {
	}
	
	public RdfPropertyRuleConfiguration(boolean applyRdfPropertyRule) {
		this.setApplyRdfPropertyRule(applyRdfPropertyRule);
	}

	public boolean isApplyRdfPropertyRule() {
		return applyRdfPropertyRule;
	}

	public void setApplyRdfPropertyRule(boolean applyRdfPropertyRule) {
		this.applyRdfPropertyRule = applyRdfPropertyRule;
	}

}
