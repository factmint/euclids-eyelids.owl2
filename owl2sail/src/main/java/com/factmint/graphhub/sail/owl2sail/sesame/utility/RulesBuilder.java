package com.factmint.graphhub.sail.owl2sail.sesame.utility;

import static com.factmint.graphhub.sail.owl2sail.sesame.utility.Constants.*;
import static com.factmint.graphhub.sail.owl2sail.sesame.utility.Constants.INVERSE_OF_RULE;
import static com.factmint.graphhub.sail.owl2sail.sesame.utility.Constants.RANGE_RULE;
import static com.factmint.graphhub.sail.owl2sail.sesame.utility.Constants.RDF_PROPERTY_RULE;
import static com.factmint.graphhub.sail.owl2sail.sesame.utility.Constants.REFLEXIVE_PROPERTY_RULE;
import static com.factmint.graphhub.sail.owl2sail.sesame.utility.Constants.RULE_DEPENDENCIES;
import static com.factmint.graphhub.sail.owl2sail.sesame.utility.Constants.SAME_AS_DIFFERENCE_RULE;
import static com.factmint.graphhub.sail.owl2sail.sesame.utility.Constants.SAME_AS_REPLACEMENT_RULE;
import static com.factmint.graphhub.sail.owl2sail.sesame.utility.Constants.SUBCLASS_OF_CLASS_AXIOM_RULE;
import static com.factmint.graphhub.sail.owl2sail.sesame.utility.Constants.SUBCLASS_OF_DOMAIN_RULE;
import static com.factmint.graphhub.sail.owl2sail.sesame.utility.Constants.SUBCLASS_OF_EQUIVALENT_CLASS_RULE;
import static com.factmint.graphhub.sail.owl2sail.sesame.utility.Constants.SUBCLASS_OF_RANGE_RULE;
import static com.factmint.graphhub.sail.owl2sail.sesame.utility.Constants.SUBPROPERTY_OF_DOMAIN_RULE;
import static com.factmint.graphhub.sail.owl2sail.sesame.utility.Constants.SUBPROPERTY_OF_EQUIVALENT_PROPERTY_RULE;
import static com.factmint.graphhub.sail.owl2sail.sesame.utility.Constants.SUBPROPERTY_OF_PROPERTY_AXIOM_RULE;
import static com.factmint.graphhub.sail.owl2sail.sesame.utility.Constants.SUBPROPERTY_OF_RANGE_RULE;
import static com.factmint.graphhub.sail.owl2sail.sesame.utility.Constants.SYMMETRIC_PROPERTY_RULE;
import static com.factmint.graphhub.sail.owl2sail.sesame.utility.Constants.TRANSITIVE_PROPERTY_RULE;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.openrdf.sail.inferencer.InferencerConnection;

import com.factmint.graphhub.sail.owl2sail.configuration.ComplementOfRuleConfiguration;
import com.factmint.graphhub.sail.owl2sail.configuration.DomainRuleConfiguration;
import com.factmint.graphhub.sail.owl2sail.configuration.InverseOfRuleConfiguration;
import com.factmint.graphhub.sail.owl2sail.configuration.Owl2RulesConfiguration;
import com.factmint.graphhub.sail.owl2sail.configuration.RangeRuleConfiguration;
import com.factmint.graphhub.sail.owl2sail.configuration.RdfPropertyRuleConfiguration;
import com.factmint.graphhub.sail.owl2sail.configuration.ReflexivePropertyRuleConfiguration;
import com.factmint.graphhub.sail.owl2sail.configuration.SameAsRuleConfiguration;
import com.factmint.graphhub.sail.owl2sail.configuration.SubClassOfRuleConfiguration;
import com.factmint.graphhub.sail.owl2sail.configuration.SubPropertyOfRuleConfiguration;
import com.factmint.graphhub.sail.owl2sail.configuration.SymmetricPropertyRuleConfiguration;
import com.factmint.graphhub.sail.owl2sail.configuration.TransitivePropertyRuleConfiguration;
import com.factmint.graphhub.sail.owl2sail.rules.ComplementOfRule;
import com.factmint.graphhub.sail.owl2sail.rules.DomainRule;
import com.factmint.graphhub.sail.owl2sail.rules.InverseOfRule;
import com.factmint.graphhub.sail.owl2sail.rules.Owl2Rule;
import com.factmint.graphhub.sail.owl2sail.rules.RangeRule;
import com.factmint.graphhub.sail.owl2sail.rules.RdfPropertyRule;
import com.factmint.graphhub.sail.owl2sail.rules.ReflexivePropertyRule;
import com.factmint.graphhub.sail.owl2sail.rules.SameAsRule;
import com.factmint.graphhub.sail.owl2sail.rules.SubClassOfRule;
import com.factmint.graphhub.sail.owl2sail.rules.SubPropertyOfRule;
import com.factmint.graphhub.sail.owl2sail.rules.SymmetricPropertyRule;
import com.factmint.graphhub.sail.owl2sail.rules.TransitivePropertyRule;

public class RulesBuilder {

	public static Collection<Owl2Rule> instantiateAllRules(InferencerConnection connection, Collection<String> activeRules) {
		Map<String, List<String>> activeRulesDependencies = buildActiveRulesDependencies(activeRules);
		
		ArrayList<Owl2Rule> owl2Rules = new ArrayList<Owl2Rule>();		
		owl2Rules.add(new ReflexivePropertyRule(connection, activeRulesDependencies));
		owl2Rules.add(new SymmetricPropertyRule(connection, activeRulesDependencies));
		owl2Rules.add(new TransitivePropertyRule(connection, activeRulesDependencies));
		owl2Rules.add(new SameAsRule(connection, activeRulesDependencies));
		owl2Rules.add(new SubClassOfRule(connection, activeRulesDependencies));
		owl2Rules.add(new SubPropertyOfRule(connection, activeRulesDependencies));
		owl2Rules.add(new DomainRule(connection, activeRulesDependencies));
		owl2Rules.add(new RangeRule(connection, activeRulesDependencies));
		owl2Rules.add(new RdfPropertyRule(connection, activeRulesDependencies));
		owl2Rules.add(new InverseOfRule(connection, activeRulesDependencies));
		owl2Rules.add(new ComplementOfRule(connection, activeRulesDependencies));
		
		return owl2Rules;
	}
	
	public static Collection<String> getActiveRules(Owl2RulesConfiguration configuration) {
		Set<String> activeRules = new HashSet<String>();

		ReflexivePropertyRuleConfiguration reflexivePropertyRuleConfiguration = configuration.getReflexivePropertyRuleConfiguration();
		if (reflexivePropertyRuleConfiguration.isApplyReflexivePropertyRule()) {
			activeRules.add(REFLEXIVE_PROPERTY_RULE);
		}

		SymmetricPropertyRuleConfiguration symmetricPropertyRuleConfiguration = configuration.getSymmetricPropertyRuleConfiguration();
		if (symmetricPropertyRuleConfiguration.isApplySymmetricPropertyRule()) {
			activeRules.add(SYMMETRIC_PROPERTY_RULE);
		}
		
		TransitivePropertyRuleConfiguration transitivePropertyRuleConfiguration = configuration.getTransitivePropertyRuleConfiguration();
		if (transitivePropertyRuleConfiguration.isApplyTransitivePropertyRule()) {
			activeRules.add(TRANSITIVE_PROPERTY_RULE);
		}
		
		SameAsRuleConfiguration sameAsRuleConfiguration = configuration.getSameAsRuleConfiguration();			
		if (sameAsRuleConfiguration.isApplyReplacementRule()) {
			activeRules.add(SAME_AS_REPLACEMENT_RULE);
		}
		if (sameAsRuleConfiguration.isApplyDifferenceRule()) {
			activeRules.add(SAME_AS_DIFFERENCE_RULE);
		}

		SubClassOfRuleConfiguration subClassOfRuleConfiguration = configuration.getSubClassOfRuleConfiguration();
		if (subClassOfRuleConfiguration.isApplyClassAxiomRule()) {
			activeRules.add(SUBCLASS_OF_CLASS_AXIOM_RULE);
		}
		if (subClassOfRuleConfiguration.isApplyEquivalentClassRule()) {
			activeRules.add(SUBCLASS_OF_EQUIVALENT_CLASS_RULE);
		}
		if (subClassOfRuleConfiguration.isApplyDomainRule()) {
			activeRules.add(SUBCLASS_OF_DOMAIN_RULE);
		}
		if (subClassOfRuleConfiguration.isApplyRangeRule()) {
			activeRules.add(SUBCLASS_OF_RANGE_RULE);
		}
		
		SubPropertyOfRuleConfiguration subPropertyOfRuleConfiguration = configuration.getSubPropertyOfRuleConfiguration();
		if (subPropertyOfRuleConfiguration.isApplyPropertyAxiomRule()) {
			activeRules.add(SUBPROPERTY_OF_PROPERTY_AXIOM_RULE);
		}
		if (subPropertyOfRuleConfiguration.isApplyEquivalentPropertyRule()) {
			activeRules.add(SUBPROPERTY_OF_EQUIVALENT_PROPERTY_RULE);
		}
		if (subPropertyOfRuleConfiguration.isApplyDomainRule()) {
			activeRules.add(SUBPROPERTY_OF_DOMAIN_RULE);
		}
		if (subPropertyOfRuleConfiguration.isApplyRangeRule()) {
			activeRules.add(SUBPROPERTY_OF_RANGE_RULE);
		}
		
		DomainRuleConfiguration domainRuleConfiguration = configuration.getDomainRuleConfiguration();
		if (domainRuleConfiguration.isApplyDomainRule()) {
			activeRules.add(DOMAIN_RULE);
		}
		
		RangeRuleConfiguration rangeRuleConfiguration = configuration.getRangeRuleConfiguration();
		if (rangeRuleConfiguration.isApplyRangeRule()) {
			activeRules.add(RANGE_RULE);
		}
		
		RdfPropertyRuleConfiguration rdfPropertyRuleConfiguration = configuration.getRdfPropertyRuleConfiguration();
		if (rdfPropertyRuleConfiguration.isApplyRdfPropertyRule()) {
			activeRules.add(RDF_PROPERTY_RULE);
		}

		InverseOfRuleConfiguration inverseOfRuleConfiguration = configuration.getInverseOfRuleConfiguration();
		if (inverseOfRuleConfiguration.isApplyInverseOf()) {
			activeRules.add(INVERSE_OF_RULE);
		}
		
		ComplementOfRuleConfiguration complementOfRuleConfiguration = configuration.getComplementOfRuleConfiguration();
		if (complementOfRuleConfiguration.isApplyComplementOfRule()) {
			activeRules.add(COMPLEMENT_OF_RULE);
		}
		
		return activeRules;
	}
	
	private static Map<String, List<String>> buildActiveRulesDependencies(Collection<String> activeRules) {
		Map<String, List<String>> activeRulesDependencies = new HashMap<String, List<String>>();
		for (String activeRule : activeRules) {
			List<String> defaultRuleDependencies = RULE_DEPENDENCIES.get(activeRule);
			
			List<String> customRuleDependencies = new ArrayList<String>();
			for (String dependency : defaultRuleDependencies) {
				if (activeRules.contains(dependency)) {
					customRuleDependencies.add(dependency);
				}
			}
			
			if (! customRuleDependencies.isEmpty()) {
				activeRulesDependencies.put(activeRule, customRuleDependencies);
			}
		}
		return activeRulesDependencies;
	}
	
}
