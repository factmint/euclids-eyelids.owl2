package com.factmint.graphhub.sail.owl2sail.rules;

import static com.factmint.graphhub.sail.owl2sail.sesame.utility.Constants.SAME_AS_DIFFERENCE_RULE;
import static com.factmint.graphhub.sail.owl2sail.sesame.utility.Constants.SAME_AS_REPLACEMENT_RULE;
import info.aduna.iteration.CloseableIteration;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.openrdf.model.Resource;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.sail.SailException;
import org.openrdf.sail.inferencer.InferencerConnection;

import com.factmint.graphhub.sail.owl2sail.axioms.OWL2;
import com.factmint.graphhub.sail.owl2sail.exception.SameAsRuleException;

public class SameAsRule extends Owl2Rule {

	public SameAsRule(InferencerConnection connection, Map<String, List<String>> activeRulesDependencies) {
		super(connection, activeRulesDependencies);
	}
	
	@Override
	public Collection<String> apply(Set<Statement> addedStatementsThisIteration, Set<Statement> removedStatementsThisIteration, Set<String> rulesToRun) throws SailException {
		Collection<String> rulesThatMadeChanges = new ArrayList<String>();
			
		if (rulesToRun.contains(SAME_AS_REPLACEMENT_RULE)) {
			int count = sameAsReplacementRule(addedStatementsThisIteration, removedStatementsThisIteration);
			logger.debug("{} statement changes after applying {}.", count, SAME_AS_REPLACEMENT_RULE);
			if (count > 0) {
				rulesThatMadeChanges.add(SAME_AS_REPLACEMENT_RULE);
			}
		}
		
		if (rulesToRun.contains(SAME_AS_DIFFERENCE_RULE)) {
			try {
				sameAsDifferenceRule(addedStatementsThisIteration);
			} catch (SameAsRuleException e) {
				throw new SailException(e);
			}
		}
		
		return buildRulesToRunNextIteration(rulesThatMadeChanges);
	}

	private int sameAsReplacementRule(Set<Statement> addedStatementsThisIteration, Set<Statement> removedStatementsThisIteration) throws SailException {
		int count = 0;
		if (addedStatementsThisIteration != null) {
			count += replacementRuleOnAdded_1(addedStatementsThisIteration);
			count += replacementRuleOnAdded_2(addedStatementsThisIteration);
		}
		if (removedStatementsThisIteration != null) {
			count += replacementRuleOnRemoved_1(removedStatementsThisIteration);
			count += replacementRuleOnRemoved_2(removedStatementsThisIteration);
		}
		return count;
	}

	private void sameAsDifferenceRule(Set<Statement> addedStatementsThisIteration) throws SameAsRuleException, SailException {
		if (addedStatementsThisIteration != null) {
			differenceRuleDirectContradictionOnAdded_1(addedStatementsThisIteration);
			differenceRuleDirectContradictionOnAdded_2(addedStatementsThisIteration);
			differenceRuleAllDifferentOnAdded_1(addedStatementsThisIteration);
			differenceRuleAllDifferentOnAdded_2(addedStatementsThisIteration);
			differenceRuleAllDifferentOnAdded_3(addedStatementsThisIteration);
			differenceRuleAllDifferentOnAdded_4(addedStatementsThisIteration);
		}
	}

	// adding: (s, p, o)
	// if existing: (s, owl:sameAs, s')
	// then inferring: (s', p, o)
	// if existing: (p, owl:sameAs, p')
	// then inferring: (s, p', o)
	// if existing: (o, owl:sameAs, o')
	// then inferring: (s, p, o')
	private int replacementRuleOnAdded_1(Set<Statement> addedStatementsThisIteration) throws SailException {
		Set<Statement> inferredStatements = new HashSet<Statement>();
		
		Set<Statement> allSameAsStatements = null;
		
		for (Statement addedStatement : addedStatementsThisIteration) {
			if (addedStatement.getPredicate().equals(OWL2.SAMEAS)) {
				continue;
			}
			
			if (allSameAsStatements == null) {
				allSameAsStatements = getAllNonReflexiveSameAsStatements();
				if (allSameAsStatements.isEmpty()) {
					return 0;
				}
			}
			
			inferredStatements.addAll(getEquivalentStatementsForSubject(addedStatement, allSameAsStatements));
			
			inferredStatements.addAll(getEquivalentStatementsForPredicate(addedStatement, allSameAsStatements));
			
			if (isResource(addedStatement.getObject())) {
				inferredStatements.addAll(getEquivalentStatementsForObject(addedStatement, allSameAsStatements));
			}
		}

		return addInferred(inferredStatements, SAME_AS_REPLACEMENT_RULE);
	}

	// adding: (e, owl:sameAs, e')
	// if existing: {(e, p, o), (s, e, o), (s, p, e)}
	// then inferring: {(e', p, o), (s, e', o), (s, p, e')}
	private int replacementRuleOnAdded_2(Set<Statement> addedStatementsThisIteration) throws SailException {
		Set<Statement> inferredStatements = new HashSet<Statement>();

		for (Statement addedStatement : addedStatementsThisIteration) {
			if (addedStatement.getPredicate().equals(OWL2.SAMEAS)) {
				if (! isValidSameAsStatement(addedStatement)) {
					continue;
				}
	
				inferredStatements.addAll(getSynonymousStatementsForSameAsSubject(addedStatement));
				inferredStatements.addAll(getSynonymousStatementsForSameAsObject(addedStatement));
			}
		}

		return addInferred(inferredStatements, SAME_AS_REPLACEMENT_RULE);
	}
	
	// removing: (s, p, o)
	// if existing: (s, owl:sameAs, s'), (s', p, o)
	// then removing inferred: (s', p, o)
	// if existing: (p, owl:sameAs, p'), (s, p', o)
	// then removing inferred: (s, p', o)
	// if existing: (o, owl:sameAs, o'), (s, p, o')
	// then removing inferred: (s, p, o')
	private int replacementRuleOnRemoved_1(Set<Statement> removedStatementsThisIteration) throws SailException {
		Set<Statement> inferredStatements = new HashSet<Statement>();

		Set<Statement> allSameAsStatements = getAllNonReflexiveSameAsStatements();
		if (allSameAsStatements.isEmpty()) {
			return 0;
		}
		
		for (Statement removedStatement : removedStatementsThisIteration) {
			if (! removedStatement.getPredicate().equals(OWL2.SAMEAS)) {
				inferredStatements.addAll(getEquivalentStatementsForSubject(removedStatement, allSameAsStatements));
				
				inferredStatements.addAll(getEquivalentStatementsForPredicate(removedStatement, allSameAsStatements));
				
				if (isResource(removedStatement.getObject())) {
					inferredStatements.addAll(getEquivalentStatementsForObject(removedStatement, allSameAsStatements));
				}
			}
		}

		return removeInferred(inferredStatements, SAME_AS_REPLACEMENT_RULE);
	}
	
	// removing: (e, owl:sameAs, e')
	// if existing: {(e, p, o), (s, e, o), (s, p, e)}, {(e', p, o), (s, e', o), (s, p, e')}
	// then removing inferred: {(e', p, o), (s, e', o), (s, p, e')}
	private int replacementRuleOnRemoved_2(Set<Statement> removedStatementsThisIteration) throws SailException {
		Set<Statement> inferredStatements = new HashSet<Statement>();

		for (Statement removedStatement : removedStatementsThisIteration) {
			if (removedStatement.getPredicate().equals(OWL2.SAMEAS)) {
				if (isReflexiveStatement(removedStatement) || ! isValidSameAsStatement(removedStatement)) {
					continue;
				}
	
				inferredStatements.addAll(getSynonymousStatementsForSameAsSubject(removedStatement));
				inferredStatements.addAll(getSynonymousStatementsForSameAsObject(removedStatement));
			}
		}

		return removeInferred(inferredStatements, SAME_AS_REPLACEMENT_RULE);
	}

	// adding: (e, owl:sameAs, e')
	// if existing: (e, owl:differentFrom, e')
	// then: throwing exception
	private void differenceRuleDirectContradictionOnAdded_1(Set<Statement> addedStatementsThisIteration) throws SameAsRuleException, SameAsRuleException, SailException {
		for (Statement addedStatement : addedStatementsThisIteration) {
			if (addedStatement.getPredicate().equals(OWL2.SAMEAS) && hasStatement(addedStatement.getSubject(), OWL2.DIFFERENTFROM, addedStatement.getObject(), true)) {
				logger.debug("Applying {}, throwing exception.", SAME_AS_DIFFERENCE_RULE);	
				
				throw new SameAsRuleException(String.format("%s exception. Cannot add (%s, %s, %s), because statement (%s, %s, %s) exists.",
						SAME_AS_DIFFERENCE_RULE,
						addedStatement.getSubject(), addedStatement.getPredicate(), addedStatement.getObject(),
						addedStatement.getSubject(), OWL2.DIFFERENTFROM, addedStatement.getObject()));
			} 
		}
	}
	
	// adding: (e, owl:differentFrom, e')
	// if existing: (e, owl:sameAs, e')
	// then: throwing exception
	private void differenceRuleDirectContradictionOnAdded_2(Set<Statement> addedStatementsThisIteration) throws SameAsRuleException, SameAsRuleException, SailException {
		for (Statement addedStatement : addedStatementsThisIteration) {
			if (addedStatement.getPredicate().equals(OWL2.DIFFERENTFROM) && hasStatement(addedStatement.getSubject(), OWL2.SAMEAS, addedStatement.getObject(), true)) {
				logger.debug("Applying {}, throwing exception.", SAME_AS_DIFFERENCE_RULE);
				
				throw new SameAsRuleException(String.format("%s exception. Cannot add (%s, %s, %s), because statement (%s, %s, %s) exists.",
						SAME_AS_DIFFERENCE_RULE,
						addedStatement.getSubject(), addedStatement.getPredicate(), addedStatement.getObject(),
						addedStatement.getSubject(), OWL2.SAMEAS, addedStatement.getObject()));
			}
		}
	}

	// adding: (s, rdf:type, owl:AllDifferent)
	// if existing: (s, owl:members, o) or (s, owl:distinctMembers, o)
	// and: (o, listOf, {o_1, o_2, ... , o_n})
	// and: (o_i, owl:sameAs, o_j)
	// then: throwing exception
	private void differenceRuleAllDifferentOnAdded_1(Set<Statement> addedStatementsThisIteration) throws SameAsRuleException, SameAsRuleException, SailException {
		for (Statement addedStatement : addedStatementsThisIteration) {
			if (isAllDifferentStatement(addedStatement)) {
				Collection<Resource> membersContainers = identifyMembersContainers(addedStatement.getSubject());
				if (membersContainers.isEmpty()) {
					continue;
				}

				for (Resource membersContainer : membersContainers) {
					Collection<URI> containerElements = identifyMembers(membersContainer);
					if (containerElements.isEmpty()) {
						continue;
					}
	
					if(! areMembersDifferent(containerElements)) {
						logger.debug("Applying {}, throwing exception.", SAME_AS_DIFFERENCE_RULE);
	
						throw new SameAsRuleException(String.format("%s exception. Cannot add (%s, %s, %s)",
								SAME_AS_DIFFERENCE_RULE, addedStatement.getSubject(), addedStatement.getPredicate(), addedStatement.getObject()));
					}
				}
			}
		}
	}

	// adding: (s, owl:members, o) or (s, owl:distinctMembers, o)
	// if existing: (s, rdf:type, owl:AllDifferent)
	// and: (o, listOf, {o_1, o_2, ... , o_n})
	// and: (o_i, owl:sameAs, o_j)
	// then: throwing exception
	private void differenceRuleAllDifferentOnAdded_2(Set<Statement> addedStatementsThisIteration) throws SameAsRuleException, SameAsRuleException, SailException {
		for (Statement addedStatement : addedStatementsThisIteration) {
			if (isMembersStatement(addedStatement)) {
				if (hasTypeAllDifferent(addedStatement.getSubject())) {
					Resource membersContainer;
					try {
						membersContainer = valueFactory.createURI(addedStatement.getObject().stringValue());			
					} catch (IllegalArgumentException e) {
						membersContainer = valueFactory.createBNode(addedStatement.getObject().stringValue());			
					}

					Collection<URI> containerElements = identifyMembers(membersContainer);
					if (containerElements.isEmpty()) {
						continue;
					}

					if(! areMembersDifferent(containerElements)) {
						logger.debug("Applying {}, throwing exception.", SAME_AS_DIFFERENCE_RULE);
						
						throw new SameAsRuleException(String.format("%s exception. Cannot add (%s, %s, %s).",
								SAME_AS_DIFFERENCE_RULE, addedStatement.getSubject(), addedStatement.getPredicate(), addedStatement.getObject()));
					}
				} 
			}
		}
	}
	
	// adding: (o, listOf, {o_1, o_2, ... , o_n})
	// if existing: (s, owl:members, o) or (s, owl:distinctMembers, o)
	// and: (s, rdf:type, owl:AllDifferent)
	// and: (o_i, owl:sameAs, o_j)
	// then: throwing exception
	private void differenceRuleAllDifferentOnAdded_3(Set<Statement> addedStatementsThisIteration) throws SameAsRuleException, SameAsRuleException, SailException {
		for (Statement addedStatement : addedStatementsThisIteration) {
			if (isListMembershipStatement(addedStatement)) {
				Collection<Resource> subjects = identifySubjectsLinkedWithContainer(addedStatement.getSubject());
				for (Resource subject : subjects) {
					if (hasTypeAllDifferent(subject)) {
						Collection<URI> containerElements = identifyMembers(addedStatement.getSubject());
						if (containerElements.isEmpty()) {
							continue;
						}
	
						if(! areMembersDifferent(containerElements)) {
							logger.debug("Applying {}, throwing exception.", SAME_AS_DIFFERENCE_RULE);
							
							throw new SameAsRuleException(String.format("%s exception. Cannot add (%s, %s, %s).",
									SAME_AS_DIFFERENCE_RULE, addedStatement.getSubject(), addedStatement.getPredicate(), addedStatement.getObject()));
						}						
					}
				}
			}
		}
	}

	// adding: (o_i, owl:sameAs, o_j)
	// if existing: (o, listOf, {o_i, o_j, ... , o_n})
	// and: (s, owl:members, o) or (s, owl:distinctMembers, o)
	// and: (s, rdf:type, owl:AllDifferent)
	// then: throwing exception
	private void differenceRuleAllDifferentOnAdded_4(Set<Statement> addedStatementsThisIteration) throws SameAsRuleException, SameAsRuleException, SailException {
		for (Statement addedStatement : addedStatementsThisIteration) {
			if (isValidSameAsStatement(addedStatement)) {
				Collection<Resource> commonLists = getCommonLists(addedStatement.getSubject(), 
						valueFactory.createURI(addedStatement.getObject().stringValue()));
				
				for (Resource commonList : commonLists) {
					Collection<Resource> subjects = identifySubjectsLinkedWithContainer(commonList);					
					for (Resource subject : subjects) {
						if(hasTypeAllDifferent(subject)) {
							logger.debug("Applying {}, throwing exception.", SAME_AS_DIFFERENCE_RULE);
							
							throw new SameAsRuleException(String.format("%s exception. Cannot add (%s, %s, %s).",
									SAME_AS_DIFFERENCE_RULE, addedStatement.getSubject(), addedStatement.getPredicate(), addedStatement.getObject()));
						}
					}
				}
			}
		}
	}

	private Set<Statement> getEquivalentStatementsForSubject(Statement statement, Set<Statement> allSameAsStatements) throws SailException {
		Set<Statement> equivalentStatements = new HashSet<Statement>();
		
		for (Statement sameAsStatement : allSameAsStatements) {
			if (sameAsStatement.getSubject().equals(statement.getSubject())) {
				if (isResource(sameAsStatement.getObject())) {
					Statement equivalentStatement = valueFactory.createStatement((Resource) sameAsStatement.getObject(), statement.getPredicate(), statement.getObject(), sameAsStatement.getContext());
					equivalentStatements.add(equivalentStatement);
				}
			} else if (sameAsStatement.getObject().equals(statement.getSubject())) {
				Statement equivalentStatement = valueFactory.createStatement(sameAsStatement.getSubject(), statement.getPredicate(), statement.getObject(), sameAsStatement.getContext());
				equivalentStatements.add(equivalentStatement);
			}			
		}

		return equivalentStatements;
	}

	private Set<Statement> getEquivalentStatementsForPredicate(Statement statement, Set<Statement> allSameAsStatements) throws SailException {
		Set<Statement> equivalentStatements = new HashSet<Statement>();
		
		for (Statement sameAsStatement : allSameAsStatements) {
			if (sameAsStatement.getSubject().equals(statement.getPredicate())) {
				if (isURI(sameAsStatement.getObject())) {
					Statement equivalentStatement = valueFactory.createStatement(statement.getSubject(), (URI) sameAsStatement.getObject(), statement.getObject(), sameAsStatement.getContext());
					equivalentStatements.add(equivalentStatement);
				}
			} else if (sameAsStatement.getObject().equals(statement.getPredicate())) {
				if (isURI(sameAsStatement.getSubject())) {
					Statement equivalentStatement = valueFactory.createStatement(statement.getSubject(), (URI) sameAsStatement.getSubject(), statement.getObject(), sameAsStatement.getContext());
					equivalentStatements.add(equivalentStatement);
				}
			}			
		}

		return equivalentStatements;
	}

	private Set<Statement> getEquivalentStatementsForObject(Statement statement, Set<Statement> allSameAsStatements) throws SailException {
		Set<Statement> equivalentStatements = new HashSet<Statement>();
		
		for (Statement sameAsStatement : allSameAsStatements) {
			if (sameAsStatement.getSubject().equals(statement.getSubject())) {
				Statement equivalentStatement = valueFactory.createStatement(statement.getSubject(), statement.getPredicate(), sameAsStatement.getObject(), sameAsStatement.getContext());
				equivalentStatements.add(equivalentStatement);
			} else if (sameAsStatement.getObject().equals(statement.getSubject())) {
				Statement equivalentStatement = valueFactory.createStatement(statement.getSubject(), statement.getPredicate(), sameAsStatement.getSubject(), sameAsStatement.getContext());
				equivalentStatements.add(equivalentStatement);
			}			
		}

		return equivalentStatements;
	}

	private Set<Statement> getSynonymousStatementsForSameAsSubject(Statement statement) throws SailException {
		Set<Statement> synonymousStatements = new HashSet<Statement>();

		URI addedStatementSubject = (URI) statement.getSubject();
		URI addedStatementObject = (URI) statement.getObject();

		CloseableIteration<? extends Statement, SailException> existingStatements;

		existingStatements = connection.getStatements(addedStatementSubject, null, null, true);
		while (existingStatements.hasNext()) {
			Statement existingStatementWithSubject = existingStatements.next();
			if (! existingStatementWithSubject.equals(statement) 
					&& ! isListMembershipStatement(existingStatementWithSubject)) {
				Statement synonymousStatement = valueFactory.createStatement(addedStatementObject, existingStatementWithSubject.getPredicate(), existingStatementWithSubject.getObject(), existingStatementWithSubject.getContext());
				synonymousStatements.add(synonymousStatement);
			}
		}

		existingStatements = connection.getStatements(null, addedStatementSubject, null, true);
		while (existingStatements.hasNext()) {
			Statement existingStatementWithPredicate = existingStatements.next();
			if (! existingStatementWithPredicate.equals(statement)) {
				Statement synonymousStatement = valueFactory.createStatement(existingStatementWithPredicate.getSubject(), addedStatementObject, existingStatementWithPredicate.getObject(), existingStatementWithPredicate.getContext()); 
				synonymousStatements.add(synonymousStatement);
			}
		}

		existingStatements = connection.getStatements(null, null, addedStatementSubject, true);
		while (existingStatements.hasNext()) {
			Statement existingStatementWithObject = existingStatements.next();
			if (! existingStatementWithObject.equals(statement) 
					&& ! isListMembershipStatement(existingStatementWithObject)) {
				Statement synonymousStatement = valueFactory.createStatement(existingStatementWithObject.getSubject(), existingStatementWithObject.getPredicate(), addedStatementObject, existingStatementWithObject.getContext());
				synonymousStatements.add(synonymousStatement);
			}
		}

		return synonymousStatements;
	}
	
	private Set<Statement> getSynonymousStatementsForSameAsObject(Statement statement) throws SailException {
		Set<Statement> synonymousStatements = new HashSet<Statement>();

		URI addedStatementSubject = (URI) statement.getSubject();
		URI addedStatementObject = (URI) statement.getObject();

		CloseableIteration<? extends Statement, SailException> existingStatements;

		existingStatements = connection.getStatements(addedStatementObject, null, null, true);
		while (existingStatements.hasNext()) {
			Statement existingStatementWithSubject = existingStatements.next();
			if (! existingStatementWithSubject.equals(statement)
					&& ! isListMembershipStatement(existingStatementWithSubject)) {
				Statement synonymousStatement = valueFactory.createStatement(addedStatementSubject, existingStatementWithSubject.getPredicate(), existingStatementWithSubject.getObject(), existingStatementWithSubject.getContext());
				synonymousStatements.add(synonymousStatement);
			}
		}

		existingStatements = connection.getStatements(null, addedStatementObject, null, true);
		while (existingStatements.hasNext()) {
			Statement existingStatementWithPredicate = existingStatements.next();
			if (! existingStatementWithPredicate.equals(statement)) {
				Statement synonymousStatement = valueFactory.createStatement(existingStatementWithPredicate.getSubject(), addedStatementSubject, existingStatementWithPredicate.getObject(), existingStatementWithPredicate.getContext()); 
				synonymousStatements.add(synonymousStatement);
			}
		}

		existingStatements = connection.getStatements(null, null, addedStatementObject, true);
		while (existingStatements.hasNext()) {
			Statement existingStatementWithObject = existingStatements.next();
			if (! existingStatementWithObject.equals(statement)
					&& ! isListMembershipStatement(existingStatementWithObject)) {
				Statement synonymousStatement = valueFactory.createStatement(existingStatementWithObject.getSubject(), existingStatementWithObject.getPredicate(), addedStatementSubject, existingStatementWithObject.getContext());
				synonymousStatements.add(synonymousStatement);
			}
		}

		return synonymousStatements;
	}

	private Collection<Resource> identifyMembersContainers(Resource subject) throws SailException {
		Collection<Resource> containers = new ArrayList<Resource>();
		
		CloseableIteration<? extends Statement, SailException> existingMembersStatements = connection.getStatements(subject, OWL2.MEMBERS, null, true);
		while (existingMembersStatements.hasNext()) {
			Statement existingMembersStatement = existingMembersStatements.next();	
			Resource containerAsResource;
			try {
				containerAsResource = valueFactory.createURI(existingMembersStatement.getObject().stringValue());			
			} catch (IllegalArgumentException e) {
				containerAsResource = valueFactory.createBNode(existingMembersStatement.getObject().stringValue());			
			}
			containers.add(containerAsResource);
		}
		
		existingMembersStatements = connection.getStatements(subject, OWL2.DISTINCTMEMBERS, null, true);
		while (existingMembersStatements.hasNext()) {
			Statement existingMembersStatement = existingMembersStatements.next();	
			Resource containerAsResource;
			try {
				containerAsResource = valueFactory.createURI(existingMembersStatement.getObject().stringValue());			
			} catch (IllegalArgumentException e) {
				containerAsResource = valueFactory.createBNode(existingMembersStatement.getObject().stringValue());			
			}
			containers.add(containerAsResource);
		}

		return containers;
	}

	private Collection<Resource> identifySubjectsLinkedWithContainer(Resource container) throws SailException {
		Collection<Resource> subjectsLinkedWithContainer = new ArrayList<Resource>();
		
		CloseableIteration<? extends Statement, SailException> existingSubjectStatements = connection.getStatements(null, OWL2.MEMBERS, container, true);
		while (existingSubjectStatements.hasNext()) {
			Statement existingSubjectStatement = existingSubjectStatements.next();		
			Resource subjectAsResource;
			try {
				subjectAsResource = valueFactory.createURI(existingSubjectStatement.getSubject().stringValue());			
			} catch (IllegalArgumentException e) {
				subjectAsResource = valueFactory.createBNode(existingSubjectStatement.getSubject().stringValue());			
			}
			
			subjectsLinkedWithContainer.add(subjectAsResource);
		}
		
		existingSubjectStatements = connection.getStatements(null, OWL2.DISTINCTMEMBERS, container, true);
		while (existingSubjectStatements.hasNext()) {
			Statement existingSubjectStatement = existingSubjectStatements.next();		
			Resource subjectAsResource;
			try {
				subjectAsResource = valueFactory.createURI(existingSubjectStatement.getSubject().stringValue());			
			} catch (IllegalArgumentException e) {
				subjectAsResource = valueFactory.createBNode(existingSubjectStatement.getSubject().stringValue());			
			}
			
			subjectsLinkedWithContainer.add(subjectAsResource);
		}

		return subjectsLinkedWithContainer;
	}

	private Collection<URI> identifyMembers(Resource container) throws SailException {
		CloseableIteration<? extends Statement, SailException> containerStatements = connection.getStatements(container, null, null, true);

		Collection<URI> containerElements = new ArrayList<URI>();		
		while (containerStatements.hasNext()) {
			Statement containerStatement = containerStatements.next();
			if (isListMembershipStatement(containerStatement)
					&& isURI(containerStatement.getObject())) {
				containerElements.add((URI) containerStatement.getObject());
			}
		}

		return containerElements;
	}

	private boolean areMembersDifferent(Collection<URI> containerElements) throws SailException {
		CloseableIteration<? extends Statement, SailException> sameAsStatements;

		for (URI containerElement : containerElements) {
			sameAsStatements = connection.getStatements(containerElement, OWL2.SAMEAS, null, true);
			while (sameAsStatements.hasNext()) {
				Statement sameAsStatement = sameAsStatements.next();
				if (sameAsStatement.getObject().equals(containerElement)) {
					continue;
				} else {
					if (containerElements.contains(sameAsStatement.getObject())) {
						return false;
					}
				}
			}
		}

		return true;
	}
	
	private Collection<Resource> getCommonLists(Resource leftEntity, Resource rightEntity) throws SailException {
		Collection<Resource> commonLists = new ArrayList<Resource>();
		
		Collection<Resource> leftEntityLists = new ArrayList<Resource>();
		CloseableIteration<? extends Statement, SailException> leftEntityStatements = connection.getStatements(null,  null, leftEntity, true);
		while (leftEntityStatements.hasNext()) {
			Statement leftEntityStatement = leftEntityStatements.next();
			if (isListMembershipStatement(leftEntityStatement)) {
				leftEntityLists.add(leftEntityStatement.getSubject());
			}			
		}
		
		if (leftEntityLists.isEmpty()) {
			return commonLists;
		}
		
		CloseableIteration<? extends Statement, SailException> rightEntityStatements = connection.getStatements(null,  null, rightEntity, true);
		while (rightEntityStatements.hasNext()) {
			Statement rightEntityStatement = rightEntityStatements.next();
			if (isListMembershipStatement(rightEntityStatement)
					&& leftEntityLists.contains(rightEntityStatement.getSubject())) {
				commonLists.add(rightEntityStatement.getSubject());
			}
		}
		
		return commonLists;
	}
	
}
