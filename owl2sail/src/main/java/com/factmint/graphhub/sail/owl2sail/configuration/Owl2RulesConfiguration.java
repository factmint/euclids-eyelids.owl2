package com.factmint.graphhub.sail.owl2sail.configuration;

public class Owl2RulesConfiguration {

	private ReflexivePropertyRuleConfiguration reflexivePropertyRuleConfiguration;
	private SymmetricPropertyRuleConfiguration symmetricPropertyRuleConfiguration;
	private TransitivePropertyRuleConfiguration transitivePropertyRuleConfiguration;
	private SameAsRuleConfiguration sameAsRuleConfiguration;
	private SubClassOfRuleConfiguration subClassOfRuleConfiguration;
	private SubPropertyOfRuleConfiguration subPropertyOfRuleConfiguration;
	private DomainRuleConfiguration domainRuleConfiguration;
	private RangeRuleConfiguration rangeRuleConfiguration;
	private RdfPropertyRuleConfiguration rdfPropertyRuleConfiguration;
	private InverseOfRuleConfiguration inverseOfRuleConfiguration;
	private ComplementOfRuleConfiguration complementOfRuleConfiguration;
	
	public Owl2RulesConfiguration() {
		this.symmetricPropertyRuleConfiguration = new SymmetricPropertyRuleConfiguration();
		this.reflexivePropertyRuleConfiguration = new ReflexivePropertyRuleConfiguration();
		this.transitivePropertyRuleConfiguration = new TransitivePropertyRuleConfiguration();
		this.sameAsRuleConfiguration = new SameAsRuleConfiguration();
		this.subClassOfRuleConfiguration = new SubClassOfRuleConfiguration();
		this.subPropertyOfRuleConfiguration = new SubPropertyOfRuleConfiguration();
		this.domainRuleConfiguration = new DomainRuleConfiguration();
		this.rangeRuleConfiguration = new RangeRuleConfiguration();
		this.rdfPropertyRuleConfiguration = new RdfPropertyRuleConfiguration();
		this.inverseOfRuleConfiguration = new InverseOfRuleConfiguration();
		this.complementOfRuleConfiguration = new ComplementOfRuleConfiguration();
	}

	public ReflexivePropertyRuleConfiguration getReflexivePropertyRuleConfiguration() {
		return reflexivePropertyRuleConfiguration;
	}
	
	public void setReflexivePropertyRuleConfiguration(ReflexivePropertyRuleConfiguration reflexivePropertyRuleConfiguration) {
		this.reflexivePropertyRuleConfiguration = reflexivePropertyRuleConfiguration;
	}
	
	public SymmetricPropertyRuleConfiguration getSymmetricPropertyRuleConfiguration() {
		return symmetricPropertyRuleConfiguration;
	}
	
	public void setSymmetricPropertyRuleConfiguration(SymmetricPropertyRuleConfiguration symmetricPropertyRuleConfiguration) {
		this.symmetricPropertyRuleConfiguration = symmetricPropertyRuleConfiguration;
	}
	
	public TransitivePropertyRuleConfiguration getTransitivePropertyRuleConfiguration() {
		return transitivePropertyRuleConfiguration;
	}
	
	public void setTransitivePropertyRuleConfiguration(TransitivePropertyRuleConfiguration transitivePropertyRuleConfiguration) {
		this.transitivePropertyRuleConfiguration = transitivePropertyRuleConfiguration;
	}
	
	public SameAsRuleConfiguration getSameAsRuleConfiguration() {
		return sameAsRuleConfiguration;
	}

	public void setSameAsRuleConfiguration(SameAsRuleConfiguration sameAsRuleConfiguration) {
		this.sameAsRuleConfiguration = sameAsRuleConfiguration;
	}

	public SubClassOfRuleConfiguration getSubClassOfRuleConfiguration() {
		return subClassOfRuleConfiguration;
	}

	public void setSubClassOfRuleConfiguration(SubClassOfRuleConfiguration subClassOfRuleConfiguration) {
		this.subClassOfRuleConfiguration = subClassOfRuleConfiguration;
	}

	public SubPropertyOfRuleConfiguration getSubPropertyOfRuleConfiguration() {
		return subPropertyOfRuleConfiguration;
	}

	public void setSubPropertyOfRuleConfiguration(SubPropertyOfRuleConfiguration subPropertyOfRuleConfiguration) {
		this.subPropertyOfRuleConfiguration = subPropertyOfRuleConfiguration;
	}

	public DomainRuleConfiguration getDomainRuleConfiguration() {
		return domainRuleConfiguration;
	}

	public void setDomainRuleConfiguration(DomainRuleConfiguration domainRuleConfiguration) {
		this.domainRuleConfiguration = domainRuleConfiguration;
	}

	public RangeRuleConfiguration getRangeRuleConfiguration() {
		return rangeRuleConfiguration;
	}

	public void setRangeRuleConfiguration(RangeRuleConfiguration rangeRuleConfiguration) {
		this.rangeRuleConfiguration = rangeRuleConfiguration;
	}

	public RdfPropertyRuleConfiguration getRdfPropertyRuleConfiguration() {
		return rdfPropertyRuleConfiguration;
	}

	public void setRdfPropertyRuleConfiguration(RdfPropertyRuleConfiguration rdfPropertyRuleConfiguration) {
		this.rdfPropertyRuleConfiguration = rdfPropertyRuleConfiguration;
	}

	public InverseOfRuleConfiguration getInverseOfRuleConfiguration() {
		return inverseOfRuleConfiguration;
	}

	public void setInverseOfRuleConfiguration(
			InverseOfRuleConfiguration inverseOfRuleConfiguration) {
		this.inverseOfRuleConfiguration = inverseOfRuleConfiguration;
	}
	
	public ComplementOfRuleConfiguration getComplementOfRuleConfiguration() {
		return complementOfRuleConfiguration;
	}

	public void setComplementOfRuleConfiguration(ComplementOfRuleConfiguration complementOfRuleConfiguration) {
		this.complementOfRuleConfiguration = complementOfRuleConfiguration;
	}
	
}
