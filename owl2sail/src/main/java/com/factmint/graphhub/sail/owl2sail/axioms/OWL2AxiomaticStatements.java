package com.factmint.graphhub.sail.owl2sail.axioms;

import java.util.HashSet;

import org.openrdf.model.Statement;
import org.openrdf.model.impl.StatementImpl;
import org.openrdf.model.vocabulary.RDF;
import org.openrdf.model.vocabulary.RDFS;

public class OWL2AxiomaticStatements extends HashSet<Statement> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 512274560376442555L;

	public OWL2AxiomaticStatements() {
		//Necessary axiomatic statements for currently implemented rules
		this.add(new StatementImpl(OWL2.ALLDIFFERENT, RDF.TYPE, RDFS.CLASS));
		this.add(new StatementImpl(OWL2.ALLDIFFERENT, RDFS.SUBCLASSOF, RDFS.RESOURCE));
		this.add(new StatementImpl(OWL2.CLASS, RDF.TYPE, RDFS.CLASS));
		this.add(new StatementImpl(OWL2.CLASS, RDFS.SUBCLASSOF, RDFS.CLASS));
		this.add(new StatementImpl(OWL2.THING, RDF.TYPE, OWL2.CLASS));
		this.add(new StatementImpl(OWL2.SAMEAS, RDF.TYPE, RDF.PROPERTY));
		this.add(new StatementImpl(OWL2.SAMEAS, RDF.TYPE, OWL2.REFLEXIVEPROPERTY));
		this.add(new StatementImpl(OWL2.SAMEAS, RDF.TYPE, OWL2.SYMMETRICPROPERTY));
		this.add(new StatementImpl(OWL2.SAMEAS, RDF.TYPE, OWL2.TRANSITIVEPROPERTY));
		this.add(new StatementImpl(OWL2.SAMEAS, RDFS.DOMAIN, OWL2.THING));
		this.add(new StatementImpl(OWL2.SAMEAS, RDFS.RANGE, OWL2.THING));
		this.add(new StatementImpl(RDFS.SUBCLASSOF, RDF.TYPE, OWL2.TRANSITIVEPROPERTY));
		this.add(new StatementImpl(RDFS.SUBPROPERTYOF, RDF.TYPE, OWL2.TRANSITIVEPROPERTY));
		this.add(new StatementImpl(OWL2.DIFFERENTFROM, RDF.TYPE, RDF.PROPERTY));
		this.add(new StatementImpl(OWL2.DIFFERENTFROM, RDFS.DOMAIN, OWL2.THING));
		this.add(new StatementImpl(OWL2.DIFFERENTFROM, RDFS.RANGE, OWL2.THING));
		this.add(new StatementImpl(OWL2.MEMBERS, RDF.TYPE, RDF.PROPERTY));
		this.add(new StatementImpl(OWL2.MEMBERS, RDFS.DOMAIN, RDFS.RESOURCE));
		this.add(new StatementImpl(OWL2.MEMBERS, RDFS.RANGE, RDF.LIST));
		this.add(new StatementImpl(OWL2.DISTINCTMEMBERS, RDF.TYPE, RDF.PROPERTY));
		this.add(new StatementImpl(OWL2.DISTINCTMEMBERS, RDFS.DOMAIN, OWL2.ALLDIFFERENT));
		this.add(new StatementImpl(OWL2.DISTINCTMEMBERS, RDFS.RANGE, RDF.LIST));
		this.add(new StatementImpl(OWL2.SYMMETRICPROPERTY, RDF.TYPE, RDFS.CLASS));
		this.add(new StatementImpl(OWL2.SYMMETRICPROPERTY, RDFS.SUBCLASSOF, OWL2.OBJECTPROPERTY));
		this.add(new StatementImpl(OWL2.ASYMMETRICPROPERTY, RDF.TYPE, RDFS.CLASS));
		this.add(new StatementImpl(OWL2.ASYMMETRICPROPERTY, RDFS.SUBCLASSOF, OWL2.OBJECTPROPERTY));
		this.add(new StatementImpl(OWL2.TRANSITIVEPROPERTY, RDF.TYPE, RDFS.CLASS));
		this.add(new StatementImpl(OWL2.TRANSITIVEPROPERTY, RDFS.SUBCLASSOF, OWL2.OBJECTPROPERTY));
		this.add(new StatementImpl(OWL2.REFLEXIVEPROPERTY, RDF.TYPE, RDFS.CLASS));
		this.add(new StatementImpl(OWL2.REFLEXIVEPROPERTY, RDFS.SUBCLASSOF, OWL2.OBJECTPROPERTY));
		this.add(new StatementImpl(OWL2.IRREFLEXIVEPROPERTY, RDF.TYPE, RDFS.CLASS));
		this.add(new StatementImpl(OWL2.IRREFLEXIVEPROPERTY, RDFS.SUBCLASSOF, OWL2.OBJECTPROPERTY));
		this.add(new StatementImpl(OWL2.FUNCTIONALPROPERTY, RDF.TYPE, RDFS.CLASS));
		this.add(new StatementImpl(OWL2.FUNCTIONALPROPERTY, RDFS.SUBCLASSOF, OWL2.OBJECTPROPERTY));
		this.add(new StatementImpl(OWL2.INVERSEFUNCTIONALPROPERTY, RDF.TYPE, RDFS.CLASS));
		this.add(new StatementImpl(OWL2.INVERSEFUNCTIONALPROPERTY, RDFS.SUBCLASSOF, OWL2.OBJECTPROPERTY));
		this.add(new StatementImpl(OWL2.INVERSEOF, RDF.TYPE, RDF.PROPERTY));
		this.add(new StatementImpl(OWL2.INVERSEOF, RDFS.DOMAIN, OWL2.OBJECTPROPERTY));
		this.add(new StatementImpl(OWL2.INVERSEOF, RDFS.RANGE, OWL2.OBJECTPROPERTY));
		this.add(new StatementImpl(OWL2.INVERSEOF, RDF.TYPE, OWL2.SYMMETRICPROPERTY));
		this.add(new StatementImpl(OWL2.COMPLEMENTOF, RDF.TYPE, RDF.PROPERTY));
		this.add(new StatementImpl(OWL2.COMPLEMENTOF, RDFS.DOMAIN, RDFS.CLASS));
		this.add(new StatementImpl(OWL2.COMPLEMENTOF, RDFS.RANGE, RDFS.CLASS));
		
		// LDP subclasses
		this.add(new StatementImpl(OWL2.LDPRDFSOURCE, RDFS.SUBCLASSOF, OWL2.LDPRESOURCE));
		this.add(new StatementImpl(OWL2.LDPNONRDFSOURCE, RDFS.SUBCLASSOF, OWL2.LDPRESOURCE));
		this.add(new StatementImpl(OWL2.LDPCONTAINER, RDFS.SUBCLASSOF, OWL2.LDPRDFSOURCE));
		this.add(new StatementImpl(OWL2.LDPBASICCONTAINER, RDFS.SUBCLASSOF, OWL2.LDPCONTAINER));
		this.add(new StatementImpl(OWL2.LDPDIRECTCONTAINER, RDFS.SUBCLASSOF, OWL2.LDPCONTAINER));
		this.add(new StatementImpl(OWL2.LDPINDIRECTCONTAINER, RDFS.SUBCLASSOF, OWL2.LDPCONTAINER));
	}
	
}
