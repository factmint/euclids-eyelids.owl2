package com.factmint.graphhub.sail.owl2sail.configuration;

public class SubClassOfRuleConfiguration {

	private boolean applyClassAxiomRule = true;
	private boolean applyEquivalentClassRule = true;
	private boolean applyDomainRule = true;
	private boolean applyRangeRule = true;
	
	public SubClassOfRuleConfiguration() {
	}
	
	public SubClassOfRuleConfiguration(boolean applyClassAxiomRule, boolean applyEquivalentClassRule, boolean applyDomainRule, boolean applyRangeRule) {
		this.applyClassAxiomRule = applyClassAxiomRule;
		this.applyEquivalentClassRule = applyEquivalentClassRule;
		this.applyDomainRule = applyDomainRule;
		this.applyRangeRule = applyRangeRule;
	}

	public boolean isApplyEquivalentClassRule() {
		return applyEquivalentClassRule;
	}

	public void setApplyEquivalentClassRule(boolean applyEquivalentClassRule) {
		this.applyEquivalentClassRule = applyEquivalentClassRule;
	}

	public boolean isApplyDomainRule() {
		return applyDomainRule;
	}

	public void setApplyDomainRule(boolean applyDomainRule) {
		this.applyDomainRule = applyDomainRule;
	}

	public boolean isApplyRangeRule() {
		return applyRangeRule;
	}

	public void setApplyRangeRule(boolean applyRangeRule) {
		this.applyRangeRule = applyRangeRule;
	}

	public boolean isApplyClassAxiomRule() {
		return applyClassAxiomRule;
	}

	public void setApplyClassAxiomRule(boolean applyClassAxiomRule) {
		this.applyClassAxiomRule = applyClassAxiomRule;
	}

}
