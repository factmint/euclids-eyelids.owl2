package com.factmint.graphhub.sail.owl2sail.exception;

public class SameAsRuleException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7334243491122441666L;

	public SameAsRuleException(Exception e) {
		super(e);
	}
	
	public SameAsRuleException(String message) {
		super(message);
	}
	
}
