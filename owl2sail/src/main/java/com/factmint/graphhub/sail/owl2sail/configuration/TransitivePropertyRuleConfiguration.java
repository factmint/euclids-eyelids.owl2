package com.factmint.graphhub.sail.owl2sail.configuration;

public class TransitivePropertyRuleConfiguration {

	private boolean applyTransitivePropertyRule = true;

	public TransitivePropertyRuleConfiguration() {
	}
	
	public TransitivePropertyRuleConfiguration(boolean applyTransitivePropertyRule) {
		this.applyTransitivePropertyRule = applyTransitivePropertyRule;
	}
	
	public boolean isApplyTransitivePropertyRule() {
		return applyTransitivePropertyRule;
	}

	public void setApplyTransitivePropertyRule(boolean applyTransitivePropertyRule) {
		this.applyTransitivePropertyRule = applyTransitivePropertyRule;
	}
	
}
