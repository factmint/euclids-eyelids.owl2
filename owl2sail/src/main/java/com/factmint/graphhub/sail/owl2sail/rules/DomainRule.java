package com.factmint.graphhub.sail.owl2sail.rules;

import static com.factmint.graphhub.sail.owl2sail.sesame.utility.Constants.DOMAIN_RULE;
import info.aduna.iteration.CloseableIteration;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.openrdf.model.Resource;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.Value;
import org.openrdf.model.vocabulary.RDF;
import org.openrdf.model.vocabulary.RDFS;
import org.openrdf.sail.SailException;
import org.openrdf.sail.inferencer.InferencerConnection;

import com.factmint.graphhub.sail.owl2sail.axioms.OWL2;

public class DomainRule extends Owl2Rule {

	public DomainRule(InferencerConnection connection, Map<String, List<String>> activeRulesDependencies) {
		super(connection, activeRulesDependencies);
	}

	@Override
	public Collection<String> apply(Set<Statement> addedStatementsThisIteration, Set<Statement> removedStatementsThisIteration,	Set<String> rulesToRunThisIteration) throws SailException {
		Collection<String> rulesThatMadeChanges = new ArrayList<String>();

		if (rulesToRunThisIteration.contains(DOMAIN_RULE)) {
			int count = domainRule(addedStatementsThisIteration, removedStatementsThisIteration);
			logger.debug("{} statement changes after applying {}.", count, DOMAIN_RULE);
			if (count > 0) {
				rulesThatMadeChanges.add(DOMAIN_RULE);
			}
		}

		return buildRulesToRunNextIteration(rulesThatMadeChanges);
	}

	private int domainRule(Set<Statement> addedStatementsThisIteration,	Set<Statement> removedStatementsThisIteration) throws SailException {
		int count = 0;
		if (addedStatementsThisIteration != null) {
			count += applyOnAdded_1(addedStatementsThisIteration);
			count += applyOnAdded_2(addedStatementsThisIteration);
		}
		if (removedStatementsThisIteration != null) {
			count += applyOnRemoved_1(removedStatementsThisIteration);
			count += applyOnRemoved_2(removedStatementsThisIteration);
		}
		return count;
	}

	// adding: (p, rdfs:domain, c)
	// if existing: (x, p, y)
	// then inferring: (x, rdf:type, c)
	private int applyOnAdded_1(Set<Statement> addedStatementsThisIteration) throws SailException {
		Set<Statement> inferredStatements = new HashSet<Statement>();

		for (Statement addedStatement : addedStatementsThisIteration) {
			if (! isDomainStatement(addedStatement) || ! isURI(addedStatement.getSubject())) {
				continue;
			}

			URI addedStatementProperty = (URI) addedStatement.getSubject();
			Value addedStatementClass = addedStatement.getObject();

			CloseableIteration<? extends Statement, SailException> existingStatements = connection.getStatements(null, addedStatementProperty, null, true);

			while (existingStatements.hasNext()) {
				Statement existingStatement = existingStatements.next();

				Statement inferredStatement = valueFactory.createStatement(existingStatement.getSubject(), RDF.TYPE, addedStatementClass, existingStatement.getContext());
				inferredStatements.add(inferredStatement);
			}
		}

		return addInferred(inferredStatements, DOMAIN_RULE);
	}

	// adding: (x, p, y)
	// if existing: (p, rdfs:domain, c)
	// then inferring: (x, rdf:type, c)
	private int applyOnAdded_2(Set<Statement> addedStatementsThisIteration) throws SailException {
		Set<Statement> inferredStatements = new HashSet<Statement>();

		Set<Statement> allDomainStatements = null;

		for (Statement addedStatement : addedStatementsThisIteration) {
			if (allDomainStatements == null) {
				allDomainStatements = getAllDomainStatements();
				if (allDomainStatements.isEmpty()) {
					return 0;
				}
			}

			URI addedStatementProperty = addedStatement.getPredicate();

			for (Statement domainStatement : allDomainStatements) {
				if (domainStatement.getSubject().equals(addedStatementProperty)) {
					Statement inferredStatement = valueFactory.createStatement(addedStatement.getSubject(), RDF.TYPE, domainStatement.getObject(), domainStatement.getContext());
					inferredStatements.add(inferredStatement);
				}				
			}
		}

		return addInferred(inferredStatements, DOMAIN_RULE);
	}

	// removing: (p, rdfs:domain, c)
	// if existing: (x, p, y)
	// (case 1) and no existing: {(x, p', y'), (p', rdfs:domain, c)}
	// (case 2) and no existing: {(p, rdfs:domain, c'), (c', rdfs:subClassOf, c)}
	// (case 3) and no existing: {(x, p', y'), (p', rdfs:domain, c'), (c', rdfs:subClassOf, c)}
	// (case 4) and no existing: {(x, rdf:type, c'), (c', rdfs:subClassOf, c)}
	// then removing inferred: (x, rdf:type, c)
	private int applyOnRemoved_1(Set<Statement> removedStatementsThisIteration) throws SailException {
		Set<Statement> inferredStatements = new HashSet<Statement>();

		for (Statement removedStatement : removedStatementsThisIteration) {
			if (! isDomainStatement(removedStatement) || ! isURI(removedStatement.getSubject())) {
				continue;
			}

			URI removedStatementProperty = (URI) removedStatement.getSubject();
			Value removedStatementClass = removedStatement.getObject();

			CloseableIteration<? extends Statement, SailException> existingStatements = connection.getStatements(null, removedStatementProperty, null, true);

			while (existingStatements.hasNext()) {
				Statement existingStatement = existingStatements.next();

				boolean needsKeepingFromFirstCase = checkDomainStatementRemovalFirstCase(existingStatement, removedStatement);
				boolean needsKeepingFromSecondCase = checkDomainStatementRemovalSecondCase(existingStatement, removedStatement);
				boolean needsKeepingFromThirdCase = checkDomainStatementRemovalThirdCase(existingStatement, removedStatement);
				
				boolean needsKeepingFromFourthCase = checkSubclassOfStatementRemovalFourthCase(existingStatement, removedStatement);
				
				if (needsKeepingFromFirstCase || needsKeepingFromSecondCase || needsKeepingFromThirdCase || needsKeepingFromFourthCase) {
				} else {
					Statement inferredStatement = valueFactory.createStatement(existingStatement.getSubject(), RDF.TYPE, removedStatementClass, removedStatement.getContext());
					inferredStatements.add(inferredStatement);
				}
			}
		}

		return removeInferred(inferredStatements, DOMAIN_RULE);
	}

	// removing: (p, rdfs:domain, c)
	// existing: (x, p, y)
	// check if existing: {(x, p', y'), (p', rdfs:domain, c)}
	private boolean checkDomainStatementRemovalFirstCase(Statement existingStatement, Statement removedStatement) throws SailException {
		CloseableIteration<? extends Statement, SailException> alternativeDomainStatements = connection.getStatements(null, RDFS.DOMAIN, removedStatement.getObject(), true, removedStatement.getContext());
		while (alternativeDomainStatements.hasNext()) {
			Statement alternativeDomainStatement = alternativeDomainStatements.next();
			if (! isURI(alternativeDomainStatement.getSubject())) {
				continue;
			}

			CloseableIteration<? extends Statement, SailException> results = connection.getStatements(existingStatement.getSubject(), (URI) alternativeDomainStatement.getSubject(), null, true);
			if (results.hasNext()) {
				return true;
			}					
		}
		return false;
	}

	// removing: (p, rdfs:domain, c)
	// existing: (x, p, y)
	// check if existing: {(p, rdfs:domain, c'), (c', rdfs:subClassOf, c)}
	private boolean checkDomainStatementRemovalSecondCase(Statement existingStatement, Statement removedStatement) throws SailException {
		CloseableIteration<? extends Statement, SailException> otherClassStatements = connection.getStatements(existingStatement.getPredicate(), RDFS.DOMAIN, null, true, removedStatement.getContext());
		while (otherClassStatements.hasNext()) {
			Statement otherClassStatement = otherClassStatements.next();
			
			if (! isResource(otherClassStatement.getObject())) {
				continue;
			}
			
			if (hasStatement((Resource) otherClassStatement.getObject(), RDFS.SUBCLASSOF, removedStatement.getObject(), true)) {
				return true;
			}
		}		
		return false;
	}
	
	// removing: (p, rdfs:domain, c)
	// existing: (x, p, y)
	// check if existing: {(x, p', y'), (p', rdfs:domain, c'), (c', rdfs:subClassOf, c)}
	private boolean checkDomainStatementRemovalThirdCase(Statement existingStatement, Statement removedStatement) throws SailException {
		CloseableIteration<? extends Statement, SailException> subclassStatements = connection.getStatements(null, RDFS.SUBCLASSOF, removedStatement.getObject(), true, removedStatement.getContext());
		while (subclassStatements.hasNext()) {
			Statement subclassStatement = subclassStatements.next();
			
			CloseableIteration<? extends Statement, SailException> domainStatements = connection.getStatements(null, RDFS.DOMAIN, subclassStatement.getSubject(), true);
			while (domainStatements.hasNext()) {
				Statement domainStatement = domainStatements.next();
				if (! isURI(domainStatement.getSubject())) {
					continue;
				}
				
				if (hasStatement(existingStatement.getSubject(), (URI) domainStatement.getSubject(), null, true)) {
					return true;
				}
			}
		}		
		return false;
	}
	
	// removing: (x, p, y)
	// if existing: (p, rdfs:domain, c)
	// (case 1) and no existing: {(x, p, y')}
	// (case 2) and no existing: {(x, p', y'), (p', rdfs:domain, c)}
	// (case 3) and no existing: {(x, p', y'), (p', rdfs:domain, c'), (c', rdfs:subClassOf, c)}
	// (case 4) and no existing: {(x, rdf:type, c'), (c', rdfs:subClassOf, c)}
	// then removing inferred: (x, rdf:type, c)
	private int applyOnRemoved_2(Set<Statement> removedStatementsThisIteration) throws SailException {
		Set<Statement> inferredStatements = new HashSet<Statement>();

		for (Statement removedStatement : removedStatementsThisIteration) {
			CloseableIteration<? extends Statement, SailException> domainStatements = connection.getStatements(removedStatement.getPredicate(), RDFS.DOMAIN, null, true, removedStatement.getContext());
			while (domainStatements.hasNext()) {
				Statement domainStatement = domainStatements.next();

				boolean needsKeepingFromFirstCase = checkGenericStatementRemovalFirstCase(removedStatement, domainStatement);
				boolean needsKeepingFromSecondCase = checkGenericStatementRemovalSecondCase(removedStatement, domainStatement);
				boolean needsKeepingFromThirdCase = checkGenericStatementRemovalThirdCase(removedStatement, domainStatement);
				boolean needsKeepingFromFourthCase = checkSubclassOfStatementRemovalFourthCase(removedStatement, domainStatement);

				if (needsKeepingFromFirstCase || needsKeepingFromSecondCase || needsKeepingFromThirdCase || needsKeepingFromFourthCase) {
				} else {
					Statement candidateToDelete = valueFactory.createStatement(removedStatement.getSubject(), RDF.TYPE, domainStatement.getObject(), removedStatement.getContext());
					inferredStatements.add(candidateToDelete);
				}
			}
		}

		return removeInferred(inferredStatements, DOMAIN_RULE);
	}

	// removing: (x, p, y)
	// existing: (p, rdfs:domain, c)
	// check if existing: {(x, p, y')}
	private boolean checkGenericStatementRemovalFirstCase(Statement removedStatement, Statement domainStatement) throws SailException {
		Statement candidateToDelete = valueFactory.createStatement(removedStatement.getSubject(), RDF.TYPE, domainStatement.getObject(), removedStatement.getContext());
		Statement typeThing = valueFactory.createStatement(removedStatement.getSubject(), RDF.TYPE, OWL2.THING, removedStatement.getContext());

		Resource domainStatementSubject = domainStatement.getSubject();
		if (isURI(domainStatementSubject)) {
			CloseableIteration<? extends Statement, SailException> statementsWithSameSubjectAndProperty = connection.getStatements(removedStatement.getSubject(), (URI) domainStatementSubject, null, true, removedStatement.getContext());

			while (statementsWithSameSubjectAndProperty.hasNext()) {
				Statement statementWithSameSubjectAndProperty = statementsWithSameSubjectAndProperty.next();
				if (! statementWithSameSubjectAndProperty.equals(candidateToDelete) &&
						! statementWithSameSubjectAndProperty.equals(typeThing)) {
					return true;
				}
			}
		}
		return false;
	}

	// removing: (x, p, y)
	// existing: (p, rdfs:domain, c)
	// check if existing: {(x, p', y'), (p', rdfs:domain, c)}
	private boolean checkGenericStatementRemovalSecondCase(Statement removedStatement, Statement domainStatement) throws SailException {
		CloseableIteration<? extends Statement, SailException> statementsWithSameSubjectAndDifferentProperty = connection.getStatements(removedStatement.getSubject(), null, null, true, removedStatement.getContext());				
		while (statementsWithSameSubjectAndDifferentProperty.hasNext()) {
			Statement statementWithSameSubjectAndDifferentProperty = statementsWithSameSubjectAndDifferentProperty.next();
			if (statementWithSameSubjectAndDifferentProperty.getPredicate().equals(removedStatement.getPredicate())) {
				continue;
			}

			CloseableIteration<? extends Statement, SailException> results = connection.getStatements(statementWithSameSubjectAndDifferentProperty.getPredicate(), RDFS.DOMAIN, domainStatement.getObject(), true, removedStatement.getContext());
			if (results.hasNext()) {
				return true;
			}
		}
		return false;
	}
	
	// removing: (x, p, y)
	// if existing: (p, rdfs:domain, c)
	// check if existing: {(x, p', y'), (p', rdfs:domain, c'), (c', rdfs:subClassOf, c)}
	private boolean checkGenericStatementRemovalThirdCase(Statement removedStatement, Statement domainStatement) throws SailException {
		CloseableIteration<? extends Statement, SailException> subclassOfStatements = connection.getStatements(null, RDFS.SUBCLASSOF, domainStatement.getObject(), true, removedStatement.getContext());
		while (subclassOfStatements.hasNext()) {
			Statement subclassOfStatement = subclassOfStatements.next();
			CloseableIteration<? extends Statement, SailException> otherDomainStatements = connection.getStatements(null, RDFS.DOMAIN, subclassOfStatement.getObject(), true);
			while (otherDomainStatements.hasNext()) {
				Statement otherDomainStatement = otherDomainStatements.next();
				if (! isURI(otherDomainStatement.getSubject())) {
					continue;
				}
				
				if (hasStatement(removedStatement.getSubject(), (URI) otherDomainStatement.getSubject(), null, true)) {
					return true;
				}
			}
		}
		return false;
	}
	
	// removing: (x, p, y)
	// existing: (p, rdfs:domain, c)
	// check if existing: {(x, rdf:type, c'), (c', rdfs:subClassOf, c)}
	private boolean checkSubclassOfStatementRemovalFourthCase(Statement removedStatement, Statement domainStatement) throws SailException {
		CloseableIteration<? extends Statement, SailException> typeStatements = connection.getStatements(removedStatement.getSubject(), RDF.TYPE, null, true, removedStatement.getContext());
		while (typeStatements.hasNext()) {
			Statement typeStatement = typeStatements.next();
			if (! isResource(typeStatement.getObject())) {
				continue;
			}
			
			if (hasStatement((Resource) typeStatement.getObject(), RDFS.SUBCLASSOF, domainStatement.getObject(), true)) {
				return true;
			}
		}		
		return false;
	}
	
}
