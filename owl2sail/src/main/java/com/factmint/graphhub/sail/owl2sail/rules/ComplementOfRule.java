package com.factmint.graphhub.sail.owl2sail.rules;

import static com.factmint.graphhub.sail.owl2sail.sesame.utility.Constants.COMPLEMENT_OF_RULE;
import info.aduna.iteration.CloseableIteration;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.vocabulary.RDF;
import org.openrdf.sail.SailException;
import org.openrdf.sail.inferencer.InferencerConnection;

import com.factmint.graphhub.sail.owl2sail.axioms.OWL2;
import com.factmint.graphhub.sail.owl2sail.exception.ComplementOfRuleException;

public class ComplementOfRule extends Owl2Rule {

	public ComplementOfRule(InferencerConnection connection, Map<String, List<String>> activeRulesDependencies) {
		super(connection, activeRulesDependencies);
	}

	@Override
	public Collection<String> apply(Set<Statement> addedStatementsThisIteration, Set<Statement> removedStatementsThisIteration,	Set<String> rulesToRunThisIteration) throws SailException {
		Collection<String> rulesThatMadeChanges = new ArrayList<String>();
		
		if (rulesToRunThisIteration.contains(COMPLEMENT_OF_RULE)) {
			try {
				complementOfRule(addedStatementsThisIteration, removedStatementsThisIteration);
			} catch (ComplementOfRuleException e) {
				throw new SailException(e);
			}
		}
		
		return buildRulesToRunNextIteration(rulesThatMadeChanges);
	}
	
	private void complementOfRule(Set<Statement> addedStatementsThisIteration, Set<Statement> removedStatementsThisIteration) throws ComplementOfRuleException, SailException {
		if (addedStatementsThisIteration != null) {
			applyOnAdded_1(addedStatementsThisIteration);
			applyOnAdded_2(addedStatementsThisIteration);
			applyOnAdded_3(addedStatementsThisIteration);
		}
	}

	// adding: (c1, owl:complementOf, c2)
	// if existing: {(x, rdf:type, c1), (x, rdf:type, c2)}
	// then: throwing exception
	private void applyOnAdded_1(Set<Statement> addedStatementsThisIteration) throws ComplementOfRuleException, SailException {
		for (Statement addedStatement : addedStatementsThisIteration) {
			if (! isComplementOfStatement(addedStatement)) {
				continue;
			}
			
			CloseableIteration<? extends Statement, SailException> statementsWithTypeSubject = connection.getStatements(null, RDF.TYPE, addedStatement.getSubject(), true);
			while (statementsWithTypeSubject.hasNext()) {
				Statement statementWithTypeSubject = statementsWithTypeSubject.next();
				if (hasStatement(statementWithTypeSubject.getSubject(), RDF.TYPE, addedStatement.getObject(), true)) {
					logger.debug("Applying {}, throwing exception.", COMPLEMENT_OF_RULE);	
					throw new ComplementOfRuleException(String.format("%s exception. Cannot add %s, because statements [ %s, %s ] exist.", COMPLEMENT_OF_RULE,
							addedStatement, statementWithTypeSubject, valueFactory.createStatement(statementWithTypeSubject.getSubject(), RDF.TYPE, addedStatement.getObject()))); 
				}
			}
		}
	}

	// adding: (x, rdf:type, c1)
	// if existing: {(c1, owl:complementOf, c2), (x, rdf:type, c2)}
	// then: throwing exception
	private void applyOnAdded_2(Set<Statement> addedStatementsThisIteration) throws SailException, ComplementOfRuleException {
		for (Statement addedStatement : addedStatementsThisIteration) {
			if (! isTypeStatement(addedStatement) || ! isURI(addedStatement.getObject())) {
				continue;
			}
			
			CloseableIteration<? extends Statement, SailException> complementOfStatements = connection.getStatements((URI) addedStatement.getObject(), OWL2.COMPLEMENTOF, null, true);
			while (complementOfStatements.hasNext()) {
				Statement complementOfStatement = complementOfStatements.next();
				if (hasStatement(addedStatement.getSubject(), RDF.TYPE, complementOfStatement.getObject(), true)) {
					logger.debug("Applying {}, throwing exception.", COMPLEMENT_OF_RULE);	
					throw new ComplementOfRuleException(String.format("%s exception. Cannot add %s, because statements [ %s, %s ] exist.", COMPLEMENT_OF_RULE,
							addedStatement, complementOfStatement, valueFactory.createStatement(addedStatement.getSubject(), RDF.TYPE, complementOfStatement.getObject())));  
				}
			}
		}
	}

	// adding: (x, rdf:type, c2)
	// if existing: {(c1, owl:complementOf, c2), (x, rdf:type, c1)}
	// then: throwing exception
	private void applyOnAdded_3(Set<Statement> addedStatementsThisIteration) throws SailException, ComplementOfRuleException {
		for (Statement addedStatement : addedStatementsThisIteration) {
			if (! isTypeStatement(addedStatement)) {
				continue;
			}
			
			CloseableIteration<? extends Statement, SailException> complementOfStatements = connection.getStatements(null, OWL2.COMPLEMENTOF, addedStatement.getObject(), true);
			while (complementOfStatements.hasNext()) {
				Statement complementOfStatement = complementOfStatements.next();
				if (hasStatement(addedStatement.getSubject(), RDF.TYPE, complementOfStatement.getSubject(), true)) {
					logger.debug("Applying {}, throwing exception.", COMPLEMENT_OF_RULE);	
					throw new ComplementOfRuleException(String.format("%s exception. Cannot add %s, because statements [ %s, %s ] exist.", COMPLEMENT_OF_RULE,
							addedStatement, complementOfStatement, valueFactory.createStatement(addedStatement.getSubject(), RDF.TYPE, complementOfStatement.getSubject())));  
				}
			}
		}
	}

}
