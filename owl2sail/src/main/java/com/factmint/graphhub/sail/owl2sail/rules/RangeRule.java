package com.factmint.graphhub.sail.owl2sail.rules;

import static com.factmint.graphhub.sail.owl2sail.sesame.utility.Constants.RANGE_RULE;
import info.aduna.iteration.CloseableIteration;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.openrdf.model.Resource;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.Value;
import org.openrdf.model.vocabulary.RDF;
import org.openrdf.model.vocabulary.RDFS;
import org.openrdf.sail.SailException;
import org.openrdf.sail.inferencer.InferencerConnection;

import com.factmint.graphhub.sail.owl2sail.axioms.OWL2;

public class RangeRule extends Owl2Rule {

	public RangeRule(InferencerConnection connection, Map<String, List<String>> activeRulesDependencies) {
		super(connection, activeRulesDependencies);
	}

	@Override
	public Collection<String> apply(Set<Statement> addedStatementsThisIteration, Set<Statement> removedStatementsThisIteration,	Set<String> rulesToRunThisIteration) throws SailException {
		Collection<String> rulesThatMadeChanges = new ArrayList<String>();

		if (rulesToRunThisIteration.contains(RANGE_RULE)) {
			int count = rangeRule(addedStatementsThisIteration, removedStatementsThisIteration);
			logger.debug("{} statement changes after applying {}.", count, RANGE_RULE);
			if (count > 0) {
				rulesThatMadeChanges.add(RANGE_RULE);
			}
		}

		return buildRulesToRunNextIteration(rulesThatMadeChanges);
	}

	private int rangeRule(Set<Statement> addedStatementsThisIteration,	Set<Statement> removedStatementsThisIteration) throws SailException {
		int count = 0;
		if (addedStatementsThisIteration != null) {
			count += applyOnAdded_1(addedStatementsThisIteration);
			count += applyOnAdded_2(addedStatementsThisIteration);
		}
		if (removedStatementsThisIteration != null) {
			count += applyOnRemoved_1(removedStatementsThisIteration);
			count += applyOnRemoved_2(removedStatementsThisIteration);
		}
		return count;
	}

	// adding: (p, rdfs:range, c)
	// if existing: (x, p, y)
	// then inferring: (y, rdf:type, c)
	private int applyOnAdded_1(Set<Statement> addedStatementsThisIteration) throws SailException {
		Set<Statement> inferredStatements = new HashSet<Statement>();

		for (Statement addedStatement : addedStatementsThisIteration) {
			if (! isRangeStatement(addedStatement) || ! isURI(addedStatement.getSubject())
					|| ! isResource(addedStatement.getObject())) {
				continue;
			}

			URI addedStatementProperty = (URI) addedStatement.getSubject();
			Value addedStatementClass = addedStatement.getObject();

			CloseableIteration<? extends Statement, SailException> existingStatements = connection.getStatements(null, addedStatementProperty, null, true);

			while (existingStatements.hasNext()) {
				Statement existingStatement = existingStatements.next();

				if (isResource(existingStatement.getObject())) {
					Statement inferredStatement = valueFactory.createStatement((Resource) existingStatement.getObject(), RDF.TYPE, addedStatementClass, existingStatement.getContext());
					inferredStatements.add(inferredStatement);
				}
			}
		}

		return addInferred(inferredStatements, RANGE_RULE);
	}

	// adding: (x, p, y)
	// if existing: (p, rdfs:range, c)
	// then inferring: (y, rdf:type, c)
	private int applyOnAdded_2(Set<Statement> addedStatementsThisIteration) throws SailException {
		Set<Statement> inferredStatements = new HashSet<Statement>();

		Set<Statement> allRangeStatements = null;

		for (Statement addedStatement : addedStatementsThisIteration) {
			if (! isResource(addedStatement.getObject())) {
				continue;
			}

			if (allRangeStatements == null) {
				allRangeStatements = getAllRangeStatements();
				if (allRangeStatements.isEmpty()) {
					return 0;
				}
			}

			URI addedStatementProperty = addedStatement.getPredicate();

			for (Statement rangeStatement : allRangeStatements) {
				if (rangeStatement.getSubject().equals(addedStatementProperty)) {
					Statement inferredStatement = valueFactory.createStatement((Resource) addedStatement.getObject(), RDF.TYPE, rangeStatement.getObject(), rangeStatement.getContext());
					inferredStatements.add(inferredStatement);
				}
			}
		}

		return addInferred(inferredStatements, RANGE_RULE);
	}

	// removing: (p, rdfs:range, c)
	// if existing: (x, p, y)
	// (case 1) and no existing: {(x', p', y), (p', rdfs:range, c)}
	// (case 2) and no existing: {(p, rdfs:range, c'), (c', rdfs:subClassOf, c)}
	// (case 3) and no existing: {(x', p', y), (p', rdfs:range, c'), (c', rdfs:subClassOf, c)}
	// (case 4) and no existing: {(y, rdf:type, c'), (c', rdfs:subClassOf, c)}
	// then removing inferred: (y, rdf:type, c)
	private int applyOnRemoved_1(Set<Statement> removedStatementsThisIteration) throws SailException {
		Set<Statement> inferredStatements = new HashSet<Statement>();

		for (Statement removedStatement : removedStatementsThisIteration) {
			if (! isRangeStatement(removedStatement) || ! isURI(removedStatement.getSubject())) {
				continue;
			}

			URI removedStatementProperty = (URI) removedStatement.getSubject();
			Value removedStatementClass = removedStatement.getObject();

			CloseableIteration<? extends Statement, SailException> existingStatements = connection.getStatements(null, removedStatementProperty, null, true);

			while (existingStatements.hasNext()) {
				Statement existingStatement = existingStatements.next();

				if (! isResource(existingStatement.getObject())) {
					continue;
				}

				boolean needsKeepingFromFirstCase = checkRangeStatementRemovalFirstCase(existingStatement, removedStatement);
				boolean needsKeepingFromSecondCase = checkRangeStatementRemovalSecondCase(existingStatement, removedStatement);
				boolean needsKeepingFromThirdCase = checkRangeStatementRemovalThirdCase(existingStatement, removedStatement);
				boolean needsKeepingFromFourthCase = checkSubclassOfStatementRemovalFourthCase(existingStatement, removedStatement);

				if (needsKeepingFromFirstCase || needsKeepingFromSecondCase || needsKeepingFromThirdCase || needsKeepingFromFourthCase) {
				} else {
					Statement inferredStatement = valueFactory.createStatement((Resource) existingStatement.getObject(), RDF.TYPE, removedStatementClass, removedStatement.getContext());
					inferredStatements.add(inferredStatement);
				}
			}
		}

		return removeInferred(inferredStatements, RANGE_RULE);
	}

	// removing: (p, rdfs:range, c)
	// existing: (x, p, y)
	// check if existing: {(x', p', y), (p', rdfs:range, c)}
	private boolean checkRangeStatementRemovalFirstCase(Statement existingStatement, Statement removedStatement) throws SailException {
		CloseableIteration<? extends Statement, SailException> alternativeRangeStatements = connection.getStatements(null, RDFS.RANGE, removedStatement.getObject(), true, removedStatement.getContext());
		while (alternativeRangeStatements.hasNext()) {
			Statement alternativeRangeStatement = alternativeRangeStatements.next();
			if (! isURI(alternativeRangeStatement.getSubject())) {
				continue;
			}

			CloseableIteration<? extends Statement, SailException> results = connection.getStatements(null, (URI) alternativeRangeStatement.getSubject(), existingStatement.getObject(), true);
			if (results.hasNext()) {
				return true;
			}					
		}
		return false;
	}

	// removing: (p, rdfs:range, c)
	// existing: (x, p, y)
	// check if existing: {(p, rdfs:range, c'), (c', rdfs:subClassOf, c)}
	private boolean checkRangeStatementRemovalSecondCase(Statement existingStatement, Statement removedStatement) throws SailException {
		CloseableIteration<? extends Statement, SailException> otherClassStatements = connection.getStatements(existingStatement.getPredicate(), RDFS.RANGE, null, true, removedStatement.getContext());
		while (otherClassStatements.hasNext()) {
			Statement otherClassStatement = otherClassStatements.next();

			if (! isResource(otherClassStatement.getObject())) {
				continue;
			}

			if (hasStatement((Resource) otherClassStatement.getObject(), RDFS.SUBCLASSOF, removedStatement.getObject(), true)) {
				return true;
			}
		}		
		return false;
	}

	// removing: (p, rdfs:range, c)
	// existing: (x, p, y)
	// check if existing: {(x', p', y), (p', rdfs:range, c'), (c', rdfs:subClassOf, c)}
	private boolean checkRangeStatementRemovalThirdCase(Statement existingStatement, Statement removedStatement) throws SailException {
		CloseableIteration<? extends Statement, SailException> subclassStatements = connection.getStatements(null, RDFS.SUBCLASSOF, removedStatement.getObject(), true, removedStatement.getContext());
		while (subclassStatements.hasNext()) {
			Statement subclassStatement = subclassStatements.next();

			CloseableIteration<? extends Statement, SailException> rangeStatements = connection.getStatements(null, RDFS.RANGE, subclassStatement.getSubject(), true);
			while (rangeStatements.hasNext()) {
				Statement rangeStatement = rangeStatements.next();
				if (! isURI(rangeStatement.getSubject())) {
					continue;
				}

				if (hasStatement(null, (URI) rangeStatement.getSubject(), existingStatement.getObject(), true)) {
					return true;
				}
			}
		}		
		return false;
	}

	// removing: (x, p, y)
	// if existing: (p, rdfs:range, c)
	// (case 1) and no existing: {(x', p, y)}
	// (case 2) and no existing: {(x', p', y), (p', rdfs:range, c)}
	// (case 3) and no existing: {(x', p', y), (p', rdfs:range, c'), (c', rdfs:subClassOf, c)}
	// (case 4) and no existing: {(y, rdf:type, c'), (c', rdfs:subClassOf, c)}
	// then removing inferred: (y, rdf:type, c)
	private int applyOnRemoved_2(Set<Statement> removedStatementsThisIteration) throws SailException {
		Set<Statement> inferredStatements = new HashSet<Statement>();

		for (Statement removedStatement : removedStatementsThisIteration) {
			if (! isResource(removedStatement.getObject())) {
				continue;
			}

			CloseableIteration<? extends Statement, SailException> rangeStatements = connection.getStatements(removedStatement.getPredicate(), RDFS.RANGE, null, true, removedStatement.getContext());
			while (rangeStatements.hasNext()) {
				Statement rangeStatement = rangeStatements.next();
				
				boolean needsKeepingFromFirstCase = checkGenericStatementRemovalFirstCase(removedStatement, rangeStatement);
				boolean needsKeepingFromSecondCase = checkGenericStatementRemovalSecondCase(removedStatement, rangeStatement);
				boolean needsKeepingFromThirdCase = checkGenericStatementRemovalThirdCase(removedStatement, rangeStatement);
				boolean needsKeepingFromFourthCase = checkSubclassOfStatementRemovalFourthCase(removedStatement, rangeStatement);

				if (needsKeepingFromFirstCase || needsKeepingFromSecondCase || needsKeepingFromThirdCase || needsKeepingFromFourthCase) {
				} else {
					Statement candidateToDelete = valueFactory.createStatement((Resource) removedStatement.getObject(), RDF.TYPE, rangeStatement.getObject(), removedStatement.getContext());
					inferredStatements.add(candidateToDelete);
				}
			}
		}

		return removeInferred(inferredStatements, RANGE_RULE);
	}

	// removing: (x, p, y)
	// existing: (p, rdfs:range, c)
	// check if existing: {(x', p, y)}
	private boolean checkGenericStatementRemovalFirstCase(Statement removedStatement, Statement rangeStatement) throws SailException {
		Statement candidateToDelete = valueFactory.createStatement((Resource) removedStatement.getObject(), RDF.TYPE, rangeStatement.getObject(), removedStatement.getContext());
		Statement typeThing = valueFactory.createStatement((Resource) removedStatement.getObject(), RDF.TYPE, OWL2.THING, removedStatement.getContext());

		Resource rangeStatementSubject = rangeStatement.getSubject();
		if (isURI(rangeStatementSubject)) {
			CloseableIteration<? extends Statement, SailException> statementsWithSamePropertyAndObject = connection.getStatements(null, (URI) rangeStatementSubject, removedStatement.getObject(), true, removedStatement.getContext());

			while (statementsWithSamePropertyAndObject.hasNext()) {
				Statement statementWithSameSubjectAndProperty = statementsWithSamePropertyAndObject.next();
				if (! statementWithSameSubjectAndProperty.equals(candidateToDelete) &&
						! statementWithSameSubjectAndProperty.equals(typeThing)) {
					return true;
				}
			}
		}
		return false;
	}

	// removing: (x, p, y)
	// existing: (p, rdfs:range, c)
	// check if existing: {(x', p', y), (p', rdfs:range, c)}
	private boolean checkGenericStatementRemovalSecondCase(Statement removedStatement, Statement rangeStatement) throws SailException {
		CloseableIteration<? extends Statement, SailException> statementsWithDifferentPropertyAndSameObject = connection.getStatements(null, null, removedStatement.getObject(), true, removedStatement.getContext());				
		while (statementsWithDifferentPropertyAndSameObject.hasNext()) {
			Statement statementWithDifferentPropertyAndSameObject = statementsWithDifferentPropertyAndSameObject.next();
			if (statementWithDifferentPropertyAndSameObject.getPredicate().equals(removedStatement.getPredicate())) {
				continue;
			}

			CloseableIteration<? extends Statement, SailException> results = connection.getStatements(statementWithDifferentPropertyAndSameObject.getPredicate(), RDFS.RANGE, rangeStatement.getObject(), true, removedStatement.getContext());
			if (results.hasNext()) {
				return true;
			}
		}
		return false;
	}

	// removing: (x, p, y)
	// if existing: (p, rdfs:range, c)
	// check if existing: {(x', p', y), (p', rdfs:range, c'), (c', rdfs:subClassOf, c)}
	private boolean checkGenericStatementRemovalThirdCase(Statement removedStatement, Statement rangeStatement) throws SailException {
		CloseableIteration<? extends Statement, SailException> subclassOfStatements = connection.getStatements(null, RDFS.SUBCLASSOF, rangeStatement.getObject(), true, removedStatement.getContext());
		while (subclassOfStatements.hasNext()) {
			Statement subclassOfStatement = subclassOfStatements.next();
			CloseableIteration<? extends Statement, SailException> otherRangeStatements = connection.getStatements(null, RDFS.RANGE, subclassOfStatement.getObject(), true);
			while (otherRangeStatements.hasNext()) {
				Statement otherRangeStatement = otherRangeStatements.next();
				if (! isURI(otherRangeStatement.getSubject())) {
					continue;
				}

				if (hasStatement(null, (URI) otherRangeStatement.getSubject(), removedStatement.getObject(), true)) {
					return true;
				}
			}
		}
		return false;
	}

	// removing: (x, p, y)
	// existing: (p, rdfs:range, c)
	// check if existing: {(y, rdf:type, c'), (c', rdfs:subClassOf, c)}
	private boolean checkSubclassOfStatementRemovalFourthCase(Statement removedStatement, Statement rangeStatement) throws SailException {
		CloseableIteration<? extends Statement, SailException> typeStatements = connection.getStatements((Resource) removedStatement.getObject(), RDF.TYPE, null, true, removedStatement.getContext());
		while (typeStatements.hasNext()) {
			Statement typeStatement = typeStatements.next();
			if (! isResource(typeStatement.getObject())) {
				continue;
			}

			if (hasStatement((Resource) typeStatement.getObject(), RDFS.SUBCLASSOF, rangeStatement.getObject(), true)) {
				return true;
			}
		}		
		return false;
	}

}
