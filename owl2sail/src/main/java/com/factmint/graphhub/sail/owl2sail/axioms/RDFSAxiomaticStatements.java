package com.factmint.graphhub.sail.owl2sail.axioms;

import java.util.HashSet;

import org.openrdf.model.Statement;
import org.openrdf.model.impl.StatementImpl;
import org.openrdf.model.vocabulary.RDF;
import org.openrdf.model.vocabulary.RDFS;

public class RDFSAxiomaticStatements extends HashSet<Statement> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3560156745071623790L;

	public RDFSAxiomaticStatements() {
		// From org.openrdf.sail.inferencer.fc.ForwardChainingRDFSInferencerConnection
		
		// RDF axiomatic triples (from RDF Semantics, section 3.1):

		this.add(new StatementImpl(RDF.TYPE, RDF.TYPE, RDF.PROPERTY));
		this.add(new StatementImpl(RDF.SUBJECT, RDF.TYPE, RDF.PROPERTY));
		this.add(new StatementImpl(RDF.PREDICATE, RDF.TYPE, RDF.PROPERTY));
		this.add(new StatementImpl(RDF.OBJECT, RDF.TYPE, RDF.PROPERTY));

		this.add(new StatementImpl(RDF.FIRST, RDF.TYPE, RDF.PROPERTY));
		this.add(new StatementImpl(RDF.REST, RDF.TYPE, RDF.PROPERTY));
		this.add(new StatementImpl(RDF.VALUE, RDF.TYPE, RDF.PROPERTY));

		this.add(new StatementImpl(RDF.NIL, RDF.TYPE, RDF.LIST));

		// RDFS axiomatic triples (from RDF Semantics, section 4.1):

		this.add(new StatementImpl(RDF.TYPE, RDFS.DOMAIN, RDFS.RESOURCE));
		this.add(new StatementImpl(RDFS.DOMAIN, RDFS.DOMAIN, RDF.PROPERTY));
		this.add(new StatementImpl(RDFS.RANGE, RDFS.DOMAIN, RDF.PROPERTY));
		this.add(new StatementImpl(RDFS.SUBPROPERTYOF, RDFS.DOMAIN, RDF.PROPERTY));
		this.add(new StatementImpl(RDFS.SUBCLASSOF, RDFS.DOMAIN, RDFS.CLASS));
		this.add(new StatementImpl(RDF.SUBJECT, RDFS.DOMAIN, RDF.STATEMENT));
		this.add(new StatementImpl(RDF.PREDICATE, RDFS.DOMAIN, RDF.STATEMENT));
		this.add(new StatementImpl(RDF.OBJECT, RDFS.DOMAIN, RDF.STATEMENT));
		this.add(new StatementImpl(RDFS.MEMBER, RDFS.DOMAIN, RDFS.RESOURCE));
		this.add(new StatementImpl(RDF.FIRST, RDFS.DOMAIN, RDF.LIST));
		this.add(new StatementImpl(RDF.REST, RDFS.DOMAIN, RDF.LIST));
		this.add(new StatementImpl(RDFS.SEEALSO, RDFS.DOMAIN, RDFS.RESOURCE));
		this.add(new StatementImpl(RDFS.ISDEFINEDBY, RDFS.DOMAIN, RDFS.RESOURCE));
		this.add(new StatementImpl(RDFS.COMMENT, RDFS.DOMAIN, RDFS.RESOURCE));
		this.add(new StatementImpl(RDFS.LABEL, RDFS.DOMAIN, RDFS.RESOURCE));
		this.add(new StatementImpl(RDF.VALUE, RDFS.DOMAIN, RDFS.RESOURCE));

		this.add(new StatementImpl(RDF.TYPE, RDFS.RANGE, RDFS.CLASS));
		this.add(new StatementImpl(RDFS.DOMAIN, RDFS.RANGE, RDFS.CLASS));
		this.add(new StatementImpl(RDFS.RANGE, RDFS.RANGE, RDFS.CLASS));
		this.add(new StatementImpl(RDFS.SUBPROPERTYOF, RDFS.RANGE, RDF.PROPERTY));
		this.add(new StatementImpl(RDFS.SUBCLASSOF, RDFS.RANGE, RDFS.CLASS));
		this.add(new StatementImpl(RDF.SUBJECT, RDFS.RANGE, RDFS.RESOURCE));
		this.add(new StatementImpl(RDF.PREDICATE, RDFS.RANGE, RDFS.RESOURCE));
		this.add(new StatementImpl(RDF.OBJECT, RDFS.RANGE, RDFS.RESOURCE));
		this.add(new StatementImpl(RDFS.MEMBER, RDFS.RANGE, RDFS.RESOURCE));
		this.add(new StatementImpl(RDF.FIRST, RDFS.RANGE, RDFS.RESOURCE));
		this.add(new StatementImpl(RDF.REST, RDFS.RANGE, RDF.LIST));
		this.add(new StatementImpl(RDFS.SEEALSO, RDFS.RANGE, RDFS.RESOURCE));
		this.add(new StatementImpl(RDFS.ISDEFINEDBY, RDFS.RANGE, RDFS.RESOURCE));
		this.add(new StatementImpl(RDFS.COMMENT, RDFS.RANGE, RDFS.LITERAL));
		this.add(new StatementImpl(RDFS.LABEL, RDFS.RANGE, RDFS.LITERAL));
		this.add(new StatementImpl(RDF.VALUE, RDFS.RANGE, RDFS.RESOURCE));

		this.add(new StatementImpl(RDF.ALT, RDFS.SUBCLASSOF, RDFS.CONTAINER));
		this.add(new StatementImpl(RDF.BAG, RDFS.SUBCLASSOF, RDFS.CONTAINER));
		this.add(new StatementImpl(RDF.SEQ, RDFS.SUBCLASSOF, RDFS.CONTAINER));
		this.add(new StatementImpl(RDFS.CONTAINERMEMBERSHIPPROPERTY, RDFS.SUBCLASSOF, RDF.PROPERTY));

		this.add(new StatementImpl(RDFS.ISDEFINEDBY, RDFS.SUBPROPERTYOF, RDFS.SEEALSO));

		this.add(new StatementImpl(RDF.XMLLITERAL, RDF.TYPE, RDFS.DATATYPE));
		this.add(new StatementImpl(RDF.XMLLITERAL, RDFS.SUBCLASSOF, RDFS.LITERAL));
		this.add(new StatementImpl(RDFS.DATATYPE, RDFS.SUBCLASSOF, RDFS.CLASS));
	}
	
}
