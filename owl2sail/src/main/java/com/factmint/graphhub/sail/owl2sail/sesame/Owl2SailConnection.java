package com.factmint.graphhub.sail.owl2sail.sesame;

import info.aduna.iteration.CloseableIteration;
import info.aduna.iteration.Iterations;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.openrdf.model.Resource;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.vocabulary.RDFS;
import org.openrdf.sail.SailConnectionListener;
import org.openrdf.sail.SailException;
import org.openrdf.sail.inferencer.InferencerConnection;
import org.openrdf.sail.inferencer.InferencerConnectionWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.factmint.graphhub.sail.owl2sail.axioms.OWL2AxiomaticStatements;
import com.factmint.graphhub.sail.owl2sail.axioms.RDFSAxiomaticStatements;
import com.factmint.graphhub.sail.owl2sail.configuration.Owl2RulesConfiguration;
import com.factmint.graphhub.sail.owl2sail.rules.Owl2Rule;
import com.factmint.graphhub.sail.owl2sail.sesame.utility.RulesBuilder;

public class Owl2SailConnection extends InferencerConnectionWrapper implements SailConnectionListener {

	private Set<Statement> addedStatements;
	private Set<Statement> removedStatements;
	private Set<Statement> addedStatementsThisIteration;
	private Set<Statement> removedStatementsThisIteration;

	private Boolean inForegroundInferencing = true;
	private Collection<Owl2Rule> owl2Rules;
	
	protected Collection<String> activeRules;
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
		
	public Owl2SailConnection(InferencerConnection sailBase, Boolean inForegroundInferecing) {
		super(sailBase);
		sailBase.addConnectionListener(this);
		
		this.inForegroundInferencing = inForegroundInferecing;
		
		activeRules = RulesBuilder.getActiveRules(new Owl2RulesConfiguration());	
		owl2Rules = RulesBuilder.instantiateAllRules(getWrappedConnection(), activeRules);
	}
	
	public Owl2SailConnection(InferencerConnection sailBase, Boolean liveInferecing, Owl2RulesConfiguration owl2RulesConfiguration) {
		super(sailBase);
		sailBase.addConnectionListener(this);
		
		this.inForegroundInferencing = liveInferecing;
		
		activeRules = RulesBuilder.getActiveRules(owl2RulesConfiguration);
		owl2Rules = RulesBuilder.instantiateAllRules(getWrappedConnection(), activeRules);
	}

	public void addAxiomStatements() throws SailException {
		RDFSAxiomaticStatements axiomaticStatements = new RDFSAxiomaticStatements();
		
		for (Statement axiom : axiomaticStatements) {
			addInferredStatement(axiom.getSubject(), axiom.getPredicate(), axiom.getObject());
		}
		
		OWL2AxiomaticStatements owl2AxiomaticStatements = new OWL2AxiomaticStatements();
		
		for (Statement axiom : owl2AxiomaticStatements) {
			addInferredStatement(axiom.getSubject(), axiom.getPredicate(), axiom.getObject());
		}
	}

	public void statementAdded(Statement statement) {
		if (addedStatements == null) {
			addedStatements = new HashSet<Statement>();
		}
		
		addedStatements.add(statement);
	}

	public void statementRemoved(Statement statement) {
		if (removedStatements == null) {
			removedStatements = new HashSet<Statement>();
		}
		
		removedStatements.add(statement);
	}

	@Override
	public void flushUpdates() throws SailException {
		super.flushUpdates();

		if (inForegroundInferencing) {
			doInferencing();
		}
	}
	
	@Override
	public void close() throws SailException {
		if (! inForegroundInferencing) {
			InferAndCloseConnectionJobRunner inferenceRunner = new InferAndCloseConnectionJobRunner(this.getWrappedConnection());
			Thread inferenceThread = new Thread(inferenceRunner);
			inferenceThread.start();
		} else {
			super.close();
		}
	}
	
	@Override
	public void rollback() throws SailException {
		super.rollback();

		addedStatements = null;
		removedStatements = null;
	}
	
	private class InferAndCloseConnectionJobRunner implements Runnable{
		private InferencerConnection connection;
		private Logger logger = LoggerFactory.getLogger(this.getClass());

		public InferAndCloseConnectionJobRunner(InferencerConnection inferencerConnection) {
			this.connection = inferencerConnection;
		}
		
		public void run(){
			try {
				if (! connection.isOpen()) {
					logger.error("No open connection available to perform inferencing.");
					return;
				}				
				
				connection.begin();
				doInferencing();
				connection.commit();
				connection.close();
			} catch (SailException e) {
				logger.error("Background inference failed with exception {}", e.getLocalizedMessage());
			}
		}
	}
		
	private void doInferencing() throws SailException {
		if (! hasAddedStatements() && ! hasRemovedStatements()) {
			logger.info("No inferencing to perform.");
			return;
		}
				
		Set<String> rulesToRun = new HashSet<String>();
		rulesToRun.addAll(activeRules);
			
		long startTime = System.currentTimeMillis();
		if (inForegroundInferencing == true) {
			logger.info("Starting foreground inferencing.");
		} else {
			logger.info("Starting background inferencing.");
		}
		
		while (hasAddedStatements() || hasRemovedStatements()) {
			if (hasRemovedStatements() && needsReinferencing()) {
				queueAllStatementsForReinferencing();
				removedStatements = null;
				rulesToRun.addAll(activeRules);
			}

			if (hasAddedStatements()) {
				addedStatementsThisIteration = addedStatements;
				addedStatements = new HashSet<Statement>();
			}
	
			if (hasRemovedStatements()) {
				removedStatementsThisIteration = removedStatements;
				removedStatements = new HashSet<Statement>();
			}
			
			Set<String> rulesToRunNextIteration = new HashSet<String>();
			for (Owl2Rule owl2Rule : owl2Rules) {
				rulesToRunNextIteration.addAll(owl2Rule.apply(addedStatementsThisIteration, removedStatementsThisIteration, rulesToRun));
			}
			
			rulesToRun.clear();
			rulesToRun.addAll(rulesToRunNextIteration);
			
			addedStatementsThisIteration = null;
			removedStatementsThisIteration = null;
		}
		
		long endTime = System.currentTimeMillis();
		if (inForegroundInferencing == true) {
			logger.info("Finished foreground inferencing after {} ms.", endTime - startTime);
		} else {
			logger.info("Finished background inferencing after {} ms.", endTime - startTime);
		}
	}

	private boolean hasAddedStatements() {
		return addedStatements != null && ! addedStatements.isEmpty();
	}

	private boolean hasRemovedStatements() {
		return removedStatements != null && ! removedStatements.isEmpty();
	}
	
	// if removing: (p, rdfs:domain, c) and existing: (x, p, y)
	// or
	// if removing: (p, rdfs:range, c) and existing: (x, p, y)
	// or
	// if removing: (x, p, y) and existing: (p, rdfs:domain, c)
	// or
	// removing: (x, p, y) and existing: (p, rdfs:range, c)
	private boolean needsReinferencing() throws SailException {
		/*for (Statement removedStatement : removedStatements) {
			if (removedStatement.getPredicate().equals(RDFS.DOMAIN) || removedStatement.getPredicate().equals(RDFS.RANGE)) {
				Resource property = removedStatement.getSubject();
				if (property instanceof URI) {
					CloseableIteration<? extends Statement, SailException> statements = getWrappedConnection().getStatements(null, (URI) property, null, true);
					if (statements.hasNext()) {
						return true;
					}
				}
			} else {
				URI property = removedStatement.getPredicate();					
				if (hasDomainStatement(property) || hasRangeStatement(property)) {
					return true;
				}
			}
		}*/
		
		return false;
	}
	
	private boolean hasDomainStatement(URI property) throws SailException {
		CloseableIteration<? extends Statement, SailException> statements = getWrappedConnection().getStatements(property, RDFS.RANGE, null, true);
		return statements.hasNext();
	}
	
	private boolean hasRangeStatement(URI property) throws SailException {
		CloseableIteration<? extends Statement, SailException> statements = getWrappedConnection().getStatements(property, RDFS.DOMAIN, null, true);
		return statements.hasNext();
	}

	private void queueAllStatementsForReinferencing() throws SailException {
		clearInferred();
		addAxiomStatements();
		Iterations.addAll(getWrappedConnection().getStatements(null, null, null, true), addedStatements);
	}
	
}
