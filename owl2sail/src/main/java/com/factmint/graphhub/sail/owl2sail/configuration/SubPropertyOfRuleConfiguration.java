package com.factmint.graphhub.sail.owl2sail.configuration;

public class SubPropertyOfRuleConfiguration {
	private boolean applyPropertyAxiomRule = true;
	private boolean applyEquivalentPropertyRule = true;
	private boolean applyDomainRule = true;
	private boolean applyRangeRule = true;
	
	public SubPropertyOfRuleConfiguration() {
	}
	
	public SubPropertyOfRuleConfiguration(boolean applyPropertyAxiomRule, boolean applyEquivalentClassRule, boolean applyDomainRule, boolean applyRangeRule) {
		this.applyPropertyAxiomRule = applyPropertyAxiomRule;
		this.setApplyEquivalentPropertyRule(applyEquivalentClassRule);
		this.applyDomainRule = applyDomainRule;
		this.applyRangeRule = applyRangeRule;
	}

	public boolean isApplyPropertyAxiomRule() {
		return applyPropertyAxiomRule;
	}

	public void setApplyPropertyAxiomRule(boolean applyPropertyAxiomRule) {
		this.applyPropertyAxiomRule = applyPropertyAxiomRule;
	}

	public boolean isApplyDomainRule() {
		return applyDomainRule;
	}

	public void setApplyDomainRule(boolean applyDomainRule) {
		this.applyDomainRule = applyDomainRule;
	}

	public boolean isApplyRangeRule() {
		return applyRangeRule;
	}

	public void setApplyRangeRule(boolean applyRangeRule) {
		this.applyRangeRule = applyRangeRule;
	}

	public boolean isApplyEquivalentPropertyRule() {
		return applyEquivalentPropertyRule;
	}

	public void setApplyEquivalentPropertyRule(boolean applyEquivalentPropertyRule) {
		this.applyEquivalentPropertyRule = applyEquivalentPropertyRule;
	}

}
