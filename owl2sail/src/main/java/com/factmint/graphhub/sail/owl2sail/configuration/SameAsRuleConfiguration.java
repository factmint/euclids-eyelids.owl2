package com.factmint.graphhub.sail.owl2sail.configuration;

public class SameAsRuleConfiguration {

	private boolean applyReplacementRule = true;
	private boolean applyDifferenceRule = true;
	
	public SameAsRuleConfiguration() {
	}
	
	public SameAsRuleConfiguration(boolean applyReplacementRule, boolean applyDifferenceRule) {
		this.applyReplacementRule = applyReplacementRule;
		this.applyDifferenceRule = applyDifferenceRule;
	}

	public boolean isApplyReplacementRule() {
		return applyReplacementRule;
	}

	public boolean isApplyDifferenceRule() {
		return applyDifferenceRule;
	}
	
	public void setApplyReplacementRule(boolean applyReplacementRule) {
		this.applyReplacementRule = applyReplacementRule;
	}

	public void setApplyDifferenceRule(boolean applyDifferenceRule) {
		this.applyDifferenceRule = applyDifferenceRule;
	}
	
}
