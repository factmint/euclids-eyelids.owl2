package com.factmint.graphhub.sail.owl2sail.rules;

import static com.factmint.graphhub.sail.owl2sail.sesame.utility.Constants.INVERSE_OF_RULE;
import info.aduna.iteration.CloseableIteration;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.openrdf.model.Resource;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.sail.SailException;
import org.openrdf.sail.inferencer.InferencerConnection;

import com.factmint.graphhub.sail.owl2sail.axioms.OWL2;

public class InverseOfRule extends Owl2Rule {

	public InverseOfRule(InferencerConnection connection, Map<String, List<String>> activeRulesDependencies) {
		super(connection, activeRulesDependencies);
	}

	@Override
	public Collection<String> apply(Set<Statement> addedStatementsThisIteration, Set<Statement> removedStatementsThisIteration, Set<String> rulesToRunThisIteration) throws SailException {
		Collection<String> rulesThatMadeChanges = new ArrayList<String>();
		if (rulesToRunThisIteration.contains(INVERSE_OF_RULE)) {
			int count = inverseOfRule(addedStatementsThisIteration, removedStatementsThisIteration);
			logger.debug("{} statement changes after applying {}.", count, INVERSE_OF_RULE);
			if (count > 0) {
				rulesThatMadeChanges.add(INVERSE_OF_RULE);
			}
		}
		return buildRulesToRunNextIteration(rulesThatMadeChanges);
	}

	private int inverseOfRule(Set<Statement> addedStatementsThisIteration, Set<Statement> removedStatementsThisIteration) throws SailException {
		int count = 0;
		if(addedStatementsThisIteration != null) {
			count += inverseOfRuleOnAddedInverseSchema(addedStatementsThisIteration);
			//Can we remove statements from the addedStatementsThisIteration when we find they have been used by one rule? 
			//This would speed up the second call.
			count += inverseOfRuleOnAddedRelationship(addedStatementsThisIteration);
		}
		if(removedStatementsThisIteration != null) {
			count += inverseOfRuleOnRemovedInverseSchema(removedStatementsThisIteration);
			count += inverseOfRuleOnRemovedRelationship(removedStatementsThisIteration);
		}
		return count;
	}


	//removed (?x, p1, ?y)
	//existing (?p1 owl:inverseOf ?p2)
	//inferred (?y, p2, ?x)
	//remove (?y, p2, ?x)
	private int inverseOfRuleOnRemovedRelationship(Set<Statement> removedStatementsThisIteration) throws SailException {
		Set<Statement> inferredStatementsToRemove = new HashSet<Statement>();
		for (Statement removedStatement : removedStatementsThisIteration) {
			//Query to find the statements that are the inverse of the removed statement 
			if(removedStatement.getObject() instanceof Resource) {
				CloseableIteration<? extends Statement, SailException> inverseStatements = connection.getStatements((Resource) removedStatement.getPredicate(), OWL2.INVERSEOF, null, false);
				while(inverseStatements.hasNext()) {
					Statement schemaStatement = inverseStatements.next();
					if(removedStatement.getObject() instanceof Resource && schemaStatement.getObject() instanceof URI) {
						Statement inferredStatementToRemove = valueFactory.createStatement((Resource) removedStatement.getObject(), (URI) schemaStatement.getObject(), removedStatement.getSubject());
						inferredStatementsToRemove.add(inferredStatementToRemove);
					}
				}
			}
		}
		return removeInferred(inferredStatementsToRemove, INVERSE_OF_RULE);
	}

	//removed (?p1 owl:inverseOf ?p2)
	//existing (?x, p1, ?y)
	//inferred (?y, p2, ?x)
	//remove (?y, p2, ?x)
	private int inverseOfRuleOnRemovedInverseSchema(Set<Statement> removedStatementsThisIteration) throws SailException {
		Set<Statement> inferredStatementsToRemove = new HashSet<Statement>();
		for (Statement removedStatement : removedStatementsThisIteration) {
			if(!isInversePropertyStatement(removedStatement)) {
				continue;
			} else if(removedStatement.getSubject() instanceof URI){
				//Query to find all existing statements with subject of removed statement as predicate
				//Then remove the inverse statements of those
				CloseableIteration<? extends Statement, SailException> existingStatements = connection.getStatements(null, (URI) removedStatement.getSubject(), null, true);
				while(existingStatements.hasNext()) {
					Statement existingStatement = existingStatements.next();
					if(existingStatement.getObject() instanceof Resource && removedStatement.getObject() instanceof URI) {
						Statement inferredStatementToRemove = valueFactory.createStatement((Resource) existingStatement.getObject(), (URI) removedStatement.getObject(), existingStatement.getSubject());
						inferredStatementsToRemove.add(inferredStatementToRemove);
					}
				}
			}
		}
		return removeInferred(inferredStatementsToRemove, INVERSE_OF_RULE);
	}


	//added (?p1 owl:inverseOf ?p2)
	//existing (?x, p1, ?y)
	//then (?y, p2, ?x)
	private int inverseOfRuleOnAddedInverseSchema(Set<Statement> addedStatementsThisIteration) throws SailException {
		Set<Statement> inferredStatements = new HashSet<Statement>();
		for (Statement addedStatement : addedStatementsThisIteration) {
			if(!isInversePropertyStatement(addedStatement)) {
				continue;
			} else {
				//Query to get all existing statements with inverse properties
				CloseableIteration<? extends Statement, SailException> allStatements = connection.getStatements(null, (URI) addedStatement.getSubject(), null, true);
				while(allStatements.hasNext()) {
					Statement nextStatement = allStatements.next();
					if(nextStatement.getObject() instanceof Resource && addedStatement.getObject() instanceof URI) {
						Statement statementToAdd = valueFactory.createStatement((Resource) nextStatement.getObject(), (URI) addedStatement.getObject(), nextStatement.getSubject());
						inferredStatements.add(statementToAdd);
					}
				}
				//addedStatementsThisIteration.remove(addedStatement);
			}

		}
		return addInferred(inferredStatements, INVERSE_OF_RULE);
	}

	//added (?x p1 ?y)
	//existing (?p1 owl:inverseOf ?p2)
	//then (?y, p2, ?x)
	private int inverseOfRuleOnAddedRelationship(Set<Statement> addedStatementsThisIteration) throws SailException {
		Set<Statement> inferredStatements = new HashSet<Statement>();

		for (Statement addedStatement : addedStatementsThisIteration) {
			//Query to get statements whos subject is the predicate of the added statement
			CloseableIteration<? extends Statement, SailException> allStatements = connection.getStatements(addedStatement.getPredicate(), null, null, true);
			while (allStatements.hasNext()) { 
				Statement nextStatement = allStatements.next();
				if(!isInversePropertyStatement(nextStatement)) {
					continue;
				} else if(addedStatement.getObject() instanceof Resource && nextStatement.getObject() instanceof URI) {
					Statement statementToAdd = valueFactory.createStatement((Resource) addedStatement.getObject(), (URI) nextStatement.getObject(), addedStatement.getSubject());
					inferredStatements.add(statementToAdd);
					//addedStatementsThisIteration.remove(addedStatement);
				}
			}
		}
		return addInferred(inferredStatements, INVERSE_OF_RULE);
	}	

}
