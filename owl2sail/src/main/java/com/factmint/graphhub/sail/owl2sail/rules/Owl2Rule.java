package com.factmint.graphhub.sail.owl2sail.rules;

import static com.factmint.graphhub.sail.owl2sail.sesame.utility.Constants.RDF_LIST_PREDICATE_PREFIX;
import info.aduna.iteration.CloseableIteration;
import info.aduna.iteration.Iterations;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.openrdf.model.Resource;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.Value;
import org.openrdf.model.ValueFactory;
import org.openrdf.model.impl.ValueFactoryImpl;
import org.openrdf.model.vocabulary.RDF;
import org.openrdf.model.vocabulary.RDFS;
import org.openrdf.sail.SailException;
import org.openrdf.sail.inferencer.InferencerConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.factmint.graphhub.sail.owl2sail.axioms.OWL2;

public abstract class Owl2Rule {
	
	protected Map<String, List<String>> activeRulesDependencies;
	protected InferencerConnection connection;
	protected ValueFactory valueFactory = ValueFactoryImpl.getInstance();

	protected Logger logger = LoggerFactory.getLogger(this.getClass());

	public Owl2Rule(InferencerConnection connection, Map<String, List<String>> activeRulesDependencies) {
		this.connection = connection;
		this.activeRulesDependencies = activeRulesDependencies;
	}
	
	public abstract Collection<String> apply(Set<Statement> addedStatementsThisIteration, Set<Statement> removedStatementsThisIteration, Set<String> rulesToRunThisIteration) throws SailException;
	
	protected Collection<String> buildRulesToRunNextIteration(Collection<String> rulesThatMadeChanges) {
		Collection<String> rulesToRunNextIteration = new ArrayList<String>();
		for (String ruleThatMadeChanges : rulesThatMadeChanges) {
			if (! activeRulesDependencies.containsKey(ruleThatMadeChanges)) {
				continue;
			}
			
			List<String> ruleDependencies = activeRulesDependencies.get(ruleThatMadeChanges);
			if (ruleDependencies.isEmpty()) {
				continue;
			}
			
			for (String ruleDependency : ruleDependencies) {
				rulesToRunNextIteration.add(ruleDependency);
			}
		}
		return rulesToRunNextIteration;
	}
	
	protected int addInferred(Set<Statement> inferredStatements, String ruleName) throws SailException {
		int count = 0;
		for (Statement inferredStatement : inferredStatements) {
			logger.debug("Applying {}, adding statement {}.", ruleName, inferredStatement.toString());			
			boolean added = connection.addInferredStatement(inferredStatement.getSubject(), inferredStatement.getPredicate(), inferredStatement.getObject(), inferredStatement.getContext());
			if (added) {
				count += 1;
			}
		}
		return count;
	}	
	
	protected int removeInferred(Set<Statement> inferredStatements, String ruleName) throws SailException {
		int count = 0;
		for (Statement inferredStatement : inferredStatements) {
			logger.debug("Applying {}, removing statement {}.", ruleName, inferredStatement.toString());
			
			boolean removed = connection.removeInferredStatement(inferredStatement.getSubject(), inferredStatement.getPredicate(), inferredStatement.getObject(), inferredStatement.getContext());
			if (removed) {
				count += 1;
			}
		}
		return count;
	}
	
	protected boolean hasStatement(Resource subject, URI predicate, Value object, boolean includeInferred, Resource... contexts)	throws SailException {
		return connection.getStatements(subject, predicate, object, includeInferred, contexts).hasNext();
	}	

	protected boolean isURI(Value statementValue) {
		return (statementValue instanceof URI);
	}	

	protected boolean isResource(Value statementValue) {
		return (statementValue instanceof Resource);
	}	

	protected boolean isReflexiveStatement(Statement statement) {
		return statement.getSubject().equals(statement.getObject());
	}
	
	protected boolean isValidSameAsStatement(Statement statement) {
		return statement.getPredicate().equals(OWL2.SAMEAS) 
				&& isResource(statement.getSubject()) 
				&& isResource(statement.getObject());
	}

	protected boolean isAllDifferentStatement(Statement statement) {
		return statement.getPredicate().equals(RDF.TYPE) && statement.getObject().equals(OWL2.ALLDIFFERENT);
	}

	protected boolean isMembersStatement(Statement statement) {
		return statement.getPredicate().equals(OWL2.MEMBERS) || statement.getPredicate().equals(OWL2.DISTINCTMEMBERS);
	}

	protected boolean isListMembershipStatement(Statement statement) {
		return statement.getPredicate().stringValue().startsWith(RDF_LIST_PREDICATE_PREFIX);
	}

	protected boolean hasTypeAllDifferent(Resource subject) throws SailException {
		return hasStatement(subject, RDF.TYPE, OWL2.ALLDIFFERENT, true);
	}
	
	protected boolean isSubclassOfStatement(Statement statement) {
		return statement.getPredicate().equals(RDFS.SUBCLASSOF);
	}
	
	protected boolean isTypeStatement(Statement statement) {
		return statement.getPredicate().equals(RDF.TYPE);
	}
	
	protected boolean isDomainStatement(Statement statement) {
		return statement.getPredicate().equals(RDFS.DOMAIN);
	}
	
	protected boolean isRangeStatement(Statement statement) {
		return statement.getPredicate().equals(RDFS.RANGE);
	}
	
	protected boolean isSymmetricPropertyStatement(Statement statement) {
		return statement.getPredicate().equals(RDF.TYPE) && statement.getObject().equals(OWL2.SYMMETRICPROPERTY);
	}
	
	protected boolean hasSymmetricPropertyType(URI predicate) throws SailException {
		return hasStatement(predicate, RDF.TYPE, OWL2.SYMMETRICPROPERTY, true);
	}
	
	protected boolean isInversePropertyStatement(Statement statement) {
		return statement.getPredicate().equals(OWL2.INVERSEOF);
	}
	
	protected boolean isTransitivePropertyStatement(Statement statement) {
		return statement.getPredicate().equals(RDF.TYPE) && statement.getObject().equals(OWL2.TRANSITIVEPROPERTY);
	}

	protected boolean hasTransitivePropertyType(URI predicate) throws SailException {
		return hasStatement(predicate, RDF.TYPE, OWL2.TRANSITIVEPROPERTY, true);
	}
	
	protected boolean isSubPropertyOfStatement(Statement statement) {
		return statement.getPredicate().equals(RDFS.SUBPROPERTYOF);
	}
	
	protected boolean isTypeThingStatement(Statement statement) {
		return statement.getPredicate().equals(RDF.TYPE) && statement.getObject().equals(OWL2.THING);
	}
	
	protected boolean isTypeResourceStatement(Statement statement) {
		return statement.getPredicate().equals(RDF.TYPE) && statement.getObject().equals(RDFS.RESOURCE);
	}
	
	protected boolean isTypeReflexivePropertyStatement(Statement statement) {
		return statement.getPredicate().equals(RDF.TYPE) && statement.getObject().equals(OWL2.REFLEXIVEPROPERTY);
	}
	
	protected boolean isComplementOfStatement(Statement statement) {
		return statement.getPredicate().equals(OWL2.COMPLEMENTOF);
	}
	
	protected Set<Statement> getAllNonReflexiveSameAsStatements() throws SailException {
		CloseableIteration<? extends Statement, SailException> allSameAsStatementsIteration = connection.getStatements(null,  OWL2.SAMEAS, null, true);
		Set<Statement> allSameAsStatements = new HashSet<Statement>();
		while (allSameAsStatementsIteration.hasNext()) {
			Statement sameAsStatement = allSameAsStatementsIteration.next();
			if (! isReflexiveStatement(sameAsStatement)) {
				allSameAsStatements.add(sameAsStatement);
			}
		}
		return allSameAsStatements;
	}
	
	protected Set<Statement> getAllSubclassOfStatements() throws SailException {
		Set<Statement> allSubclassOfStatements = new HashSet<Statement>();
		Iterations.addAll(connection.getStatements(null,  RDFS.SUBCLASSOF, null, true), allSubclassOfStatements);
		return allSubclassOfStatements;
	}
	
	protected Set<Resource> getSuperClasses(Resource subclass) throws SailException {
		Set<Resource> superClasses = new HashSet<Resource>();
		
		CloseableIteration<? extends Statement, SailException> superClassStatementIterator = connection.getStatements(subclass, RDFS.SUBCLASSOF, null, true);
		while (superClassStatementIterator.hasNext()) {
			superClasses.add((Resource) superClassStatementIterator.next().getObject());
		}
		
		return superClasses;
	}
	
	protected Set<Statement> getAllSubpropertyOfStatements() throws SailException {
		Set<Statement> allSubpropertyOfStatements = new HashSet<Statement>();
		Iterations.addAll(connection.getStatements(null,  RDFS.SUBPROPERTYOF, null, true), allSubpropertyOfStatements);
		return allSubpropertyOfStatements;
	}
	
	protected Set<Statement> getAllDomainStatements() throws SailException {
		Set<Statement> allDomainStatements = new HashSet<Statement>();
		Iterations.addAll(connection.getStatements(null,  RDFS.DOMAIN, null, true), allDomainStatements);
		return allDomainStatements;
	}
		
	protected Set<Statement> getAllRangeStatements() throws SailException {
		Set<Statement> allRangeStatements = new HashSet<Statement>();
		Iterations.addAll(connection.getStatements(null,  RDFS.RANGE, null, true), allRangeStatements);
		return allRangeStatements;
	}
	
	protected Set<Statement> getAllTypeStatements() throws SailException {
		Set<Statement> allTypeStatements = new HashSet<Statement>();
		Iterations.addAll(connection.getStatements(null,  RDF.TYPE, null, true), allTypeStatements);
		return allTypeStatements;
	}
	
	protected Set<Statement> getAllTypeSymmetricProperty() throws SailException {
		Set<Statement> allTypeSymmetricPropertyStatements = new HashSet<Statement>();
		Iterations.addAll(connection.getStatements(null,  RDF.TYPE, OWL2.SYMMETRICPROPERTY, true), allTypeSymmetricPropertyStatements);
		return allTypeSymmetricPropertyStatements;
	}
	
	protected Set<URI> getAllPredicatesFromSubject(Resource subject) throws SailException {
		Set<URI> predicates = new HashSet<URI>();
		
		CloseableIteration<? extends Statement, SailException> superClassStatementIterator = connection.getStatements(subject, null, null, true);
		while (superClassStatementIterator.hasNext()) {
			predicates.add(superClassStatementIterator.next().getPredicate());
		}
		
		return predicates;
	}
	
	protected Set<URI> getAllPredicatesFromObject(Resource object) throws SailException {
		Set<URI> predicates = new HashSet<URI>();
		
		CloseableIteration<? extends Statement, SailException> superClassStatementIterator = connection.getStatements(null, null, object, true);
		while (superClassStatementIterator.hasNext()) {
			predicates.add(superClassStatementIterator.next().getPredicate());
		}
		
		return predicates;
	}
	
}
