/* 
 * Licensed to Aduna under one or more contributor license agreements.  
 * See the NOTICE.txt file distributed with this work for additional 
 * information regarding copyright ownership. 
 *
 * Aduna licenses this file to you under the terms of the Aduna BSD 
 * License (the "License"); you may not use this file except in compliance 
 * with the License. See the LICENSE.txt file distributed with this work 
 * for the full License.
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS, 
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or 
 * implied. See the License for the specific language governing permissions
 * and limitations under the License.
 */

package com.factmint.graphhub.sail.owl2sail.axioms;

import org.openrdf.model.URI;
import org.openrdf.model.ValueFactory;
import org.openrdf.model.impl.ValueFactoryImpl;

public class OWL2 {

	private static ValueFactory valueFactory = ValueFactoryImpl.getInstance();
	
	public final static String OWL_PREFIX = "http://www.w3.org/2002/07/owl#";
	public final static String LDP_PREFIX = "http://www.w3.org/ns/ldp#";
	
	public final static URI OWL_NAMESPACE = valueFactory.createURI(OWL_PREFIX);
	public final static URI ALLDIFFERENT = valueFactory.createURI(OWL_PREFIX + "AllDifferent");
	public final static URI ALLDISJOINTCLASSES = valueFactory.createURI(OWL_PREFIX + "AllDisjointClasses");
	public final static URI ALLDISJOINTPROPERTIES = valueFactory.createURI(OWL_PREFIX + "AllDisjointProperties");
	public final static URI ANNOTATION = valueFactory.createURI(OWL_PREFIX + "Annotation");
	public final static URI ANNOTATIONPROPERTY = valueFactory.createURI(OWL_PREFIX + "AnnotationProperty");
	public final static URI ASYMMETRICPROPERTY = valueFactory.createURI(OWL_PREFIX + "AsymmetricProperty");
	public final static URI AXIOM = valueFactory.createURI(OWL_PREFIX + "Axiom");
	public final static URI CLASS = valueFactory.createURI(OWL_PREFIX + "Class");
	public final static URI DATARANGE = valueFactory.createURI(OWL_PREFIX + "DataRange");
	public final static URI DATATYPEPROPERTY = valueFactory.createURI(OWL_PREFIX + "DatatypeProperty");
	public final static URI DEPRECATEDCLASS = valueFactory.createURI(OWL_PREFIX + "DeprecatedClass");
	public final static URI DEPRECATEDPROPERTY = valueFactory.createURI(OWL_PREFIX + "DeprecatedProperty");
	public final static URI FUNCTIONALPROPERTY = valueFactory.createURI(OWL_PREFIX + "FunctionalProperty");
	public final static URI INVERSEFUNCTIONALPROPERTY = valueFactory.createURI(OWL_PREFIX + "InverseFunctionalProperty");
	public final static URI IRREFLEXIVEPROPERTY = valueFactory.createURI(OWL_PREFIX + "IrreflexiveProperty");
	public final static URI NAMEDINDIVIDUAL = valueFactory.createURI(OWL_PREFIX + "NamedIndividual");
	public final static URI NEGATIVEPROPERTYASSERTION = valueFactory.createURI(OWL_PREFIX + "NegativePropertyAssertion");
	public final static URI NOTHING = valueFactory.createURI(OWL_PREFIX + "Nothing");
	public final static URI OBJECTPROPERTY = valueFactory.createURI(OWL_PREFIX + "ObjectProperty");
	public final static URI ONTOLOGY = valueFactory.createURI(OWL_PREFIX + "Ontology");
	public final static URI ONTOLOGYPROPERTY = valueFactory.createURI(OWL_PREFIX + "OntologyProperty");
	public final static URI REFLEXIVEPROPERTY = valueFactory.createURI(OWL_PREFIX + "ReflexiveProperty");
	public final static URI RESTRICTION = valueFactory.createURI(OWL_PREFIX + "Restriction");
	public final static URI SYMMETRICPROPERTY = valueFactory.createURI(OWL_PREFIX + "SymmetricProperty");
	public final static URI THING = valueFactory.createURI(OWL_PREFIX + "Thing");
	public final static URI TRANSITIVEPROPERTY = valueFactory.createURI(OWL_PREFIX + "TransitiveProperty");
	public final static URI ALLVALUESFROM = valueFactory.createURI(OWL_PREFIX + "allValuesFrom");
	public final static URI ANNOTATEDPROPERTY = valueFactory.createURI(OWL_PREFIX + "annotatedProperty");
	public final static URI ANNOTATEDSOURCE = valueFactory.createURI(OWL_PREFIX + "annotatedSource");
	public final static URI ANNOTATEDTARGET = valueFactory.createURI(OWL_PREFIX + "annotatedTarget");
	public final static URI ASSERTIONPROPERTY = valueFactory.createURI(OWL_PREFIX + "assertionProperty");
	public final static URI BACKWARDCOMPATIBLEWITH = valueFactory.createURI(OWL_PREFIX + "backwardCompatibleWith");
	public final static URI BOTTOMDATAPROPERTY = valueFactory.createURI(OWL_PREFIX + "bottomDataProperty");
	public final static URI BOTTOMOBJECTPROPERTY = valueFactory.createURI(OWL_PREFIX + "bottomObjectProperty");
	public final static URI CARDINALITY = valueFactory.createURI(OWL_PREFIX + "cardinality");
	public final static URI COMPLEMENTOF = valueFactory.createURI(OWL_PREFIX + "complementOf");
	public final static URI DATATYPECOMPLEMENTOF = valueFactory.createURI(OWL_PREFIX + "datatypeComplementOf");
	public final static URI DEPRECATED = valueFactory.createURI(OWL_PREFIX + "deprecated");
	public final static URI DIFFERENTFROM = valueFactory.createURI(OWL_PREFIX + "differentFrom");
	public final static URI DISJOINTUNIONOF = valueFactory.createURI(OWL_PREFIX + "disjointUnionOf");
	public final static URI DISJOINTWITH = valueFactory.createURI(OWL_PREFIX + "disjointWith");
	public final static URI DISTINCTMEMBERS = valueFactory.createURI(OWL_PREFIX + "distinctMembers");
	public final static URI EQUIVALENTCLASS = valueFactory.createURI(OWL_PREFIX + "equivalentClass");
	public final static URI EQUIVALENTPROPERTY = valueFactory.createURI(OWL_PREFIX + "equivalentProperty");
	public final static URI HASKEY = valueFactory.createURI(OWL_PREFIX + "hasKey");
	public final static URI HASSELF = valueFactory.createURI(OWL_PREFIX + "hasSelf");
	public final static URI HASVALUE = valueFactory.createURI(OWL_PREFIX + "hasValue");
	public final static URI IMPORTS = valueFactory.createURI(OWL_PREFIX + "imports");
	public final static URI INCOMPATIBLEWITH = valueFactory.createURI(OWL_PREFIX + "incompatibleWith");
	public final static URI INTERSECTIONOF = valueFactory.createURI(OWL_PREFIX + "intersectionOf");
	public final static URI INVERSEOF = valueFactory.createURI(OWL_PREFIX + "inverseOf");
	public final static URI MAXCARDINALITY = valueFactory.createURI(OWL_PREFIX + "maxCardinality");
	public final static URI MAXQUALIFIEDCARDINALITY = valueFactory.createURI(OWL_PREFIX + "maxQualifiedCardinality");
	public final static URI MEMBERS = valueFactory.createURI(OWL_PREFIX + "members");
	public final static URI MINCARDINALITY = valueFactory.createURI(OWL_PREFIX + "minCardinality");
	public final static URI MINQUALIFIEDCARDINALITY = valueFactory.createURI(OWL_PREFIX + "minQualifiedCardinality");
	public final static URI ONCLASS = valueFactory.createURI(OWL_PREFIX + "onClass");
	public final static URI ONDATARANGE = valueFactory.createURI(OWL_PREFIX + "onDataRange");
	public final static URI ONDATATYPE = valueFactory.createURI(OWL_PREFIX + "onDatatype");
	public final static URI ONPROPERTIES = valueFactory.createURI(OWL_PREFIX + "onProperties");
	public final static URI ONPROPERTY = valueFactory.createURI(OWL_PREFIX + "onProperty");
	public final static URI ONEOF = valueFactory.createURI(OWL_PREFIX + "oneOf");
	public final static URI PRIORVERSION = valueFactory.createURI(OWL_PREFIX + "priorVersion");
	public final static URI PROPERTYCHAINAXIOM = valueFactory.createURI(OWL_PREFIX + "propertyChainAxiom");
	public final static URI PROPERTYDISJOINTWITH = valueFactory.createURI(OWL_PREFIX + "propertyDisjointWith");
	public final static URI QUALIFIEDCARDINALITY = valueFactory.createURI(OWL_PREFIX + "qualifiedCardinality");
	public final static URI SAMEAS = valueFactory.createURI(OWL_PREFIX + "sameAs");
	public final static URI SOMEVALUESFROM = valueFactory.createURI(OWL_PREFIX + "someValuesFrom");
	public final static URI SOURCEINDIVIDUAL = valueFactory.createURI(OWL_PREFIX + "sourceIndividual");
	public final static URI TARGETINDIVIDUAL = valueFactory.createURI(OWL_PREFIX + "targetIndividual");
	public final static URI TARGETVALUE = valueFactory.createURI(OWL_PREFIX + "targetValue");
	public final static URI TOPDATARESOURCE = valueFactory.createURI(OWL_PREFIX + "topDataProperty");
	public final static URI TOPOBJECTPROPERTY = valueFactory.createURI(OWL_PREFIX + "topObjectProperty");
	public final static URI UNIONOF = valueFactory.createURI(OWL_PREFIX + "unionOf");
	public final static URI VERSIONIRI = valueFactory.createURI(OWL_PREFIX + "versionIRI");
	public final static URI VERSIONINFO = valueFactory.createURI(OWL_PREFIX + "versionInfo");
	public final static URI WITHRESTRICTIONS = valueFactory.createURI(OWL_PREFIX + "withRestrictions");

	// LDP classes
	public final static URI LDPRESOURCE = valueFactory.createURI(LDP_PREFIX + "Resource");
	public final static URI LDPRDFSOURCE = valueFactory.createURI(LDP_PREFIX + "RDFSource");
	public final static URI LDPNONRDFSOURCE = valueFactory.createURI(LDP_PREFIX + "NonRDFSource");
	public final static URI LDPCONTAINER = valueFactory.createURI(LDP_PREFIX + "Container");
	public final static URI LDPBASICCONTAINER = valueFactory.createURI(LDP_PREFIX + "BasicContainer");
	public final static URI LDPDIRECTCONTAINER = valueFactory.createURI(LDP_PREFIX + "DirectContainer");
	public final static URI LDPINDIRECTCONTAINER = valueFactory.createURI(LDP_PREFIX + "IndirectContainer");
	
}

