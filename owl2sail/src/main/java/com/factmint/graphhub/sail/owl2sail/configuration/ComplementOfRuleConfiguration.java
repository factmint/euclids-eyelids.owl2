package com.factmint.graphhub.sail.owl2sail.configuration;

public class ComplementOfRuleConfiguration {

	private boolean applyComplementOfRule = true;
	
	public ComplementOfRuleConfiguration() {
	}
	
	public ComplementOfRuleConfiguration(boolean applyComplementOfRule) {
		this.applyComplementOfRule = applyComplementOfRule;
	}

	public boolean isApplyComplementOfRule() {
		return applyComplementOfRule;
	}

	public void setApplyComplementOfRule(boolean applyComplementOfRule) {
		this.applyComplementOfRule = applyComplementOfRule;
	}
	
}
