package com.factmint.graphhub.sail.owl2sail.rules;

import static com.factmint.graphhub.sail.owl2sail.sesame.utility.Constants.RULE_DEPENDENCIES;
import static com.factmint.graphhub.sail.owl2sail.sesame.utility.Constants.SUBPROPERTY_OF_DOMAIN_RULE;
import static com.factmint.graphhub.sail.owl2sail.sesame.utility.Constants.SUBPROPERTY_OF_EQUIVALENT_PROPERTY_RULE;
import static com.factmint.graphhub.sail.owl2sail.sesame.utility.Constants.SUBPROPERTY_OF_PROPERTY_AXIOM_RULE;
import static com.factmint.graphhub.sail.owl2sail.sesame.utility.Constants.SUBPROPERTY_OF_RANGE_RULE;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import info.aduna.iteration.CloseableIteratorIteration;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.openrdf.model.Resource;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.ValueFactory;
import org.openrdf.model.impl.ValueFactoryImpl;
import org.openrdf.model.vocabulary.RDFS;
import org.openrdf.sail.SailException;
import org.openrdf.sail.inferencer.InferencerConnection;

import com.factmint.graphhub.sail.owl2sail.axioms.OWL2;
import com.factmint.graphhub.sail.owl2sail.rules.SubPropertyOfRule;
import com.google.common.collect.Sets;

public class SubPropertyOfRuleTest {
	
	private static final String HAS_MOTHER = "http://test.owl2.com/hasMother";
	private static final String HAS_PARENT = "http://test.owl2.com/hasParent";
	private static final String HAS_RELATIVE = "http://test.owl2.com/hasRelative";
	private static final String HAS_FAMILY_MEMBER = "http://test.owl2.com/hasFamilyMember";
	private static final String IRENE_CURIE = "http://test.owl2.com/IreneCurie";
	private static final String MARIE_CURIE = "http://test.owl2.com/MarieCurie";
	private static final String CHILD = "http://test.owl2.com/Child";
	private static final String ADULT = "http://test.owl2.com/Adult";
	
	private InferencerConnection mockConnection = mock(InferencerConnection.class);
	private ValueFactory valueFactory = ValueFactoryImpl.getInstance(); 
	
	private SubPropertyOfRule ruleUnderTest;
	
	@Before
	public void setup() {
		ruleUnderTest = new SubPropertyOfRule(mockConnection, RULE_DEPENDENCIES);
	}
	
	@Test
	public void testDependenciesOfPropertyAxiom() throws SailException {
		URI hasMotherResource = valueFactory.createURI(HAS_MOTHER);
		URI hasParentResource = valueFactory.createURI(HAS_PARENT);
		Resource ireneCurieResource = valueFactory.createURI(IRENE_CURIE);
		Resource marieCurieResource = valueFactory.createURI(MARIE_CURIE);
		
		Set<Statement> addedStatements = Sets.newHashSet(valueFactory.createStatement(hasMotherResource, RDFS.SUBPROPERTYOF, hasParentResource));
		Set<String> rules = new HashSet<String>();
		rules.add(SUBPROPERTY_OF_PROPERTY_AXIOM_RULE);
		
		Set<Statement> statementList = new HashSet<Statement>();
		statementList.add(valueFactory.createStatement(ireneCurieResource, hasMotherResource, marieCurieResource));
		Iterator<Statement> statementListIterator = statementList.iterator();
		
		when(mockConnection.getStatements(null, hasMotherResource, null, true)).thenReturn(new CloseableIteratorIteration(statementListIterator));
		when(mockConnection.addInferredStatement(ireneCurieResource, hasParentResource, marieCurieResource, null)).thenReturn(true);
		
		Collection<String> rulesToApplyNextIteration = ruleUnderTest.apply(addedStatements, null, rules);
		assertEquals(RULE_DEPENDENCIES.get(SUBPROPERTY_OF_PROPERTY_AXIOM_RULE), rulesToApplyNextIteration);
	}
	
	@Test
	public void testDependenciesOfEquivalentProperty() throws SailException {
		Resource hasRelativeResource = valueFactory.createURI(HAS_RELATIVE);
		Resource hasFamilyMemberResource = valueFactory.createURI(HAS_FAMILY_MEMBER);
		
		Set<Statement> addedStatements = Sets.newHashSet(valueFactory.createStatement(hasRelativeResource, RDFS.SUBPROPERTYOF, hasFamilyMemberResource));
		Set<String> rules = new HashSet<String>();
		rules.add(SUBPROPERTY_OF_EQUIVALENT_PROPERTY_RULE);
		
		Set<Statement> statementList = new HashSet<Statement>();
		statementList.add(valueFactory.createStatement(hasFamilyMemberResource, RDFS.SUBPROPERTYOF, hasRelativeResource));
		Iterator<Statement> statementListIterator = statementList.iterator();
		
		when(mockConnection.getStatements(hasFamilyMemberResource, RDFS.SUBPROPERTYOF, hasRelativeResource, true)).thenReturn(new CloseableIteratorIteration(statementListIterator));
		when(mockConnection.addInferredStatement(hasRelativeResource, OWL2.EQUIVALENTPROPERTY, hasFamilyMemberResource, null)).thenReturn(true);
		when(mockConnection.addInferredStatement(hasFamilyMemberResource, OWL2.EQUIVALENTPROPERTY, hasRelativeResource, null)).thenReturn(true);
		
		Collection<String> rulesToApplyNextIteration = ruleUnderTest.apply(addedStatements, null, rules);
		assertEquals(RULE_DEPENDENCIES.get(SUBPROPERTY_OF_EQUIVALENT_PROPERTY_RULE), rulesToApplyNextIteration);
	}
	
	@Test
	public void testDependenciesOfDomain() throws SailException {
		Resource hasParentResource = valueFactory.createURI(HAS_PARENT);
		Resource childResource = valueFactory.createURI(CHILD);
		Resource hasMotherResource = valueFactory.createURI(HAS_MOTHER);
		
		Set<Statement> addedStatements = Sets.newHashSet(valueFactory.createStatement(hasParentResource, RDFS.DOMAIN, childResource));
		Set<String> rules = new HashSet<String>();
		rules.add(SUBPROPERTY_OF_DOMAIN_RULE);
		
		Set<Statement> statementList = new HashSet<Statement>();
		statementList.add(valueFactory.createStatement(hasMotherResource, RDFS.SUBPROPERTYOF, hasParentResource));
		Iterator<Statement> statementListIterator = statementList.iterator();
		
		when(mockConnection.getStatements(null, RDFS.SUBPROPERTYOF, null, true)).thenReturn(new CloseableIteratorIteration(statementListIterator));
		when(mockConnection.addInferredStatement(hasMotherResource, RDFS.DOMAIN, childResource, null)).thenReturn(true);
		
		Collection<String> rulesToApplyNextIteration = ruleUnderTest.apply(addedStatements, null, rules);
		assertEquals(RULE_DEPENDENCIES.get(SUBPROPERTY_OF_DOMAIN_RULE), rulesToApplyNextIteration);
	}
	
	@Test
	public void testDependenciesOfRange() throws SailException {
		Resource hasParentResource = valueFactory.createURI(HAS_PARENT);
		Resource adultResource = valueFactory.createURI(ADULT);
		Resource hasMotherResource = valueFactory.createURI(HAS_MOTHER);
		
		Set<Statement> addedStatements = Sets.newHashSet(valueFactory.createStatement(hasParentResource, RDFS.RANGE, adultResource));
		Set<String> rules = new HashSet<String>();
		rules.add(SUBPROPERTY_OF_RANGE_RULE);
		
		Set<Statement> statementList = new HashSet<Statement>();
		statementList.add(valueFactory.createStatement(hasMotherResource, RDFS.SUBPROPERTYOF, hasParentResource));
		Iterator<Statement> statementListIterator = statementList.iterator();
		
		when(mockConnection.getStatements(null, RDFS.SUBPROPERTYOF, null, true)).thenReturn(new CloseableIteratorIteration(statementListIterator));
		when(mockConnection.addInferredStatement(hasMotherResource, RDFS.RANGE, adultResource, null)).thenReturn(true);
		
		Collection<String> rulesToApplyNextIteration = ruleUnderTest.apply(addedStatements, null, rules);
		assertEquals(RULE_DEPENDENCIES.get(SUBPROPERTY_OF_RANGE_RULE), rulesToApplyNextIteration);
	}
	
}
