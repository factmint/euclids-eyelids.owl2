package com.factmint.graphhub.sail.owl2sail.rules;

import static com.factmint.graphhub.sail.owl2sail.sesame.utility.Constants.REFLEXIVE_PROPERTY_RULE;
import static com.factmint.graphhub.sail.owl2sail.sesame.utility.Constants.RULE_DEPENDENCIES;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import info.aduna.iteration.CloseableIteratorIteration;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.openrdf.model.Resource;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.ValueFactory;
import org.openrdf.model.impl.ValueFactoryImpl;
import org.openrdf.model.vocabulary.RDF;
import org.openrdf.sail.SailException;
import org.openrdf.sail.inferencer.InferencerConnection;

import com.factmint.graphhub.sail.owl2sail.axioms.OWL2;
import com.factmint.graphhub.sail.owl2sail.rules.ReflexivePropertyRule;
import com.google.common.collect.Sets;

public class ReflexivePropertyRuleTest {
	
	private static final String IS_EQUAL_TO = "http://test.owl2.com/isEqualTo";
	private static final String FIFTEEN = "http://test.owl2.com/Fifteen";
	private static final String FIVE_TIMES_THREE = "http://test.owl2.com/FiveTimesThree";
	
	private InferencerConnection mockConnection = mock(InferencerConnection.class);
	private ValueFactory valueFactory = ValueFactoryImpl.getInstance(); 
	
	private ReflexivePropertyRule ruleUnderTest;
	
	@Before
	public void setup() {
		ruleUnderTest = new ReflexivePropertyRule(mockConnection, RULE_DEPENDENCIES);
	}
	
	@Test
	public void testDependenciesOfReflexiveProperty() throws SailException {
		Resource fiveTimesThreeResource = valueFactory.createURI(FIVE_TIMES_THREE);
		Resource fifteenResource = valueFactory.createURI(FIFTEEN);
		URI isEqualToResource = valueFactory.createURI(IS_EQUAL_TO);
		
		Set<Statement> addedStatements = Sets.newHashSet(valueFactory.createStatement(fiveTimesThreeResource, isEqualToResource, fifteenResource));
		Set<String> rules = new HashSet<String>();
		rules.add(REFLEXIVE_PROPERTY_RULE);
		
		Set<Statement> statementList = new HashSet<Statement>();
		statementList.add(valueFactory.createStatement(isEqualToResource, RDF.TYPE, OWL2.REFLEXIVEPROPERTY));
		Iterator<Statement> statementListIterator = statementList.iterator();
		when(mockConnection.getStatements(null, RDF.TYPE, OWL2.REFLEXIVEPROPERTY, true)).thenReturn(new CloseableIteratorIteration(statementListIterator));
		
		Set<Statement> emptyStatementList = new HashSet<Statement>();
		Iterator<Statement> emptyStatementListIterator = emptyStatementList.iterator();
		when(mockConnection.getStatements(null, isEqualToResource, null, true)).thenReturn(new CloseableIteratorIteration(emptyStatementListIterator));
		
		when(mockConnection.addInferredStatement(fiveTimesThreeResource, isEqualToResource, fiveTimesThreeResource, null)).thenReturn(true);
		when(mockConnection.addInferredStatement(fifteenResource, isEqualToResource, fifteenResource, null)).thenReturn(true);
		
		Collection<String> rulesToApplyNextIteration = ruleUnderTest.apply(addedStatements, null, rules);
		assertEquals(RULE_DEPENDENCIES.get(REFLEXIVE_PROPERTY_RULE), rulesToApplyNextIteration);
	}
	
}
