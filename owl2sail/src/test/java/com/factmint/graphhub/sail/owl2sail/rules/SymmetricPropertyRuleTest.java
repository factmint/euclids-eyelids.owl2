package com.factmint.graphhub.sail.owl2sail.rules;

import static com.factmint.graphhub.sail.owl2sail.sesame.utility.Constants.RULE_DEPENDENCIES;
import static com.factmint.graphhub.sail.owl2sail.sesame.utility.Constants.SYMMETRIC_PROPERTY_RULE;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import info.aduna.iteration.CloseableIteratorIteration;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.openrdf.model.Resource;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.ValueFactory;
import org.openrdf.model.impl.ValueFactoryImpl;
import org.openrdf.model.vocabulary.RDF;
import org.openrdf.sail.SailException;
import org.openrdf.sail.inferencer.InferencerConnection;

import com.factmint.graphhub.sail.owl2sail.axioms.OWL2;
import com.factmint.graphhub.sail.owl2sail.rules.SymmetricPropertyRule;
import com.google.common.collect.Sets;

public class SymmetricPropertyRuleTest {

	private static final String IS_SYNONYMOUS_WITH = "http://test.owl2.com/isSynonymousWith";
	private static final String ZUCCHINI = "http://test.owl2.com/Zucchini";
	private static final String COURGETTE = "http://test.owl2.com/Courgette";
	
	private InferencerConnection mockConnection = mock(InferencerConnection.class);
	private ValueFactory valueFactory = ValueFactoryImpl.getInstance(); 
	
	private SymmetricPropertyRule ruleUnderTest;
	
	@Before
	public void setup() {
		ruleUnderTest = new SymmetricPropertyRule(mockConnection, RULE_DEPENDENCIES);
	}
	
	@Test
	public void testDependenciesOfSymmetricProperty() throws SailException {
		Resource courgetteResource = valueFactory.createURI(COURGETTE);
		Resource zucchiniResource = valueFactory.createURI(ZUCCHINI);
		URI isSynonymousWith = valueFactory.createURI(IS_SYNONYMOUS_WITH);
		
		Set<Statement> addedStatements = Sets.newHashSet(valueFactory.createStatement(isSynonymousWith, RDF.TYPE, OWL2.SYMMETRICPROPERTY));
		Set<String> rules = new HashSet<String>();
		rules.add(SYMMETRIC_PROPERTY_RULE);
		
		Set<Statement> statementList = new HashSet<Statement>();
		statementList.add(valueFactory.createStatement(courgetteResource, isSynonymousWith, zucchiniResource));
		Iterator<Statement> statementListIterator = statementList.iterator();
		when(mockConnection.getStatements(null, isSynonymousWith, null, true)).thenReturn(new CloseableIteratorIteration(statementListIterator));
		
		Set<Statement> emptyStatementList = new HashSet<Statement>();
		Iterator<Statement> emptyStatementListIterator = emptyStatementList.iterator();
		when(mockConnection.getStatements(RDF.TYPE, RDF.TYPE, OWL2.SYMMETRICPROPERTY, true)).thenReturn(new CloseableIteratorIteration(emptyStatementListIterator));
		
		when(mockConnection.addInferredStatement(zucchiniResource, isSynonymousWith, courgetteResource, null)).thenReturn(true);
		
		Collection<String> rulesToApplyNextIteration = ruleUnderTest.apply(addedStatements, null, rules);
		assertEquals(RULE_DEPENDENCIES.get(SYMMETRIC_PROPERTY_RULE), rulesToApplyNextIteration);
	}
	
}
