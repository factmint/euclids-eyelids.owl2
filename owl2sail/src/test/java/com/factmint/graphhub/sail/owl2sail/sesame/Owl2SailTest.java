package com.factmint.graphhub.sail.owl2sail.sesame;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.whenNew;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openrdf.sail.inferencer.InferencerConnection;
import org.openrdf.sail.memory.MemoryStore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.factmint.graphhub.sail.owl2sail.sesame.Owl2Sail;
import com.factmint.graphhub.sail.owl2sail.sesame.Owl2SailConnection;

@RunWith(PowerMockRunner.class)
@PrepareForTest({Owl2SailConnection.class, Owl2Sail.class})
public class Owl2SailTest {

	private Owl2Sail sailUnderTest;
	
	private Owl2SailConnection mockConnection = mock(Owl2SailConnection.class);
	
	@Before
	public void setup() {
		sailUnderTest = new Owl2Sail(new MemoryStore(), false);
	}
	
	@Test
	public void testInitialize() throws Exception {
		whenNew(Owl2SailConnection.class).withArguments(any(InferencerConnection.class), anyBoolean()).thenReturn(mockConnection);
		
		sailUnderTest.initialize();
		
		verify(mockConnection).addAxiomStatements();
	}

}
