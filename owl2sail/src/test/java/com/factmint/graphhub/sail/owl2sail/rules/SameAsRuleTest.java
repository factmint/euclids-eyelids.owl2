package com.factmint.graphhub.sail.owl2sail.rules;

import static com.factmint.graphhub.sail.owl2sail.sesame.utility.Constants.RULE_DEPENDENCIES;
import static com.factmint.graphhub.sail.owl2sail.sesame.utility.Constants.SAME_AS_REPLACEMENT_RULE;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import info.aduna.iteration.CloseableIteratorIteration;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.openrdf.model.Resource;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.ValueFactory;
import org.openrdf.model.impl.ValueFactoryImpl;
import org.openrdf.sail.SailException;
import org.openrdf.sail.inferencer.InferencerConnection;

import com.factmint.graphhub.sail.owl2sail.axioms.OWL2;
import com.factmint.graphhub.sail.owl2sail.rules.SameAsRule;
import com.google.common.collect.Sets;

public class SameAsRuleTest {

	private static final String SUBJECT_SYNONYM = "http://test.owl2.com/subjectSynonym";
	private static final String OBJECT = "http://test.owl2.com/object";
	private static final String PREDICATE = "http://test.owl2.com/predicate";
	private static final String SUBJECT = "http://test.owl2.com/subject";
	private InferencerConnection mockConnection = mock(InferencerConnection.class);
	private ValueFactory valueFactory = ValueFactoryImpl.getInstance(); 
	
	private SameAsRule ruleUnderTest;
	
	@Before
	public void setup() {
		ruleUnderTest = new SameAsRule(mockConnection, RULE_DEPENDENCIES);
	}
	
	@Test
	public void testDependenciesOfReplacement() throws SailException {
		Resource subject = valueFactory.createURI(SUBJECT);
		URI predicate = valueFactory.createURI(PREDICATE);
		Resource object = valueFactory.createURI(OBJECT);
		Resource subjectSynonym = valueFactory.createURI(SUBJECT_SYNONYM);
		
		Set<Statement> addedStatements = Sets.newHashSet(valueFactory.createStatement(subject, predicate, object));
		Set<String> rules = new HashSet<String>();
		rules.add(SAME_AS_REPLACEMENT_RULE);
		
		Set<Statement> emptyStatementList = new HashSet<Statement>();
		Iterator<Statement> emptyStatementListIterator = emptyStatementList.iterator();
		
		Set<Statement> subjectStatementList = Sets.newHashSet(valueFactory.createStatement(subject, OWL2.SAMEAS, subjectSynonym));
		Iterator<Statement> subjectStatementListIterator = subjectStatementList.iterator();
		
		when(mockConnection.getStatements(null, OWL2.SAMEAS, null, true)).thenReturn(new CloseableIteratorIteration(subjectStatementListIterator));
		when(mockConnection.addInferredStatement(subjectSynonym, predicate, object, null)).thenReturn(true);
		
		Collection<String> rulesToApplyNextIteration = ruleUnderTest.apply(addedStatements, null, rules);
		assertEquals(RULE_DEPENDENCIES.get(SAME_AS_REPLACEMENT_RULE), rulesToApplyNextIteration);
	}

}
