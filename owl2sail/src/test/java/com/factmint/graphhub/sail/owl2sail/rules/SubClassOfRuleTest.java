package com.factmint.graphhub.sail.owl2sail.rules;

import static com.factmint.graphhub.sail.owl2sail.sesame.utility.Constants.RULE_DEPENDENCIES;
import static com.factmint.graphhub.sail.owl2sail.sesame.utility.Constants.SUBCLASS_OF_CLASS_AXIOM_RULE;
import static com.factmint.graphhub.sail.owl2sail.sesame.utility.Constants.SUBCLASS_OF_DOMAIN_RULE;
import static com.factmint.graphhub.sail.owl2sail.sesame.utility.Constants.SUBCLASS_OF_EQUIVALENT_CLASS_RULE;
import static com.factmint.graphhub.sail.owl2sail.sesame.utility.Constants.SUBCLASS_OF_RANGE_RULE;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import info.aduna.iteration.CloseableIteratorIteration;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.openrdf.model.Resource;
import org.openrdf.model.Statement;
import org.openrdf.model.ValueFactory;
import org.openrdf.model.impl.ValueFactoryImpl;
import org.openrdf.model.vocabulary.RDF;
import org.openrdf.model.vocabulary.RDFS;
import org.openrdf.sail.SailException;
import org.openrdf.sail.inferencer.InferencerConnection;

import com.factmint.graphhub.sail.owl2sail.axioms.OWL2;
import com.factmint.graphhub.sail.owl2sail.rules.SubClassOfRule;
import com.google.common.collect.Sets;

public class SubClassOfRuleTest {

	private static final String LIVING_THING = "http://test.owl2.com/LivingThing";
	private static final String ORGANISM = "http://test.owl2.com/Organism";
	private static final String ANIMAL = "http://test.owl2.com/Animal";
	private static final String MAMMAL = "http://test.owl2.com/Mammal";
	private static final String TIGER = "http://test.owl2.com/Tiger";
	private static final String DEER = "http://test.owl2.com/Deer";
	private static final String PREDATOR = "http://test.owl2.com/Predator";
	private static final String PREY = "http://test.owl2.com/Prey";
	private static final String HUNTS = "http://test.owl2.com/hunts";
	
	private InferencerConnection mockConnection = mock(InferencerConnection.class);
	private ValueFactory valueFactory = ValueFactoryImpl.getInstance(); 
	
	private SubClassOfRule ruleUnderTest;
	
	@Before
	public void setup() {
		ruleUnderTest = new SubClassOfRule(mockConnection, RULE_DEPENDENCIES);
	}
	
	@Test
	public void testDependenciesOfClassAxiom() throws SailException {
		Resource animalResource = valueFactory.createURI(ANIMAL);
		Resource mammalResource = valueFactory.createURI(MAMMAL);
		Resource tigerResource = valueFactory.createURI(TIGER);
		
		Set<Statement> addedStatements = Sets.newHashSet(valueFactory.createStatement(mammalResource, RDFS.SUBCLASSOF, animalResource));
		Set<String> rules = new HashSet<String>();
		rules.add(SUBCLASS_OF_CLASS_AXIOM_RULE);
		
		Set<Statement> statementList = new HashSet<Statement>();
		statementList.add(valueFactory.createStatement(tigerResource, RDF.TYPE, mammalResource));
		Iterator<Statement> statementListIterator = statementList.iterator();
		
		when(mockConnection.getStatements(null, RDF.TYPE, null, true)).thenReturn(new CloseableIteratorIteration(statementListIterator));
		when(mockConnection.addInferredStatement(tigerResource, RDF.TYPE, animalResource, null)).thenReturn(true);
		
		Collection<String> rulesToApplyNextIteration = ruleUnderTest.apply(addedStatements, null, rules);
		assertEquals(RULE_DEPENDENCIES.get(SUBCLASS_OF_CLASS_AXIOM_RULE), rulesToApplyNextIteration);
	}
	
	@Test
	public void testDependenciesOfEquivalentClass() throws SailException {
		Resource livingThingResource = valueFactory.createURI(LIVING_THING);
		Resource organismResource = valueFactory.createURI(ORGANISM);
		
		Set<Statement> addedStatements = Sets.newHashSet(valueFactory.createStatement(livingThingResource, RDFS.SUBCLASSOF, organismResource));
		Set<String> rules = new HashSet<String>();
		rules.add(SUBCLASS_OF_EQUIVALENT_CLASS_RULE);
		
		Set<Statement> statementList = new HashSet<Statement>();
		statementList.add(valueFactory.createStatement(organismResource, RDFS.SUBCLASSOF, livingThingResource));
		Iterator<Statement> statementListIterator = statementList.iterator();
		
		when(mockConnection.getStatements(organismResource, RDFS.SUBCLASSOF, livingThingResource, true)).thenReturn(new CloseableIteratorIteration(statementListIterator));
		when(mockConnection.addInferredStatement(livingThingResource, OWL2.EQUIVALENTCLASS, organismResource, null)).thenReturn(true);
		when(mockConnection.addInferredStatement(organismResource, OWL2.EQUIVALENTCLASS, livingThingResource, null)).thenReturn(true);
		
		Collection<String> rulesToApplyNextIteration = ruleUnderTest.apply(addedStatements, null, rules);
		assertEquals(RULE_DEPENDENCIES.get(SUBCLASS_OF_EQUIVALENT_CLASS_RULE), rulesToApplyNextIteration);
	}
	
	@Test
	public void testDependenciesOfDomain() throws SailException {
		Resource huntsResource = valueFactory.createURI(HUNTS);
		Resource predatorResource = valueFactory.createURI(PREDATOR);
		Resource tigerResource = valueFactory.createURI(TIGER);
		
		Set<Statement> addedStatements = Sets.newHashSet(valueFactory.createStatement(huntsResource, RDFS.DOMAIN, tigerResource));
		Set<String> rules = new HashSet<String>();
		rules.add(SUBCLASS_OF_DOMAIN_RULE);
		
		Set<Statement> statementList = new HashSet<Statement>();
		statementList.add(valueFactory.createStatement(tigerResource, RDFS.SUBCLASSOF, predatorResource));
		Iterator<Statement> statementListIterator = statementList.iterator();
		
		when(mockConnection.getStatements(null, RDFS.SUBCLASSOF, null, true)).thenReturn(new CloseableIteratorIteration(statementListIterator));
		when(mockConnection.addInferredStatement(huntsResource, RDFS.DOMAIN, predatorResource, null)).thenReturn(true);
		
		Collection<String> rulesToApplyNextIteration = ruleUnderTest.apply(addedStatements, null, rules);
		assertEquals(RULE_DEPENDENCIES.get(SUBCLASS_OF_DOMAIN_RULE), rulesToApplyNextIteration);
	}
	
	@Test
	public void testDependenciesOfRange() throws SailException {
		Resource huntsResource = valueFactory.createURI(HUNTS);
		Resource preyResource = valueFactory.createURI(PREY);
		Resource deerResource = valueFactory.createURI(DEER);
		
		Set<Statement> addedStatements = Sets.newHashSet(valueFactory.createStatement(huntsResource, RDFS.RANGE, deerResource));
		Set<String> rules = new HashSet<String>();
		rules.add(SUBCLASS_OF_RANGE_RULE);
		
		Set<Statement> statementList = new HashSet<Statement>();
		statementList.add(valueFactory.createStatement(deerResource, RDFS.SUBCLASSOF, preyResource));
		Iterator<Statement> statementListIterator = statementList.iterator();
		
		when(mockConnection.getStatements(null, RDFS.SUBCLASSOF, null, true)).thenReturn(new CloseableIteratorIteration(statementListIterator));
		when(mockConnection.addInferredStatement(huntsResource, RDFS.RANGE, preyResource, null)).thenReturn(true);
		
		Collection<String> rulesToApplyNextIteration = ruleUnderTest.apply(addedStatements, null, rules);
		assertEquals(RULE_DEPENDENCIES.get(SUBCLASS_OF_RANGE_RULE), rulesToApplyNextIteration);
	}
	
}
