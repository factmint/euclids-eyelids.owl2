package com.factmint.graphhub.sail.owl2sail.rules;

import static com.factmint.graphhub.sail.owl2sail.sesame.utility.Constants.RDF_LIST_PREDICATE_PREFIX;
import static com.factmint.graphhub.sail.owl2sail.sesame.utility.Constants.SAME_AS_DIFFERENCE_RULE;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;

import org.junit.Test;
import org.openrdf.model.Resource;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.Value;
import org.openrdf.model.vocabulary.RDF;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.RepositoryResult;
import org.openrdf.rio.RDFFormat;
import org.openrdf.rio.RDFParseException;

import com.factmint.graphhub.sail.owl2sail.Owl2TestBase;
import com.factmint.graphhub.sail.owl2sail.axioms.OWL2;

public class SameAsRuleIT extends Owl2TestBase {

	private static final String MARGARET_THATCHER = "http://test.owl2.com/MargaretThatcher";
	private static final String IRON_LADY = "http://test.owl2.com/IronLady";

	@Test
	public void testReflexivity() throws RepositoryException {
		assertTrue(connection.hasStatement(OWL2.SAMEAS, RDF.TYPE, OWL2.REFLEXIVEPROPERTY, true));
		
		Resource margaretThatcherResource = valueFactory.createURI(MARGARET_THATCHER);
		
		URI hasNameUri = valueFactory.createURI("http://test.owl2.com/hasName");
		Value name = valueFactory.createLiteral("Margaret Thatcher");
		
		URI hasPartyUri = valueFactory.createURI("http://test.owl2.com/hasParty");
		Resource conservativePartyResource = valueFactory.createURI("http://test.owl2.com/ConservativeParty");
				
		connection.add(margaretThatcherResource, hasPartyUri, conservativePartyResource);
		assertTrue(connection.hasStatement(margaretThatcherResource, hasPartyUri, conservativePartyResource, false));
		assertTrue(connection.hasStatement(margaretThatcherResource, OWL2.SAMEAS, margaretThatcherResource, true));
		assertTrue(connection.hasStatement(hasPartyUri, OWL2.SAMEAS, hasPartyUri, true));
		assertTrue(connection.hasStatement(conservativePartyResource, OWL2.SAMEAS, conservativePartyResource, true));
		
		connection.add(margaretThatcherResource, hasNameUri, name);
		assertTrue(connection.hasStatement(margaretThatcherResource, hasNameUri, name, false));
				
		connection.remove(margaretThatcherResource, hasPartyUri, conservativePartyResource);
		assertFalse(connection.hasStatement(margaretThatcherResource, hasPartyUri, conservativePartyResource, false));
		
		assertTrue(connection.hasStatement(margaretThatcherResource, hasNameUri, name, false));
		assertTrue(connection.hasStatement(margaretThatcherResource, OWL2.SAMEAS, margaretThatcherResource, true));
		assertFalse(connection.hasStatement(hasPartyUri, OWL2.SAMEAS, hasPartyUri, true));
		assertFalse(connection.hasStatement(conservativePartyResource, OWL2.SAMEAS, conservativePartyResource, true));
		
		connection.remove(margaretThatcherResource, hasNameUri, name);
		assertFalse(connection.hasStatement(margaretThatcherResource, hasNameUri, name, false));
		assertFalse(connection.hasStatement(margaretThatcherResource, hasNameUri, name, true));
		assertFalse(connection.hasStatement(margaretThatcherResource, OWL2.SAMEAS, margaretThatcherResource, true));
		assertFalse(connection.hasStatement(hasNameUri, OWL2.SAMEAS, hasNameUri, true));
	}

	@Test
	public void testSymmetry() throws RepositoryException {
		assertTrue(connection.hasStatement(OWL2.SAMEAS, RDF.TYPE, OWL2.SYMMETRICPROPERTY, true));
		
		Resource margaretThatcherResource = valueFactory.createURI(MARGARET_THATCHER);
		Resource ironLadyResource = valueFactory.createURI(IRON_LADY);
				
		connection.add(margaretThatcherResource, OWL2.SAMEAS, ironLadyResource);
		
		assertTrue(connection.hasStatement(margaretThatcherResource, OWL2.SAMEAS, ironLadyResource, false));
		assertTrue(connection.hasStatement(ironLadyResource, OWL2.SAMEAS, margaretThatcherResource, true));
		
		connection.remove(margaretThatcherResource, OWL2.SAMEAS, ironLadyResource);
		
		assertFalse(connection.hasStatement(margaretThatcherResource, OWL2.SAMEAS, ironLadyResource, false));
		assertFalse(connection.hasStatement(ironLadyResource, OWL2.SAMEAS, margaretThatcherResource, true));
	}

	@Test
	public void testTransitivity() throws RepositoryException {
		assertTrue(connection.hasStatement(OWL2.SAMEAS, RDF.TYPE, OWL2.TRANSITIVEPROPERTY, true));
		
		Resource margaretThatcherResource = valueFactory.createURI(MARGARET_THATCHER);
		Resource ironLadyResource = valueFactory.createURI(IRON_LADY);
		Resource primeMinisterResource = valueFactory.createURI("http://test.owl2.com/PM_of_the_UK_between_1979_and_1990");
		
		connection.add(margaretThatcherResource, OWL2.SAMEAS, ironLadyResource);
		connection.add(ironLadyResource, OWL2.SAMEAS, primeMinisterResource);

		assertTrue(connection.hasStatement(margaretThatcherResource, OWL2.SAMEAS, ironLadyResource, false));
		assertTrue(connection.hasStatement(ironLadyResource, OWL2.SAMEAS, primeMinisterResource, false));
		assertTrue(connection.hasStatement(margaretThatcherResource, OWL2.SAMEAS, primeMinisterResource, true));
		
		connection.remove(ironLadyResource, OWL2.SAMEAS, primeMinisterResource);
		
		assertFalse(connection.hasStatement(ironLadyResource, OWL2.SAMEAS, primeMinisterResource, false));
		assertFalse(connection.hasStatement(margaretThatcherResource, OWL2.SAMEAS, primeMinisterResource, true));
	}

	@Test
	public void testReplacementRuleForSameAsStatement() throws RepositoryException {
		Resource margaretThatcherResource = valueFactory.createURI(MARGARET_THATCHER);
		Resource ironLadyResource = valueFactory.createURI(IRON_LADY);
		
		URI hasNameUri = valueFactory.createURI("http://test.owl2.com/hasName");
		Value name = valueFactory.createLiteral("Margaret Thatcher");
		
		connection.add(margaretThatcherResource, hasNameUri, name);
		assertTrue(connection.hasStatement(margaretThatcherResource, hasNameUri, name, false));
		
		connection.add(margaretThatcherResource, OWL2.SAMEAS, ironLadyResource);
		assertTrue(connection.hasStatement(margaretThatcherResource, OWL2.SAMEAS, ironLadyResource, false));
		assertTrue(connection.hasStatement(ironLadyResource, hasNameUri, name, true));
		
		connection.remove(margaretThatcherResource, OWL2.SAMEAS, ironLadyResource);
		assertFalse(connection.hasStatement(margaretThatcherResource, OWL2.SAMEAS, ironLadyResource, false));
		assertFalse(connection.hasStatement(ironLadyResource, hasNameUri, name, true));
	}
	
	@Test
	public void testReplacementRuleForNonSameAsStatement() throws RepositoryException {
		Resource margaretThatcherResource = valueFactory.createURI(MARGARET_THATCHER);
		Resource ironLadyResource = valueFactory.createURI(IRON_LADY);
		
		URI hasNameUri = valueFactory.createURI("http://test.owl2.com/hasName");
		Value name = valueFactory.createLiteral("Margaret Thatcher");
		
		URI hasPartyUri = valueFactory.createURI("http://test.owl2.com/hasParty");
		Resource conservativePartyResource = valueFactory.createURI("http://test.owl2.com/ConservativeParty");
		
		connection.add(margaretThatcherResource, OWL2.SAMEAS, ironLadyResource);
		assertTrue(connection.hasStatement(margaretThatcherResource, OWL2.SAMEAS, ironLadyResource, false));
		
		connection.add(margaretThatcherResource, hasNameUri, name);
		assertTrue(connection.hasStatement(margaretThatcherResource, hasNameUri, name, false));
		assertTrue(connection.hasStatement(ironLadyResource, hasNameUri, name, true));
		
		connection.add(ironLadyResource, OWL2.SAMEAS, margaretThatcherResource);
		assertTrue(connection.hasStatement(ironLadyResource, OWL2.SAMEAS, margaretThatcherResource, false));
		
		connection.add(ironLadyResource, hasPartyUri, conservativePartyResource);
		assertTrue(connection.hasStatement(ironLadyResource, hasPartyUri, conservativePartyResource, false));
		assertTrue(connection.hasStatement(margaretThatcherResource, hasPartyUri, conservativePartyResource, true));
		
		connection.remove(margaretThatcherResource, hasNameUri, name);
		assertFalse(connection.hasStatement(margaretThatcherResource, hasNameUri, name, false));
		assertFalse(connection.hasStatement(ironLadyResource, hasNameUri, name, true));
		
		connection.remove(ironLadyResource, hasPartyUri, conservativePartyResource);
		assertFalse(connection.hasStatement(ironLadyResource, hasPartyUri, conservativePartyResource, false));
		assertFalse(connection.hasStatement(margaretThatcherResource,hasPartyUri, conservativePartyResource, true));
	}

	@Test(expected=RepositoryException.class)
	public void testDifferenceRuleWithSameAsAndDifferentFrom() throws RepositoryException {
		Resource margaretThatcherResource = valueFactory.createURI(MARGARET_THATCHER);
		Resource ironLadyResource = valueFactory.createURI(IRON_LADY);
				
		connection.add(margaretThatcherResource, OWL2.SAMEAS, ironLadyResource);
		assertTrue(connection.hasStatement(margaretThatcherResource, OWL2.SAMEAS, ironLadyResource, false));
		
		try {
			connection.add(margaretThatcherResource, OWL2.DIFFERENTFROM, ironLadyResource);
		} catch (RepositoryException e) {
			assertTrue(e.getMessage().contains(SAME_AS_DIFFERENCE_RULE));
			throw e;
		}
	}
	
	@Test(expected=RepositoryException.class)
	public void testDifferenceRuleWithDifferentFromAndSameAs() throws RepositoryException {
		Resource margaretThatcherResource = valueFactory.createURI(MARGARET_THATCHER);
		Resource ironLadyResource = valueFactory.createURI(IRON_LADY);
				
		connection.add(margaretThatcherResource, OWL2.DIFFERENTFROM, ironLadyResource);
		assertTrue(connection.hasStatement(margaretThatcherResource, OWL2.DIFFERENTFROM, ironLadyResource, false));
		
		try {
			connection.add(margaretThatcherResource, OWL2.SAMEAS, ironLadyResource);
		} catch (RepositoryException e) {
			assertTrue(e.getMessage().contains(SAME_AS_DIFFERENCE_RULE));		
			throw e;
		}
	}
	
	@Test(expected=RepositoryException.class)
	public void testDifferenceRuleWithAllDifferent1a() throws RepositoryException, RDFParseException, IOException {
		InputStream ukPrimeMinistersContent = this.getClass().getResourceAsStream("/uk-prime-ministers.xml");
		connection.add(ukPrimeMinistersContent, "http://test.owl2.com", RDFFormat.RDFXML);
		
		Resource primeMinistersListResource = valueFactory.createURI("http://test.owl2.com#PrimeMinistersList");
		Collection<String> primeMinisterNames = queryPrimeMinisterNames(primeMinistersListResource);
		assertTrue(primeMinisterNames.contains(MARGARET_THATCHER));
		assertTrue(primeMinisterNames.contains(IRON_LADY));
		
		Resource margaretThatcherResource = valueFactory.createURI(MARGARET_THATCHER);
		Resource ironLadyResource = valueFactory.createURI(IRON_LADY);
		connection.add(margaretThatcherResource, OWL2.SAMEAS, ironLadyResource);		
		assertTrue(connection.hasStatement(margaretThatcherResource, OWL2.SAMEAS, ironLadyResource, false));
		
		Resource primeMinistersResource = valueFactory.createURI("http://test.owl2.com/UK#PrimeMinisters");
		connection.add(primeMinistersResource, OWL2.MEMBERS, primeMinistersListResource);		
		assertTrue(connection.hasStatement(primeMinistersResource, OWL2.MEMBERS, primeMinistersListResource, false));
		
		try {
			connection.add(primeMinistersResource, RDF.TYPE, OWL2.ALLDIFFERENT);
		} catch (RepositoryException e) {
			assertTrue(e.getMessage().contains(SAME_AS_DIFFERENCE_RULE));			
			throw e;
		}
	}
	
	@Test(expected=RepositoryException.class)
	public void testDifferenceRuleWithAllDifferent1b() throws RepositoryException, RDFParseException, IOException {
		InputStream ukPrimeMinistersContent = this.getClass().getResourceAsStream("/uk-prime-ministers.xml");
		connection.add(ukPrimeMinistersContent, "http://test.owl2.com", RDFFormat.RDFXML);
		
		Resource primeMinistersListResource = valueFactory.createURI("http://test.owl2.com#PrimeMinistersList");
		Collection<String> primeMinisterNames = queryPrimeMinisterNames(primeMinistersListResource);
		assertTrue(primeMinisterNames.contains(MARGARET_THATCHER));
		assertTrue(primeMinisterNames.contains(IRON_LADY));
		
		Resource margaretThatcherResource = valueFactory.createURI(MARGARET_THATCHER);
		Resource ironLadyResource = valueFactory.createURI(IRON_LADY);
		connection.add(margaretThatcherResource, OWL2.SAMEAS, ironLadyResource);		
		assertTrue(connection.hasStatement(margaretThatcherResource, OWL2.SAMEAS, ironLadyResource, false));
		
		Resource primeMinistersResource = valueFactory.createURI("http://test.owl2.com/UK#PrimeMinisters");
		connection.add(primeMinistersResource, OWL2.DISTINCTMEMBERS, primeMinistersListResource);		
		assertTrue(connection.hasStatement(primeMinistersResource, OWL2.DISTINCTMEMBERS, primeMinistersListResource, false));
		
		try {
			connection.add(primeMinistersResource, RDF.TYPE, OWL2.ALLDIFFERENT);
		} catch (RepositoryException e) {
			assertTrue(e.getMessage().contains(SAME_AS_DIFFERENCE_RULE));			
			throw e;
		}
	}
	
	@Test(expected=RepositoryException.class)
	public void testDifferenceRuleWithAllDifferent1c() throws RepositoryException, RDFParseException, IOException {
		InputStream ukPrimeMinistersContent = this.getClass().getResourceAsStream("/uk-prime-ministers.xml");
		connection.add(ukPrimeMinistersContent, "http://test.owl2.com", RDFFormat.RDFXML);
		
		Resource primeMinistersListResource = valueFactory.createURI("http://test.owl2.com#PrimeMinistersList");
		Collection<String> primeMinisterNames = queryPrimeMinisterNames(primeMinistersListResource);
		assertTrue(primeMinisterNames.contains(MARGARET_THATCHER));
		assertTrue(primeMinisterNames.contains(IRON_LADY));
		
		InputStream ukPrimeMinistersNoDuplicatesContent = this.getClass().getResourceAsStream("/uk-prime-ministers-no-duplicates.xml");
		connection.add(ukPrimeMinistersNoDuplicatesContent, "http://test.owl2.com", RDFFormat.RDFXML);
		
		Resource primeMinistersListNoDuplicatesResource = valueFactory.createURI("http://test.owl2.com#PrimeMinistersListNoDuplicates");
		Collection<String> primeMinisterNamesNoDuplicates = queryPrimeMinisterNames(primeMinistersListNoDuplicatesResource);
		assertFalse(primeMinisterNamesNoDuplicates.contains(MARGARET_THATCHER));
		assertFalse(primeMinisterNamesNoDuplicates.contains(IRON_LADY));
		
		Resource margaretThatcherResource = valueFactory.createURI(MARGARET_THATCHER);
		Resource ironLadyResource = valueFactory.createURI(IRON_LADY);
		connection.add(margaretThatcherResource, OWL2.SAMEAS, ironLadyResource);		
		assertTrue(connection.hasStatement(margaretThatcherResource, OWL2.SAMEAS, ironLadyResource, false));
		
		Resource primeMinistersResource = valueFactory.createURI("http://test.owl2.com/UK#PrimeMinisters");
		connection.add(primeMinistersResource, OWL2.MEMBERS, primeMinistersListNoDuplicatesResource);		
		assertTrue(connection.hasStatement(primeMinistersResource, OWL2.MEMBERS, primeMinistersListNoDuplicatesResource, false));
		
		// There should be no "difference" rule conflict, as the first prime ministers list does not include {MargaretThatcher, IronLady}
		connection.add(primeMinistersResource, RDF.TYPE, OWL2.ALLDIFFERENT);
		assertTrue(connection.hasStatement(primeMinistersResource, RDF.TYPE, OWL2.ALLDIFFERENT, false));
		
		connection.remove(primeMinistersResource, RDF.TYPE, OWL2.ALLDIFFERENT);
		
		connection.add(primeMinistersResource, OWL2.MEMBERS, primeMinistersListResource);		
		assertTrue(connection.hasStatement(primeMinistersResource, OWL2.MEMBERS, primeMinistersListResource, false));

		try {
			// There should be a "difference" rule conflict, as the second prime ministers list includes {MargaretThatcher, IronLady}
			connection.add(primeMinistersResource, RDF.TYPE, OWL2.ALLDIFFERENT);
		} catch (RepositoryException e) {
			assertTrue(e.getMessage().contains(SAME_AS_DIFFERENCE_RULE));			
			throw e;
		}
	}
	
	@Test(expected=RepositoryException.class)
	public void testDifferenceRuleWithAllDifferent1d() throws RepositoryException, RDFParseException, IOException {
		InputStream ukPrimeMinistersContent = this.getClass().getResourceAsStream("/uk-prime-ministers.xml");
		connection.add(ukPrimeMinistersContent, "http://test.owl2.com", RDFFormat.RDFXML);
		
		Resource primeMinistersListResource = valueFactory.createURI("http://test.owl2.com#PrimeMinistersList");
		Collection<String> primeMinisterNames = queryPrimeMinisterNames(primeMinistersListResource);
		assertTrue(primeMinisterNames.contains(MARGARET_THATCHER));
		assertTrue(primeMinisterNames.contains(IRON_LADY));
		
		InputStream ukPrimeMinistersNoDuplicatesContent = this.getClass().getResourceAsStream("/uk-prime-ministers-no-duplicates.xml");
		connection.add(ukPrimeMinistersNoDuplicatesContent, "http://test.owl2.com", RDFFormat.RDFXML);
		
		Resource primeMinistersListNoDuplicatesResource = valueFactory.createURI("http://test.owl2.com#PrimeMinistersListNoDuplicates");
		Collection<String> primeMinisterNamesNoDuplicates = queryPrimeMinisterNames(primeMinistersListNoDuplicatesResource);
		assertFalse(primeMinisterNamesNoDuplicates.contains(MARGARET_THATCHER));
		assertFalse(primeMinisterNamesNoDuplicates.contains(IRON_LADY));
		
		Resource margaretThatcherResource = valueFactory.createURI(MARGARET_THATCHER);
		Resource ironLadyResource = valueFactory.createURI(IRON_LADY);
		connection.add(margaretThatcherResource, OWL2.SAMEAS, ironLadyResource);		
		assertTrue(connection.hasStatement(margaretThatcherResource, OWL2.SAMEAS, ironLadyResource, false));
		
		Resource primeMinistersResource = valueFactory.createURI("http://test.owl2.com/UK#PrimeMinisters");
		connection.add(primeMinistersResource, OWL2.DISTINCTMEMBERS, primeMinistersListNoDuplicatesResource);		
		assertTrue(connection.hasStatement(primeMinistersResource, OWL2.DISTINCTMEMBERS, primeMinistersListNoDuplicatesResource, false));
		
		// There should be no "difference" rule conflict, as the first prime ministers list does not include {MargaretThatcher, IronLady}
		connection.add(primeMinistersResource, RDF.TYPE, OWL2.ALLDIFFERENT);
		assertTrue(connection.hasStatement(primeMinistersResource, RDF.TYPE, OWL2.ALLDIFFERENT, false));
		
		connection.remove(primeMinistersResource, RDF.TYPE, OWL2.ALLDIFFERENT);
		
		connection.add(primeMinistersResource, OWL2.DISTINCTMEMBERS, primeMinistersListResource);		
		assertTrue(connection.hasStatement(primeMinistersResource, OWL2.DISTINCTMEMBERS, primeMinistersListResource, false));

		try {
			// There should be a "difference" rule conflict, as the second prime ministers list includes {MargaretThatcher, IronLady}
			connection.add(primeMinistersResource, RDF.TYPE, OWL2.ALLDIFFERENT);
		} catch (RepositoryException e) {
			assertTrue(e.getMessage().contains(SAME_AS_DIFFERENCE_RULE));			
			throw e;
		}
	}
	
	@Test(expected=RepositoryException.class)
	public void testDifferenceRuleWithAllDifferent2a() throws RepositoryException, RDFParseException, IOException {
		InputStream ukPrimeMinistersContent = this.getClass().getResourceAsStream("/uk-prime-ministers.xml");
		connection.add(ukPrimeMinistersContent, "http://test.owl2.com", RDFFormat.RDFXML);
		
		Resource primeMinistersListResource = valueFactory.createURI("http://test.owl2.com#PrimeMinistersList");
		Collection<String> primeMinisterNames = queryPrimeMinisterNames(primeMinistersListResource);
		assertTrue(primeMinisterNames.contains(MARGARET_THATCHER));
		assertTrue(primeMinisterNames.contains(IRON_LADY));
		
		Resource margaretThatcherResource = valueFactory.createURI(MARGARET_THATCHER);
		Resource ironLadyResource = valueFactory.createURI(IRON_LADY);
		connection.add(margaretThatcherResource, OWL2.SAMEAS, ironLadyResource);		
		assertTrue(connection.hasStatement(margaretThatcherResource, OWL2.SAMEAS, ironLadyResource, false));
		
		Resource primeMinistersResource = valueFactory.createURI("http://test.owl2.com/UK#PrimeMinisters");
		connection.add(primeMinistersResource, RDF.TYPE, OWL2.ALLDIFFERENT);		
		assertTrue(connection.hasStatement(primeMinistersResource, RDF.TYPE, OWL2.ALLDIFFERENT, false));
		
		try {
			connection.add(primeMinistersResource, OWL2.MEMBERS, primeMinistersListResource);
		} catch (RepositoryException e) {
			assertTrue(e.getMessage().contains(SAME_AS_DIFFERENCE_RULE));		
			throw e;
		}
	}

	@Test(expected=RepositoryException.class)
	public void testDifferenceRuleWithAllDifferent2b() throws RepositoryException, RDFParseException, IOException {
		InputStream ukPrimeMinistersContent = this.getClass().getResourceAsStream("/uk-prime-ministers.xml");
		connection.add(ukPrimeMinistersContent, "http://test.owl2.com", RDFFormat.RDFXML);
		
		Resource primeMinistersListResource = valueFactory.createURI("http://test.owl2.com#PrimeMinistersList");
		Collection<String> primeMinisterNames = queryPrimeMinisterNames(primeMinistersListResource);
		assertTrue(primeMinisterNames.contains(MARGARET_THATCHER));
		assertTrue(primeMinisterNames.contains(IRON_LADY));
		
		Resource margaretThatcherResource = valueFactory.createURI(MARGARET_THATCHER);
		Resource ironLadyResource = valueFactory.createURI(IRON_LADY);
		connection.add(margaretThatcherResource, OWL2.SAMEAS, ironLadyResource);		
		assertTrue(connection.hasStatement(margaretThatcherResource, OWL2.SAMEAS, ironLadyResource, false));
		
		Resource primeMinistersResource = valueFactory.createURI("http://test.owl2.com/UK#PrimeMinisters");
		connection.add(primeMinistersResource, RDF.TYPE, OWL2.ALLDIFFERENT);		
		assertTrue(connection.hasStatement(primeMinistersResource, RDF.TYPE, OWL2.ALLDIFFERENT, false));
		
		try {
			connection.add(primeMinistersResource, OWL2.DISTINCTMEMBERS, primeMinistersListResource);
		} catch (RepositoryException e) {
			assertTrue(e.getMessage().contains(SAME_AS_DIFFERENCE_RULE));		
			throw e;
		}
	}
	
	@Test(expected=RepositoryException.class)
	public void testDifferenceRuleWithAllDifferent3a() throws RepositoryException, RDFParseException, IOException {
		Resource margaretThatcherResource = valueFactory.createURI(MARGARET_THATCHER);
		Resource ironLadyResource = valueFactory.createURI(IRON_LADY);
		connection.add(margaretThatcherResource, OWL2.SAMEAS, ironLadyResource);		
		assertTrue(connection.hasStatement(margaretThatcherResource, OWL2.SAMEAS, ironLadyResource, false));
		
		Resource primeMinistersResource = valueFactory.createURI("http://test.owl2.com/UK#PrimeMinisters");
		connection.add(primeMinistersResource, RDF.TYPE, OWL2.ALLDIFFERENT);		
		assertTrue(connection.hasStatement(primeMinistersResource, RDF.TYPE, OWL2.ALLDIFFERENT, false));
		
		Resource primeMinistersListResource = valueFactory.createURI("http://test.owl2.com#PrimeMinistersList");
		connection.add(primeMinistersResource, OWL2.MEMBERS, primeMinistersListResource);
		assertTrue(connection.hasStatement(primeMinistersResource, OWL2.MEMBERS, primeMinistersListResource, false));
		
		try {
			InputStream ukPrimeMinistersContent = this.getClass().getResourceAsStream("/uk-prime-ministers.xml");
			connection.add(ukPrimeMinistersContent, "http://test.owl2.com", RDFFormat.RDFXML);
		} catch (RepositoryException e) {
			assertTrue(e.getMessage().contains(SAME_AS_DIFFERENCE_RULE));		
			throw e;
		}
	}
	
	@Test(expected=RepositoryException.class)
	public void testDifferenceRuleWithAllDifferent3b() throws RepositoryException, RDFParseException, IOException {
		Resource margaretThatcherResource = valueFactory.createURI(MARGARET_THATCHER);
		Resource ironLadyResource = valueFactory.createURI(IRON_LADY);
		connection.add(margaretThatcherResource, OWL2.SAMEAS, ironLadyResource);		
		assertTrue(connection.hasStatement(margaretThatcherResource, OWL2.SAMEAS, ironLadyResource, false));
		
		Resource primeMinistersResource = valueFactory.createURI("http://test.owl2.com/UK#PrimeMinisters");
		connection.add(primeMinistersResource, RDF.TYPE, OWL2.ALLDIFFERENT);		
		assertTrue(connection.hasStatement(primeMinistersResource, RDF.TYPE, OWL2.ALLDIFFERENT, false));
		
		Resource primeMinistersListResource = valueFactory.createURI("http://test.owl2.com#PrimeMinistersList");
		connection.add(primeMinistersResource, OWL2.DISTINCTMEMBERS, primeMinistersListResource);
		assertTrue(connection.hasStatement(primeMinistersResource, OWL2.DISTINCTMEMBERS, primeMinistersListResource, false));
		
		try {
			InputStream ukPrimeMinistersContent = this.getClass().getResourceAsStream("/uk-prime-ministers.xml");
			connection.add(ukPrimeMinistersContent, "http://test.owl2.com", RDFFormat.RDFXML);
		} catch (RepositoryException e) {
			assertTrue(e.getMessage().contains(SAME_AS_DIFFERENCE_RULE));		
			throw e;
		}
	}
	
	@Test(expected=RepositoryException.class)
	public void testDifferenceRuleWithAllDifferent3c() throws RepositoryException, RDFParseException, IOException {
		Resource margaretThatcherResource = valueFactory.createURI(MARGARET_THATCHER);
		Resource ironLadyResource = valueFactory.createURI(IRON_LADY);
		connection.add(margaretThatcherResource, OWL2.SAMEAS, ironLadyResource);		
		assertTrue(connection.hasStatement(margaretThatcherResource, OWL2.SAMEAS, ironLadyResource, false));
		
		Resource primeMinistersListResource = valueFactory.createURI("http://test.owl2.com#PrimeMinistersList");

		Resource primeMinistersResource = valueFactory.createURI("http://test.owl2.com/UK#PrimeMinisters");
		connection.add(primeMinistersResource, OWL2.MEMBERS, primeMinistersListResource);
		assertTrue(connection.hasStatement(primeMinistersResource, OWL2.MEMBERS, primeMinistersListResource, false));
		
		InputStream ukPrimeMinistersContent = this.getClass().getResourceAsStream("/uk-prime-ministers.xml");
		connection.add(ukPrimeMinistersContent, "http://test.owl2.com", RDFFormat.RDFXML);
		
		Collection<String> primeMinisterNames = queryPrimeMinisterNames(primeMinistersListResource);
		assertTrue(primeMinisterNames.contains(MARGARET_THATCHER));
		assertTrue(primeMinisterNames.contains(IRON_LADY));
		
		RepositoryResult<Statement> results = connection.getStatements(primeMinistersListResource, null, null, true);
		connection.remove(results);
		
		Resource primeMinistersResourceWithAllDifferent = valueFactory.createURI("http://test.owl2.com/UK#PrimeMinistersAllDifferent");
		connection.add(primeMinistersResourceWithAllDifferent, RDF.TYPE, OWL2.ALLDIFFERENT);		
		connection.add(primeMinistersResourceWithAllDifferent, OWL2.MEMBERS, primeMinistersListResource);
		assertTrue(connection.hasStatement(primeMinistersResourceWithAllDifferent, RDF.TYPE, OWL2.ALLDIFFERENT, false));
		assertTrue(connection.hasStatement(primeMinistersResourceWithAllDifferent, OWL2.MEMBERS, primeMinistersListResource, false));
		
		try {
			ukPrimeMinistersContent = this.getClass().getResourceAsStream("/uk-prime-ministers.xml");
			connection.add(ukPrimeMinistersContent, "http://test.owl2.com", RDFFormat.RDFXML);
		} catch (RepositoryException e) {
			assertTrue(e.getMessage().contains(SAME_AS_DIFFERENCE_RULE));		
			throw e;
		}
	}
	
	@Test(expected=RepositoryException.class)
	public void testDifferenceRuleWithAllDifferent3d() throws RepositoryException, RDFParseException, IOException {
		Resource margaretThatcherResource = valueFactory.createURI(MARGARET_THATCHER);
		Resource ironLadyResource = valueFactory.createURI(IRON_LADY);
		connection.add(margaretThatcherResource, OWL2.SAMEAS, ironLadyResource);		
		assertTrue(connection.hasStatement(margaretThatcherResource, OWL2.SAMEAS, ironLadyResource, false));
		
		Resource primeMinistersListResource = valueFactory.createURI("http://test.owl2.com#PrimeMinistersList");

		Resource primeMinistersResource = valueFactory.createURI("http://test.owl2.com/UK#PrimeMinisters");
		connection.add(primeMinistersResource, OWL2.DISTINCTMEMBERS, primeMinistersListResource);
		assertTrue(connection.hasStatement(primeMinistersResource, OWL2.DISTINCTMEMBERS, primeMinistersListResource, false));
		
		InputStream ukPrimeMinistersContent = this.getClass().getResourceAsStream("/uk-prime-ministers.xml");
		connection.add(ukPrimeMinistersContent, "http://test.owl2.com", RDFFormat.RDFXML);
		
		Collection<String> primeMinisterNames = queryPrimeMinisterNames(primeMinistersListResource);
		assertTrue(primeMinisterNames.contains(MARGARET_THATCHER));
		assertTrue(primeMinisterNames.contains(IRON_LADY));
		
		RepositoryResult<Statement> results = connection.getStatements(primeMinistersListResource, null, null, true);
		connection.remove(results);
		
		Resource primeMinistersResourceWithAllDifferent = valueFactory.createURI("http://test.owl2.com/UK#PrimeMinistersAllDifferent");
		connection.add(primeMinistersResourceWithAllDifferent, RDF.TYPE, OWL2.ALLDIFFERENT);		
		connection.add(primeMinistersResourceWithAllDifferent, OWL2.DISTINCTMEMBERS, primeMinistersListResource);
		assertTrue(connection.hasStatement(primeMinistersResourceWithAllDifferent, RDF.TYPE, OWL2.ALLDIFFERENT, false));
		assertTrue(connection.hasStatement(primeMinistersResourceWithAllDifferent, OWL2.DISTINCTMEMBERS, primeMinistersListResource, false));
		
		try {
			ukPrimeMinistersContent = this.getClass().getResourceAsStream("/uk-prime-ministers.xml");
			connection.add(ukPrimeMinistersContent, "http://test.owl2.com", RDFFormat.RDFXML);
		} catch (RepositoryException e) {
			assertTrue(e.getMessage().contains(SAME_AS_DIFFERENCE_RULE));		
			throw e;
		}
	}
	
	@Test(expected=RepositoryException.class)
	public void testDifferenceRuleWithAllDifferent4a() throws RepositoryException, RDFParseException, IOException {
		InputStream ukPrimeMinistersContent = this.getClass().getResourceAsStream("/uk-prime-ministers.xml");
		connection.add(ukPrimeMinistersContent, "http://test.owl2.com", RDFFormat.RDFXML);
		
		Resource primeMinistersListResource = valueFactory.createURI("http://test.owl2.com#PrimeMinistersList");
		Collection<String> primeMinisterNames = queryPrimeMinisterNames(primeMinistersListResource);
		assertTrue(primeMinisterNames.contains(MARGARET_THATCHER));
		assertTrue(primeMinisterNames.contains(IRON_LADY));
		
		Resource primeMinistersResource = valueFactory.createURI("http://test.owl2.com/UK#PrimeMinisters");
		connection.add(primeMinistersResource, RDF.TYPE, OWL2.ALLDIFFERENT);		
		assertTrue(connection.hasStatement(primeMinistersResource, RDF.TYPE, OWL2.ALLDIFFERENT, false));
		
		connection.add(primeMinistersResource, OWL2.MEMBERS, primeMinistersListResource);
		assertTrue(connection.hasStatement(primeMinistersResource, OWL2.MEMBERS, primeMinistersListResource, false));
		
		Resource margaretThatcherResource = valueFactory.createURI(MARGARET_THATCHER);
		Resource ironLadyResource = valueFactory.createURI(IRON_LADY);
		try {
			connection.add(margaretThatcherResource, OWL2.SAMEAS, ironLadyResource);		
		} catch (RepositoryException e) {
			assertTrue(e.getMessage().contains(SAME_AS_DIFFERENCE_RULE));		
			throw e;
		}
	}
	
	@Test(expected=RepositoryException.class)
	public void testDifferenceRuleWithAllDifferent4b() throws RepositoryException, RDFParseException, IOException {
		InputStream ukPrimeMinistersContent = this.getClass().getResourceAsStream("/uk-prime-ministers.xml");
		connection.add(ukPrimeMinistersContent, "http://test.owl2.com", RDFFormat.RDFXML);
		
		Resource primeMinistersListResource = valueFactory.createURI("http://test.owl2.com#PrimeMinistersList");
		Collection<String> primeMinisterNames = queryPrimeMinisterNames(primeMinistersListResource);
		assertTrue(primeMinisterNames.contains(MARGARET_THATCHER));
		assertTrue(primeMinisterNames.contains(IRON_LADY));
		
		Resource primeMinistersResource = valueFactory.createURI("http://test.owl2.com/UK#PrimeMinisters");
		connection.add(primeMinistersResource, RDF.TYPE, OWL2.ALLDIFFERENT);		
		assertTrue(connection.hasStatement(primeMinistersResource, RDF.TYPE, OWL2.ALLDIFFERENT, false));
		
		connection.add(primeMinistersResource, OWL2.DISTINCTMEMBERS, primeMinistersListResource);
		assertTrue(connection.hasStatement(primeMinistersResource, OWL2.DISTINCTMEMBERS, primeMinistersListResource, false));
		
		Resource margaretThatcherResource = valueFactory.createURI(MARGARET_THATCHER);
		Resource ironLadyResource = valueFactory.createURI(IRON_LADY);
		try {
			connection.add(margaretThatcherResource, OWL2.SAMEAS, ironLadyResource);		
		} catch (RepositoryException e) {
			assertTrue(e.getMessage().contains(SAME_AS_DIFFERENCE_RULE));		
			throw e;
		}
	}
	
	@Test
	public void testCheckCleanupOnRemoval() throws RepositoryException {
		Resource margaretThatcherResource = valueFactory.createURI(MARGARET_THATCHER);
		Resource ironLadyResource = valueFactory.createURI(IRON_LADY);
				
		connection.add(margaretThatcherResource, OWL2.SAMEAS, ironLadyResource);
		assertTrue(connection.hasStatement(margaretThatcherResource, OWL2.SAMEAS, ironLadyResource, false));
		
		connection.remove(margaretThatcherResource, OWL2.SAMEAS, ironLadyResource);
		
		assertFalse(connection.hasStatement(margaretThatcherResource, null, null, true));
		assertFalse(connection.hasStatement(null, (URI) margaretThatcherResource, null, true));
		assertFalse(connection.hasStatement(null, null, margaretThatcherResource, true));
		
		assertFalse(connection.hasStatement(ironLadyResource, null, null, true));
		assertFalse(connection.hasStatement(null, (URI) ironLadyResource, null, true));
		assertFalse(connection.hasStatement(null, null, ironLadyResource, true));
	}
	
	private Collection<String> queryPrimeMinisterNames(Resource primeMinistersListResource) throws RepositoryException {
		Collection<String> primeMinisterNames = new ArrayList<String>();
		RepositoryResult<Statement> results = connection.getStatements(primeMinistersListResource, null, null, true);
		while (results.hasNext()) {
			Statement result = results.next();
			if (result.getPredicate().stringValue().startsWith(RDF_LIST_PREDICATE_PREFIX)) {
				primeMinisterNames.add(result.getObject().stringValue());
			}
		}
		return primeMinisterNames;
	}

}
