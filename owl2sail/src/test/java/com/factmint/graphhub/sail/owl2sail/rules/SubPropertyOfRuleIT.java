package com.factmint.graphhub.sail.owl2sail.rules;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Ignore;
import org.junit.Test;
import org.openrdf.model.Resource;
import org.openrdf.model.URI;
import org.openrdf.model.vocabulary.RDF;
import org.openrdf.model.vocabulary.RDFS;
import org.openrdf.repository.RepositoryException;

import com.factmint.graphhub.sail.owl2sail.Owl2TestBase;
import com.factmint.graphhub.sail.owl2sail.axioms.OWL2;

public class SubPropertyOfRuleIT extends Owl2TestBase {

	private static final String HAS_MOTHER = "http://test.owl2.com/hasMother";
	private static final String HAS_PARENT = "http://test.owl2.com/hasParent";
	private static final String HAS_GUARDIAN = "http://test.owl2.com/hasGuardian";
	private static final String HAS_RELATIVE = "http://test.owl2.com/hasRelative";
	private static final String HAS_FAMILY_MEMBER = "http://test.owl2.com/hasFamilyMember";
	private static final String IRENE_CURIE = "http://test.owl2.com/IreneCurie";
	private static final String MARIE_CURIE = "http://test.owl2.com/MarieCurie";
	private static final String CHILD = "http://test.owl2.com/Child";
	private static final String ADULT = "http://test.owl2.com/Adult";
	
	@Test
	public void testTransitivity() throws RepositoryException {
		assertTrue(connection.hasStatement(RDFS.SUBPROPERTYOF, RDF.TYPE, OWL2.TRANSITIVEPROPERTY, true));
		
		Resource hasMotherResource = valueFactory.createURI(HAS_MOTHER);
		Resource hasParentResource = valueFactory.createURI(HAS_PARENT);
		Resource hasRelativeResource = valueFactory.createURI(HAS_RELATIVE);
		
		connection.add(hasParentResource, RDFS.SUBPROPERTYOF, hasRelativeResource);
		assertTrue(connection.hasStatement(hasParentResource, RDFS.SUBPROPERTYOF, hasRelativeResource, false));

		connection.add(hasMotherResource, RDFS.SUBPROPERTYOF, hasParentResource);
		assertTrue(connection.hasStatement(hasMotherResource, RDFS.SUBPROPERTYOF, hasParentResource, false));
		assertTrue(connection.hasStatement(hasMotherResource, RDFS.SUBPROPERTYOF, hasRelativeResource, true));
		
		connection.remove(hasMotherResource, RDFS.SUBPROPERTYOF, hasParentResource);
		assertFalse(connection.hasStatement(hasMotherResource, RDFS.SUBPROPERTYOF, hasParentResource, false));
		assertFalse(connection.hasStatement(hasMotherResource, RDFS.SUBPROPERTYOF, hasRelativeResource, true));
	}
	
	@Test
	public void testSubPropertyOfPropertyAxiomRule_1() throws RepositoryException {
		URI hasMotherResource = valueFactory.createURI(HAS_MOTHER);
		URI hasParentResource = valueFactory.createURI(HAS_PARENT);
		Resource ireneCurieResource = valueFactory.createURI(IRENE_CURIE);
		Resource marieCurieResource = valueFactory.createURI(MARIE_CURIE);
		
		connection.add(ireneCurieResource, hasMotherResource, marieCurieResource);
		assertTrue(connection.hasStatement(ireneCurieResource, hasMotherResource, marieCurieResource, false));

		connection.add(hasMotherResource, RDFS.SUBPROPERTYOF, hasParentResource);
		assertTrue(connection.hasStatement(hasMotherResource, RDFS.SUBPROPERTYOF, hasParentResource, false));
		assertTrue(connection.hasStatement(ireneCurieResource, hasParentResource, marieCurieResource, true));
		
		connection.remove(hasMotherResource, RDFS.SUBPROPERTYOF, hasParentResource);
		assertFalse(connection.hasStatement(hasMotherResource, RDFS.SUBPROPERTYOF, hasParentResource, false));
		assertFalse(connection.hasStatement(ireneCurieResource, hasParentResource, marieCurieResource, true));
	}
	
	@Test
	public void testSubPropertyOfPropertyAxiomRule_2() throws RepositoryException {
		URI hasMotherResource = valueFactory.createURI(HAS_MOTHER);
		URI hasParentResource = valueFactory.createURI(HAS_PARENT);
		Resource ireneCurieResource = valueFactory.createURI(IRENE_CURIE);
		Resource marieCurieResource = valueFactory.createURI(MARIE_CURIE);
		
		connection.add(hasMotherResource, RDFS.SUBPROPERTYOF, hasParentResource);
		assertTrue(connection.hasStatement(hasMotherResource, RDFS.SUBPROPERTYOF, hasParentResource, false));
		
		connection.add(ireneCurieResource, hasMotherResource, marieCurieResource);
		assertTrue(connection.hasStatement(ireneCurieResource, hasMotherResource, marieCurieResource, false));
		assertTrue(connection.hasStatement(ireneCurieResource, hasParentResource, marieCurieResource, true));
		
		connection.remove(ireneCurieResource, hasMotherResource, marieCurieResource);
		assertFalse(connection.hasStatement(ireneCurieResource, hasMotherResource, marieCurieResource, false));
		assertFalse(connection.hasStatement(ireneCurieResource, hasParentResource, marieCurieResource, true));
	}
	
	@Test
	public void testSubPropertyOfEquivalentPropertyRule() throws RepositoryException {
		Resource hasRelativeResource = valueFactory.createURI(HAS_RELATIVE);
		Resource hasFamilyMemberResource = valueFactory.createURI(HAS_FAMILY_MEMBER);
		
		connection.add(hasRelativeResource, RDFS.SUBPROPERTYOF, hasFamilyMemberResource);
		assertTrue(connection.hasStatement(hasRelativeResource, RDFS.SUBPROPERTYOF, hasFamilyMemberResource, false));

		connection.add(hasFamilyMemberResource, RDFS.SUBPROPERTYOF, hasRelativeResource);
		assertTrue(connection.hasStatement(hasFamilyMemberResource, RDFS.SUBPROPERTYOF, hasRelativeResource, false));
		assertTrue(connection.hasStatement(hasRelativeResource, OWL2.EQUIVALENTPROPERTY, hasFamilyMemberResource, true));
		assertTrue(connection.hasStatement(hasFamilyMemberResource, OWL2.EQUIVALENTPROPERTY, hasRelativeResource, true));
		
		connection.remove(hasFamilyMemberResource, RDFS.SUBPROPERTYOF, hasRelativeResource);
		assertFalse(connection.hasStatement(hasFamilyMemberResource, RDFS.SUBPROPERTYOF, hasRelativeResource, false));
		assertFalse(connection.hasStatement(hasRelativeResource, OWL2.EQUIVALENTPROPERTY, hasFamilyMemberResource, true));
		assertFalse(connection.hasStatement(hasFamilyMemberResource, OWL2.EQUIVALENTPROPERTY, hasRelativeResource, true));
	}
	
	@Test
	public void testSubPropertyOfDomainRule_1() throws RepositoryException {
		Resource hasParentResource = valueFactory.createURI(HAS_PARENT);
		Resource childResource = valueFactory.createURI(CHILD);
		Resource hasMotherResource = valueFactory.createURI(HAS_MOTHER);
		Resource hasGuardianResource = valueFactory.createURI(HAS_GUARDIAN);
		
		connection.add(hasMotherResource, RDFS.SUBPROPERTYOF, hasParentResource);
		assertTrue(connection.hasStatement(hasMotherResource, RDFS.SUBPROPERTYOF, hasParentResource, false));
		
		connection.add(hasParentResource, RDFS.DOMAIN, childResource);
		assertTrue(connection.hasStatement(hasParentResource, RDFS.DOMAIN, childResource, false));
		assertTrue(connection.hasStatement(hasMotherResource, RDFS.DOMAIN, childResource, true));
	
		connection.add(hasMotherResource, RDFS.SUBPROPERTYOF, hasGuardianResource);
		assertTrue(connection.hasStatement(hasMotherResource, RDFS.SUBPROPERTYOF, hasGuardianResource, false));
		connection.add(hasGuardianResource, RDFS.DOMAIN, childResource);
		assertTrue(connection.hasStatement(hasGuardianResource, RDFS.DOMAIN, childResource, false));
		
		connection.remove(hasParentResource, RDFS.DOMAIN, childResource);
		assertFalse(connection.hasStatement(hasParentResource, RDFS.DOMAIN, childResource, false));
		assertTrue(connection.hasStatement(hasMotherResource, RDFS.DOMAIN, childResource, true));
		
		connection.remove(hasGuardianResource, RDFS.DOMAIN, childResource);
		assertFalse(connection.hasStatement(hasGuardianResource, RDFS.DOMAIN, childResource, false));
		assertFalse(connection.hasStatement(hasMotherResource, RDFS.DOMAIN, childResource, true));
	}
	
	@Test
	public void testSubPropertyOfDomainRule_2() throws RepositoryException {
		Resource hasParentResource = valueFactory.createURI(HAS_PARENT);
		Resource childResource = valueFactory.createURI(CHILD);
		Resource hasMotherResource = valueFactory.createURI(HAS_MOTHER);
		Resource hasGuardianResource = valueFactory.createURI(HAS_GUARDIAN);
		
		connection.add(hasParentResource, RDFS.DOMAIN, childResource);
		assertTrue(connection.hasStatement(hasParentResource, RDFS.DOMAIN, childResource, false));
		
		connection.add(hasMotherResource, RDFS.SUBPROPERTYOF, hasParentResource);
		assertTrue(connection.hasStatement(hasMotherResource, RDFS.SUBPROPERTYOF, hasParentResource, false));
		assertTrue(connection.hasStatement(hasMotherResource, RDFS.DOMAIN, childResource, true));
	
		connection.add(hasGuardianResource, RDFS.DOMAIN, childResource);
		assertTrue(connection.hasStatement(hasGuardianResource, RDFS.DOMAIN, childResource, false));
		connection.add(hasMotherResource, RDFS.SUBPROPERTYOF, hasGuardianResource);
		assertTrue(connection.hasStatement(hasMotherResource, RDFS.SUBPROPERTYOF, hasGuardianResource, false));
		
		connection.remove(hasMotherResource, RDFS.SUBPROPERTYOF, hasParentResource);
		assertFalse(connection.hasStatement(hasMotherResource, RDFS.SUBPROPERTYOF, hasParentResource, false));
		assertTrue(connection.hasStatement(hasMotherResource, RDFS.DOMAIN, childResource, true));
		
		connection.remove(hasMotherResource, RDFS.SUBPROPERTYOF, hasGuardianResource);
		assertFalse(connection.hasStatement(hasMotherResource, RDFS.SUBPROPERTYOF, hasGuardianResource, false));
		assertFalse(connection.hasStatement(hasMotherResource, RDFS.DOMAIN, childResource, true));
	}
	
	@Test
	public void testSubPropertyOfRangeRule_1() throws RepositoryException {
		Resource hasParentResource = valueFactory.createURI(HAS_PARENT);
		Resource adultResource = valueFactory.createURI(ADULT);
		Resource hasMotherResource = valueFactory.createURI(HAS_MOTHER);
		Resource hasGuardianResource = valueFactory.createURI(HAS_GUARDIAN);
		
		connection.add(hasMotherResource, RDFS.SUBPROPERTYOF, hasParentResource);
		assertTrue(connection.hasStatement(hasMotherResource, RDFS.SUBPROPERTYOF, hasParentResource, false));
		
		connection.add(hasParentResource, RDFS.RANGE, adultResource);
		assertTrue(connection.hasStatement(hasParentResource, RDFS.RANGE, adultResource, false));
		assertTrue(connection.hasStatement(hasMotherResource, RDFS.RANGE, adultResource, true));
	
		connection.add(hasMotherResource, RDFS.SUBPROPERTYOF, hasGuardianResource);
		assertTrue(connection.hasStatement(hasMotherResource, RDFS.SUBPROPERTYOF, hasGuardianResource, false));
		connection.add(hasGuardianResource, RDFS.RANGE, adultResource);
		assertTrue(connection.hasStatement(hasGuardianResource, RDFS.RANGE, adultResource, false));
		
		connection.remove(hasParentResource, RDFS.RANGE, adultResource);
		assertFalse(connection.hasStatement(hasParentResource, RDFS.RANGE, adultResource, false));
		assertTrue(connection.hasStatement(hasMotherResource, RDFS.RANGE, adultResource, true));
		
		connection.remove(hasGuardianResource, RDFS.RANGE, adultResource);
		assertFalse(connection.hasStatement(hasGuardianResource, RDFS.RANGE, adultResource, false));
		assertFalse(connection.hasStatement(hasMotherResource, RDFS.RANGE, adultResource, true));
	}
	
	@Test
	public void testSubPropertyOfRangeRule_2() throws RepositoryException {
		Resource hasParentResource = valueFactory.createURI(HAS_PARENT);
		Resource adultResource = valueFactory.createURI(ADULT);
		Resource hasMotherResource = valueFactory.createURI(HAS_MOTHER);
		Resource hasGuardianResource = valueFactory.createURI(HAS_GUARDIAN);
		
		connection.add(hasParentResource, RDFS.RANGE, adultResource);
		assertTrue(connection.hasStatement(hasParentResource, RDFS.RANGE, adultResource, false));
		
		connection.add(hasMotherResource, RDFS.SUBPROPERTYOF, hasParentResource);
		assertTrue(connection.hasStatement(hasMotherResource, RDFS.SUBPROPERTYOF, hasParentResource, false));
		assertTrue(connection.hasStatement(hasMotherResource, RDFS.RANGE, adultResource, true));
	
		connection.add(hasGuardianResource, RDFS.RANGE, adultResource);
		assertTrue(connection.hasStatement(hasGuardianResource, RDFS.RANGE, adultResource, false));
		connection.add(hasMotherResource, RDFS.SUBPROPERTYOF, hasGuardianResource);
		assertTrue(connection.hasStatement(hasMotherResource, RDFS.SUBPROPERTYOF, hasGuardianResource, false));
		
		connection.remove(hasMotherResource, RDFS.SUBPROPERTYOF, hasParentResource);
		assertFalse(connection.hasStatement(hasMotherResource, RDFS.SUBPROPERTYOF, hasParentResource, false));
		assertTrue(connection.hasStatement(hasMotherResource, RDFS.RANGE, adultResource, true));
		
		connection.remove(hasMotherResource, RDFS.SUBPROPERTYOF, hasGuardianResource);
		assertFalse(connection.hasStatement(hasMotherResource, RDFS.SUBPROPERTYOF, hasGuardianResource, false));
		assertFalse(connection.hasStatement(hasMotherResource, RDFS.RANGE, adultResource, true));
	}
	
	@Ignore("There is currently no logic in place for rdf:type rule removal, as rdf:type is not yet implemented. This test will fail.")
	@Test
	public void testCheckCleanupOnRemoval() throws RepositoryException {
		URI hasMotherResource = valueFactory.createURI(HAS_MOTHER);
		URI hasParentResource = valueFactory.createURI(HAS_PARENT);
		Resource ireneCurieResource = valueFactory.createURI(IRENE_CURIE);
		Resource marieCurieResource = valueFactory.createURI(MARIE_CURIE);
		
		connection.add(ireneCurieResource, hasMotherResource, marieCurieResource);
		assertTrue(connection.hasStatement(ireneCurieResource, hasMotherResource, marieCurieResource, false));

		connection.add(hasMotherResource, RDFS.SUBPROPERTYOF, hasParentResource);
		assertTrue(connection.hasStatement(hasMotherResource, RDFS.SUBPROPERTYOF, hasParentResource, false));
		assertTrue(connection.hasStatement(ireneCurieResource, hasParentResource, marieCurieResource, true));
		
		connection.remove(hasMotherResource, RDFS.SUBPROPERTYOF, hasParentResource);
		connection.remove(ireneCurieResource, hasMotherResource, marieCurieResource);
		
		assertFalse(connection.hasStatement(hasMotherResource, null, null, true));
		assertFalse(connection.hasStatement(null, (URI) hasMotherResource, null, true));
		assertFalse(connection.hasStatement(null, null, hasMotherResource, true));
		
		assertFalse(connection.hasStatement(hasParentResource, null, null, true));
		assertFalse(connection.hasStatement(null, (URI) hasParentResource, null, true));
		assertFalse(connection.hasStatement(null, null, hasParentResource, true));
		
		assertFalse(connection.hasStatement(ireneCurieResource, null, null, true));
		assertFalse(connection.hasStatement(null, (URI) ireneCurieResource, null, true));
		assertFalse(connection.hasStatement(null, null, ireneCurieResource, true));
		
		assertFalse(connection.hasStatement(marieCurieResource, null, null, true));
		assertFalse(connection.hasStatement(null, (URI) marieCurieResource, null, true));
		assertFalse(connection.hasStatement(null, null, marieCurieResource, true));
	}
	
}
