package com.factmint.graphhub.sail.owl2sail.rules;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Ignore;
import org.junit.Test;
import org.openrdf.model.Resource;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.vocabulary.RDF;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.RepositoryResult;

import com.factmint.graphhub.sail.owl2sail.Owl2TestBase;
import com.factmint.graphhub.sail.owl2sail.axioms.OWL2;

public class ReflexivePropertyRuleIT extends Owl2TestBase {

	@Test
	public void testReflexivePropertyRule_1() throws RepositoryException {
		Resource fiveTimesThreeResource = valueFactory.createURI("http://test.owl2.com/FiveTimesThree");
		Resource fifteenResource = valueFactory.createURI("http://test.owl2.com/Fifteen");
		URI isEqualToResource = valueFactory.createURI("http://test.owl2.com/isEqualTo");
		
		connection.add(isEqualToResource, RDF.TYPE, OWL2.REFLEXIVEPROPERTY);
		assertTrue(connection.hasStatement(isEqualToResource, RDF.TYPE, OWL2.REFLEXIVEPROPERTY, false));

		connection.add(fiveTimesThreeResource, isEqualToResource, fifteenResource);
		assertTrue(connection.hasStatement(fiveTimesThreeResource, isEqualToResource, fifteenResource, false));
		assertTrue(connection.hasStatement(fiveTimesThreeResource, isEqualToResource, fiveTimesThreeResource, true));
		assertTrue(connection.hasStatement(fifteenResource, isEqualToResource, fifteenResource, true));
		
		connection.remove(fiveTimesThreeResource, isEqualToResource, fifteenResource);
		assertFalse(connection.hasStatement(fiveTimesThreeResource, isEqualToResource, fifteenResource, false));
		assertFalse(connection.hasStatement(fiveTimesThreeResource, isEqualToResource, fiveTimesThreeResource, true));
		assertFalse(connection.hasStatement(fifteenResource, isEqualToResource, fifteenResource, true));		
	}
	
	@Test
	public void testReflexivePropertyRule_2() throws RepositoryException {
		Resource fiveTimesThreeResource = valueFactory.createURI("http://test.owl2.com/FiveTimesThree");
		Resource fifteenResource = valueFactory.createURI("http://test.owl2.com/Fifteen");
		URI isEqualToResource = valueFactory.createURI("http://test.owl2.com/isEqualTo");
		
		connection.add(fiveTimesThreeResource, isEqualToResource, fifteenResource);
		assertTrue(connection.hasStatement(fiveTimesThreeResource, isEqualToResource, fifteenResource, false));

		connection.add(isEqualToResource, RDF.TYPE, OWL2.REFLEXIVEPROPERTY);
		assertTrue(connection.hasStatement(isEqualToResource, RDF.TYPE, OWL2.REFLEXIVEPROPERTY, false));
		assertTrue(connection.hasStatement(fiveTimesThreeResource, isEqualToResource, fiveTimesThreeResource, true));
		assertTrue(connection.hasStatement(fifteenResource, isEqualToResource, fifteenResource, true));
		
		connection.remove(isEqualToResource, RDF.TYPE, OWL2.REFLEXIVEPROPERTY);
		assertFalse(connection.hasStatement(isEqualToResource, RDF.TYPE, OWL2.REFLEXIVEPROPERTY, false));
		assertFalse(connection.hasStatement(fiveTimesThreeResource, isEqualToResource, fiveTimesThreeResource, true));
		assertFalse(connection.hasStatement(fifteenResource, isEqualToResource, fifteenResource, true));		
	}
	
	@Ignore("There is currently no logic in place for rdf:type rule removal, as rdf:type is not yet implemented. This test will fail.")
	@Test
	public void testCheckCleanupOnRemoval() throws RepositoryException {
		Resource fiveTimesThreeResource = valueFactory.createURI("http://test.owl2.com/FiveTimesThree");
		Resource fifteenResource = valueFactory.createURI("http://test.owl2.com/Fifteen");
		URI isEqualToResource = valueFactory.createURI("http://test.owl2.com/isEqualTo");
		
		connection.add(isEqualToResource, RDF.TYPE, OWL2.REFLEXIVEPROPERTY);
		assertTrue(connection.hasStatement(isEqualToResource, RDF.TYPE, OWL2.REFLEXIVEPROPERTY, false));

		connection.add(fiveTimesThreeResource, isEqualToResource, fifteenResource);
		assertTrue(connection.hasStatement(fiveTimesThreeResource, isEqualToResource, fifteenResource, false));
		assertTrue(connection.hasStatement(fiveTimesThreeResource, isEqualToResource, fiveTimesThreeResource, true));
		assertTrue(connection.hasStatement(fifteenResource, isEqualToResource, fifteenResource, true));
		
		connection.remove(isEqualToResource, RDF.TYPE, OWL2.REFLEXIVEPROPERTY);
		connection.remove(fiveTimesThreeResource, isEqualToResource, fifteenResource);

		assertFalse(connection.hasStatement(fiveTimesThreeResource, null, null, true));
		assertFalse(connection.hasStatement(null, (URI) fiveTimesThreeResource, null, true));
		assertFalse(connection.hasStatement(null, null, fiveTimesThreeResource, true));
		
		assertFalse(connection.hasStatement(fifteenResource, null, null, true));
		assertFalse(connection.hasStatement(null, (URI) fifteenResource, null, true));
		assertFalse(connection.hasStatement(null, null, fifteenResource, true));
		
		assertFalse(connection.hasStatement(isEqualToResource, null, null, true));
		assertFalse(connection.hasStatement(null, isEqualToResource, null, true));
		assertFalse(connection.hasStatement(null, null, isEqualToResource, true));
	}
	
}
