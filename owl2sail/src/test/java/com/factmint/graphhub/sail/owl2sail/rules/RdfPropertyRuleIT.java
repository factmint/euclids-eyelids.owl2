package com.factmint.graphhub.sail.owl2sail.rules;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Ignore;
import org.junit.Test;
import org.openrdf.model.Resource;
import org.openrdf.model.URI;
import org.openrdf.model.vocabulary.RDF;
import org.openrdf.repository.RepositoryException;

import com.factmint.graphhub.sail.owl2sail.Owl2TestBase;

public class RdfPropertyRuleIT extends Owl2TestBase {

	@Test
	public void testRdfPropertyRule() throws RepositoryException {
		Resource fiveTimesThreeResource = valueFactory.createURI("http://test.owl2.com/FiveTimesThree");
		Resource fifteenResource = valueFactory.createURI("http://test.owl2.com/Fifteen");
		URI isEqualToResource = valueFactory.createURI("http://test.owl2.com/isEqualTo");
		
		Resource twoTimesFiveResource = valueFactory.createURI("http://test.owl2.com/TwoTimesFive");
		Resource tenResource = valueFactory.createURI("http://test.owl2.com/Ten");
		
		connection.add(fiveTimesThreeResource, isEqualToResource, fifteenResource);
		assertTrue(connection.hasStatement(fiveTimesThreeResource, isEqualToResource, fifteenResource, false));
		assertTrue(connection.hasStatement(isEqualToResource, RDF.TYPE, RDF.PROPERTY, true));
		
		connection.add(twoTimesFiveResource, isEqualToResource, tenResource);
		assertTrue(connection.hasStatement(twoTimesFiveResource, isEqualToResource, tenResource, false));
		
		connection.remove(fiveTimesThreeResource, isEqualToResource, fifteenResource);
		assertFalse(connection.hasStatement(fiveTimesThreeResource, isEqualToResource, fifteenResource, false));
		assertTrue(connection.hasStatement(isEqualToResource, RDF.TYPE, RDF.PROPERTY, true));	
		
		connection.remove(twoTimesFiveResource, isEqualToResource, tenResource);
		assertFalse(connection.hasStatement(twoTimesFiveResource, isEqualToResource, tenResource, false));
		assertFalse(connection.hasStatement(isEqualToResource, RDF.TYPE, RDF.PROPERTY, true));
	}
	
	@Ignore("There is currently no logic in place for rdf:type rule removal, as rdf:type is not yet implemented. This test will fail.")
	@Test
	public void testCheckCleanupOnRemoval() throws RepositoryException {
		Resource fiveTimesThreeResource = valueFactory.createURI("http://test.owl2.com/FiveTimesThree");
		Resource fifteenResource = valueFactory.createURI("http://test.owl2.com/Fifteen");
		URI isEqualToResource = valueFactory.createURI("http://test.owl2.com/isEqualTo");
		
		connection.add(fiveTimesThreeResource, isEqualToResource, fifteenResource);
		assertTrue(connection.hasStatement(fiveTimesThreeResource, isEqualToResource, fifteenResource, false));
		assertTrue(connection.hasStatement(isEqualToResource, RDF.TYPE, RDF.PROPERTY, true));
		
		connection.remove(fiveTimesThreeResource, isEqualToResource, fifteenResource);
		
		assertFalse(connection.hasStatement(fiveTimesThreeResource, null, null, true));
		assertFalse(connection.hasStatement(null, (URI) fiveTimesThreeResource, null, true));
		assertFalse(connection.hasStatement(null, null, fiveTimesThreeResource, true));
		
		assertFalse(connection.hasStatement(fifteenResource, null, null, true));
		assertFalse(connection.hasStatement(null, (URI) fifteenResource, null, true));
		assertFalse(connection.hasStatement(null, null, fifteenResource, true));
		
		assertFalse(connection.hasStatement(isEqualToResource, null, null, true));
		assertFalse(connection.hasStatement(null, isEqualToResource, null, true));
		assertFalse(connection.hasStatement(null, null, isEqualToResource, true));
	}
	
}
