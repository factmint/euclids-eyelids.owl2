package com.factmint.graphhub.sail.owl2sail.rules;

import static com.factmint.graphhub.sail.owl2sail.sesame.utility.Constants.DOMAIN_RULE;
import static com.factmint.graphhub.sail.owl2sail.sesame.utility.Constants.RULE_DEPENDENCIES;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import info.aduna.iteration.CloseableIteratorIteration;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.openrdf.model.Resource;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.ValueFactory;
import org.openrdf.model.impl.ValueFactoryImpl;
import org.openrdf.model.vocabulary.RDF;
import org.openrdf.model.vocabulary.RDFS;
import org.openrdf.sail.SailException;
import org.openrdf.sail.inferencer.InferencerConnection;

import com.factmint.graphhub.sail.owl2sail.rules.DomainRule;
import com.google.common.collect.Sets;

public class DomainRuleTest {

	private static final String TIGER = "http://test.owl2.com/Tiger";
	private static final String DEER = "http://test.owl2.com/Deer";
	private static final String PREDATOR = "http://test.owl2.com/Predator";
	private static final String HUNTS = "http://test.owl2.com/hunts";
	
	private InferencerConnection mockConnection = mock(InferencerConnection.class);
	private ValueFactory valueFactory = ValueFactoryImpl.getInstance(); 
	
	private DomainRule ruleUnderTest;
	
	@Before
	public void setup() {
		ruleUnderTest = new DomainRule(mockConnection, RULE_DEPENDENCIES);
	}
	
	@Test
	public void testDependenciesOfDomainRule() throws SailException {
		Resource tigerResource = valueFactory.createURI(TIGER);
		Resource deerResource = valueFactory.createURI(DEER);
		Resource predatorResource = valueFactory.createURI(PREDATOR);
		URI huntsResource = valueFactory.createURI(HUNTS);
		
		Set<Statement> addedStatements = Sets.newHashSet(valueFactory.createStatement(tigerResource, huntsResource, deerResource));
		Set<String> rules = Sets.newHashSet(DOMAIN_RULE);
		
		Set<Statement> statementList = new HashSet<Statement>();
		statementList.add(valueFactory.createStatement(huntsResource, RDFS.DOMAIN, predatorResource));
		Iterator<Statement> statementListIterator = statementList.iterator();
		when(mockConnection.getStatements(null, RDFS.DOMAIN, null, true)).thenReturn(new CloseableIteratorIteration(statementListIterator));
		
		when(mockConnection.addInferredStatement(tigerResource, RDF.TYPE, predatorResource, null)).thenReturn(true);
		
		Collection<String> rulesToApplyNextIteration = ruleUnderTest.apply(addedStatements, null, rules);
		assertEquals(RULE_DEPENDENCIES.get(DOMAIN_RULE), rulesToApplyNextIteration);
	}
	
}
