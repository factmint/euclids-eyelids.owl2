package com.factmint.graphhub.sail.owl2sail.rules;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Ignore;
import org.junit.Test;
import org.openrdf.model.Resource;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.repository.RepositoryException;

import com.factmint.graphhub.sail.owl2sail.Owl2TestBase;
import com.factmint.graphhub.sail.owl2sail.axioms.OWL2;

public class InverseOfRuleIT extends Owl2TestBase {
	
	private static final String BILL = "http://test.owl2.com/bill";
	private static final String MORWENNA = "http://test.owl2.com/morwenna";
	private static final String IS_WIFE = "http://test.owl2.com/isWifeOf";
	private static final String IS_HUSBAND = "http://test.owl2.com/isHusbandOf";
	private static final String IS_FRIEND = "http://test.owl2.com/isFriendOf";
	
	@Test
	public void testAddInverseProperty() throws RepositoryException {
		Resource bill = valueFactory.createURI(BILL);
		Resource morwenna = valueFactory.createURI(MORWENNA);
		URI isWifeOf = valueFactory.createURI(IS_WIFE);
		URI isHusbandOf = valueFactory.createURI(IS_HUSBAND);
		Statement existingRelationship = valueFactory.createStatement(morwenna, isWifeOf, bill);
		Statement inverseProperty = valueFactory.createStatement(isWifeOf, OWL2.INVERSEOF, isHusbandOf);
		Statement inverseRelationship = valueFactory.createStatement(bill, isHusbandOf, morwenna);
		connection.add(existingRelationship);
		connection.add(inverseProperty);
		assertTrue(connection.hasStatement(inverseRelationship, true));
		
		connection.remove(inverseProperty);
		assertFalse(connection.hasStatement(inverseRelationship, true));
		assertTrue(connection.hasStatement(existingRelationship, true));
		
	}
	
	@Test
	public void testAddInversePropertyAdditionalRelattionship() throws RepositoryException {
		Resource bill = valueFactory.createURI(BILL);
		Resource morwenna = valueFactory.createURI(MORWENNA);
		URI isWifeOf = valueFactory.createURI(IS_WIFE);
		URI isFriendOf = valueFactory.createURI(IS_FRIEND);
		URI isHusbandOf = valueFactory.createURI(IS_HUSBAND);
		Statement existingRelationship = valueFactory.createStatement(morwenna, isWifeOf, bill);
		Statement additionalRelationship = valueFactory.createStatement(morwenna, isFriendOf, bill);
		Statement inverseProperty = valueFactory.createStatement(isWifeOf, OWL2.INVERSEOF, isHusbandOf);
		Statement inverseRelationship = valueFactory.createStatement(bill, isHusbandOf, morwenna);
		connection.add(existingRelationship);
		connection.add(additionalRelationship);
		connection.add(inverseProperty);
		assertTrue(connection.hasStatement(inverseRelationship, true));
		
		connection.remove(inverseProperty);
		assertFalse(connection.hasStatement(inverseRelationship, true));
		assertTrue(connection.hasStatement(existingRelationship, true));
		assertTrue(connection.hasStatement(additionalRelationship, true));
		
	}
	
	@Test
	public void testAddRelationship() throws RepositoryException {
		Resource bill = valueFactory.createURI(BILL);
		Resource morwenna = valueFactory.createURI(MORWENNA);
		URI isWifeOf = valueFactory.createURI(IS_WIFE);
		URI isHusbandOf = valueFactory.createURI(IS_HUSBAND);
		Statement existingInverseProperty = valueFactory.createStatement(isWifeOf, OWL2.INVERSEOF, isHusbandOf);
		Statement relationshipToAdd = valueFactory.createStatement(morwenna, isWifeOf, bill);
		Statement inverseRelationship = valueFactory.createStatement(bill, isHusbandOf, morwenna);
		connection.add(existingInverseProperty);
		connection.add(relationshipToAdd);
		assertTrue(connection.hasStatement(inverseRelationship, true));
		
		connection.remove(relationshipToAdd);
		assertFalse(connection.hasStatement(inverseRelationship, true));
		assertTrue(connection.hasStatement(existingInverseProperty, true));
	}
	
	@Ignore("There is currently no logic in place for rdf:type rule removal, as rdf:type is not yet implemented. This test will fail.")
	@Test
	@Override
	public void testCheckCleanupOnRemoval() throws RepositoryException {
		Resource bill = valueFactory.createURI(BILL);
		Resource morwenna = valueFactory.createURI(MORWENNA);
		URI isWifeOf = valueFactory.createURI(IS_WIFE);
		URI isHusbandOf = valueFactory.createURI(IS_HUSBAND);
		
		connection.add(bill, isHusbandOf,  morwenna);
		connection.add(isWifeOf, OWL2.INVERSEOF, isHusbandOf);
		assertTrue(connection.hasStatement(isWifeOf, OWL2.INVERSEOF, isHusbandOf, true));
		assertTrue(connection.hasStatement(bill, isHusbandOf, morwenna, true));
		assertTrue(connection.hasStatement(morwenna, isWifeOf, bill, true));
		
		connection.remove(bill, isHusbandOf,  morwenna);
		connection.remove(isWifeOf, OWL2.INVERSEOF, isHusbandOf);
		
		assertFalse(connection.hasStatement(isWifeOf, OWL2.INVERSEOF, isHusbandOf, true));
		assertFalse(connection.hasStatement(isHusbandOf, OWL2.INVERSEOF, isWifeOf, true));
		assertFalse(connection.hasStatement(bill, isHusbandOf, morwenna, true));
		assertFalse(connection.hasStatement(morwenna, isWifeOf, bill, true));
		
		assertFalse(connection.hasStatement(morwenna, null, null, true));
		assertFalse(connection.hasStatement(bill, null, null, true));
		assertFalse(connection.hasStatement(isWifeOf, null, null, true));
		assertFalse(connection.hasStatement(isHusbandOf, null, null, true));
	}

}
