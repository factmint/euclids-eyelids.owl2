package com.factmint.graphhub.sail.owl2sail.rules;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.io.InputStream;

import org.junit.Ignore;
import org.junit.Test;
import org.openrdf.model.Resource;
import org.openrdf.model.URI;
import org.openrdf.model.vocabulary.RDF;
import org.openrdf.repository.RepositoryException;
import org.openrdf.rio.RDFFormat;
import org.openrdf.rio.RDFParseException;

import com.factmint.graphhub.sail.owl2sail.Owl2TestBase;
import com.factmint.graphhub.sail.owl2sail.axioms.OWL2;

public class TransitivePropertyRuleIT extends Owl2TestBase {

	private static final String UK = "http://test.owl2.com/UK";
	private static final String LONDON = "http://test.owl2.com/London";
	private static final String ABBEY_ROAD = "http://test.owl2.com/AbbeyRoad";
	private static final String IS_IN = "http://test.owl2.com/isIn";

	@Test
	public void testTransitivePropertyRule1() throws RepositoryException {
		Resource abbeyRoadResource = valueFactory.createURI(ABBEY_ROAD);
		URI isInResource = valueFactory.createURI(IS_IN);
		Resource londonResource = valueFactory.createURI(LONDON);
		Resource ukResource = valueFactory.createURI(UK);
		
		connection.add(abbeyRoadResource, isInResource, londonResource);
		assertTrue(connection.hasStatement(abbeyRoadResource, isInResource, londonResource, false));
		
		connection.add(londonResource, isInResource, ukResource);
		assertTrue(connection.hasStatement(londonResource, isInResource, ukResource, false));
		
		connection.add(isInResource, RDF.TYPE, OWL2.TRANSITIVEPROPERTY);
		assertTrue(connection.hasStatement(isInResource, RDF.TYPE, OWL2.TRANSITIVEPROPERTY, false));
		assertTrue(connection.hasStatement(abbeyRoadResource, isInResource, ukResource, true));
		
		connection.remove(isInResource, RDF.TYPE, OWL2.TRANSITIVEPROPERTY);
		assertFalse(connection.hasStatement(isInResource, RDF.TYPE, OWL2.TRANSITIVEPROPERTY, false));
		assertFalse(connection.hasStatement(abbeyRoadResource, isInResource, ukResource, true));
	}
	
	@Test
	public void testTransitivePropertyRule2() throws RepositoryException {
		Resource abbeyRoadResource = valueFactory.createURI(ABBEY_ROAD);
		URI isInResource = valueFactory.createURI(IS_IN);
		Resource londonResource = valueFactory.createURI(LONDON);
		Resource ukResource = valueFactory.createURI(UK);
		
		connection.add(isInResource, RDF.TYPE, OWL2.TRANSITIVEPROPERTY);
		assertTrue(connection.hasStatement(isInResource, RDF.TYPE, OWL2.TRANSITIVEPROPERTY, false));

		connection.add(londonResource, isInResource, ukResource);
		assertTrue(connection.hasStatement(londonResource, isInResource, ukResource, false));
		
		connection.add(abbeyRoadResource, isInResource, londonResource);
		assertTrue(connection.hasStatement(abbeyRoadResource, isInResource, londonResource, false));
		assertTrue(connection.hasStatement(abbeyRoadResource, isInResource, ukResource, true));
		
		connection.remove(abbeyRoadResource, isInResource, londonResource);
		assertFalse(connection.hasStatement(abbeyRoadResource, isInResource, londonResource, false));
		assertFalse(connection.hasStatement(abbeyRoadResource, isInResource, ukResource, true));
	}
	
	@Test
	public void testTransitivePropertyRule3() throws RepositoryException {
		Resource abbeyRoadResource = valueFactory.createURI(ABBEY_ROAD);
		URI isInResource = valueFactory.createURI(IS_IN);
		Resource londonResource = valueFactory.createURI(LONDON);
		Resource ukResource = valueFactory.createURI(UK);
		
		connection.add(isInResource, RDF.TYPE, OWL2.TRANSITIVEPROPERTY);
		assertTrue(connection.hasStatement(isInResource, RDF.TYPE, OWL2.TRANSITIVEPROPERTY, false));
		
		connection.add(abbeyRoadResource, isInResource, londonResource);
		assertTrue(connection.hasStatement(abbeyRoadResource, isInResource, londonResource, false));
		
		connection.add(londonResource, isInResource, ukResource);
		assertTrue(connection.hasStatement(londonResource, isInResource, ukResource, false));
		assertTrue(connection.hasStatement(abbeyRoadResource, isInResource, ukResource, true));
		
		connection.remove(londonResource, isInResource, ukResource);
		assertFalse(connection.hasStatement(londonResource, isInResource, ukResource, false));
		assertFalse(connection.hasStatement(abbeyRoadResource, isInResource, ukResource, true));
	}
	
	@Test
	public void testTransitivePropertyFromFile() throws RepositoryException, RDFParseException, IOException {
		InputStream input = this.getClass().getResourceAsStream("/is-in-transitivity.ttl");
		connection.add(input, "http://test.owl2.com", RDFFormat.TURTLE);
		
		Resource abbeyRoadResource = valueFactory.createURI(ABBEY_ROAD);
		URI isInResource = valueFactory.createURI(IS_IN);
		Resource londonResource = valueFactory.createURI(LONDON);
		Resource ukResource = valueFactory.createURI(UK);
		
		assertTrue(connection.hasStatement(abbeyRoadResource, isInResource, londonResource, false));
		assertTrue(connection.hasStatement(londonResource, isInResource, ukResource, false));
		assertTrue(connection.hasStatement(isInResource, RDF.TYPE, OWL2.TRANSITIVEPROPERTY, false));
		assertTrue(connection.hasStatement(abbeyRoadResource, isInResource, ukResource, true));
		
		connection.remove(isInResource, RDF.TYPE, OWL2.TRANSITIVEPROPERTY);
		assertFalse(connection.hasStatement(isInResource, RDF.TYPE, OWL2.TRANSITIVEPROPERTY, false));
		assertFalse(connection.hasStatement(abbeyRoadResource, isInResource, ukResource, true));
	}
	
	@Ignore("There is currently no logic in place for rdf:type rule removal, as rdf:type is not yet implemented. This test will fail.")
	@Test
	public void testCheckCleanupOnRemoval() throws RepositoryException {
		Resource abbeyRoadResource = valueFactory.createURI(ABBEY_ROAD);
		URI isInResource = valueFactory.createURI(IS_IN);
		Resource londonResource = valueFactory.createURI(LONDON);
		Resource ukResource = valueFactory.createURI(UK);
		
		connection.add(abbeyRoadResource, isInResource, londonResource);
		assertTrue(connection.hasStatement(abbeyRoadResource, isInResource, londonResource, false));
		
		connection.add(londonResource, isInResource, ukResource);
		assertTrue(connection.hasStatement(londonResource, isInResource, ukResource, false));
		
		connection.add(isInResource, RDF.TYPE, OWL2.TRANSITIVEPROPERTY);
		assertTrue(connection.hasStatement(isInResource, RDF.TYPE, OWL2.TRANSITIVEPROPERTY, false));
		assertTrue(connection.hasStatement(abbeyRoadResource, isInResource, ukResource, true));
		
		connection.remove(abbeyRoadResource, isInResource, londonResource);
		connection.remove(londonResource, isInResource, ukResource);
		connection.remove(isInResource, RDF.TYPE, OWL2.TRANSITIVEPROPERTY);
		
		assertFalse(connection.hasStatement(abbeyRoadResource, null, null, true));
		assertFalse(connection.hasStatement(null, (URI) abbeyRoadResource, null, true));
		assertFalse(connection.hasStatement(null, null, abbeyRoadResource, true));
		
		assertFalse(connection.hasStatement(isInResource, null, null, true));
		assertFalse(connection.hasStatement(null, isInResource, null, true));
		assertFalse(connection.hasStatement(null, null, isInResource, true));
		
		assertFalse(connection.hasStatement(londonResource, null, null, true));
		assertFalse(connection.hasStatement(null, (URI) londonResource, null, true));
		assertFalse(connection.hasStatement(null, null, londonResource, true));
		
		assertFalse(connection.hasStatement(ukResource, null, null, true));
		assertFalse(connection.hasStatement(null, (URI) ukResource, null, true));
		assertFalse(connection.hasStatement(null, null, ukResource, true));
	}
	
}
