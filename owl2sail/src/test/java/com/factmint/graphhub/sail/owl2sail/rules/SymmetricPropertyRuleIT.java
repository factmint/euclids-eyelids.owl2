package com.factmint.graphhub.sail.owl2sail.rules;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Ignore;
import org.junit.Test;
import org.openrdf.model.Resource;
import org.openrdf.model.URI;
import org.openrdf.model.vocabulary.RDF;
import org.openrdf.repository.RepositoryException;

import com.factmint.graphhub.sail.owl2sail.Owl2TestBase;
import com.factmint.graphhub.sail.owl2sail.axioms.OWL2;

public class SymmetricPropertyRuleIT extends Owl2TestBase {

	@Test
	public void testSymmetricPropertyRule1() throws RepositoryException {
		Resource courgetteResource = valueFactory.createURI("http://test.owl2.com/Courgette");
		Resource zucchiniResource = valueFactory.createURI("http://test.owl2.com/Zucchini");
		URI isSynonymousWith = valueFactory.createURI("http://test.owl2.com/isSynonymousWith");
		
		connection.add(isSynonymousWith, RDF.TYPE, OWL2.SYMMETRICPROPERTY);
		assertTrue(connection.hasStatement(isSynonymousWith, RDF.TYPE, OWL2.SYMMETRICPROPERTY, false));

		connection.add(courgetteResource, isSynonymousWith, zucchiniResource);
		assertTrue(connection.hasStatement(courgetteResource, isSynonymousWith, zucchiniResource, false));
		assertTrue(connection.hasStatement(zucchiniResource, isSynonymousWith, courgetteResource, true));
		
		connection.remove(courgetteResource, isSynonymousWith, zucchiniResource);
		assertFalse(connection.hasStatement(courgetteResource, isSynonymousWith, zucchiniResource, false));
		assertFalse(connection.hasStatement(zucchiniResource, isSynonymousWith, courgetteResource, true));		
	}
	
	@Test
	public void testSymmetricPropertyRule2() throws RepositoryException {
		Resource courgetteResource = valueFactory.createURI("http://test.owl2.com/Courgette");
		Resource zucchiniResource = valueFactory.createURI("http://test.owl2.com/Zucchini");
		URI isSynonymousWith = valueFactory.createURI("http://test.owl2.com/isSynonymousWith");
		
		connection.add(courgetteResource, isSynonymousWith, zucchiniResource);
		assertTrue(connection.hasStatement(courgetteResource, isSynonymousWith, zucchiniResource, false));

		connection.add(isSynonymousWith, RDF.TYPE, OWL2.SYMMETRICPROPERTY);
		assertTrue(connection.hasStatement(isSynonymousWith, RDF.TYPE, OWL2.SYMMETRICPROPERTY, false));
		assertTrue(connection.hasStatement(zucchiniResource, isSynonymousWith, courgetteResource, true));
		
		connection.remove(isSynonymousWith, RDF.TYPE, OWL2.SYMMETRICPROPERTY);
		assertFalse(connection.hasStatement(isSynonymousWith, RDF.TYPE, OWL2.SYMMETRICPROPERTY, false));
		assertFalse(connection.hasStatement(zucchiniResource, isSynonymousWith, courgetteResource, true));		
	}
	
	@Ignore("There is currently no logic in place for rdf:type rule removal, as rdf:type is not yet implemented. This test will fail.")
	@Test
	public void testCheckCleanupOnRemoval() throws RepositoryException {
		Resource courgetteResource = valueFactory.createURI("http://test.owl2.com/Courgette");
		Resource zucchiniResource = valueFactory.createURI("http://test.owl2.com/Zucchini");
		URI isSynonymousWith = valueFactory.createURI("http://test.owl2.com/isSynonymousWith");
		
		connection.add(isSynonymousWith, RDF.TYPE, OWL2.SYMMETRICPROPERTY);
		assertTrue(connection.hasStatement(isSynonymousWith, RDF.TYPE, OWL2.SYMMETRICPROPERTY, false));

		connection.add(courgetteResource, isSynonymousWith, zucchiniResource);
		assertTrue(connection.hasStatement(courgetteResource, isSynonymousWith, zucchiniResource, false));
		assertTrue(connection.hasStatement(zucchiniResource, isSynonymousWith, courgetteResource, true));
		
		connection.remove(isSynonymousWith, RDF.TYPE, OWL2.SYMMETRICPROPERTY);
		connection.remove(courgetteResource, isSynonymousWith, zucchiniResource);
		
		assertFalse(connection.hasStatement(courgetteResource, null, null, true));
		assertFalse(connection.hasStatement(null, (URI) courgetteResource, null, true));
		assertFalse(connection.hasStatement(null, null, courgetteResource, true));
		
		assertFalse(connection.hasStatement(zucchiniResource, null, null, true));
		assertFalse(connection.hasStatement(null, (URI) zucchiniResource, null, true));
		assertFalse(connection.hasStatement(null, null, zucchiniResource, true));
		
		assertFalse(connection.hasStatement(isSynonymousWith, null, null, true));
		assertFalse(connection.hasStatement(null, isSynonymousWith, null, true));
		assertFalse(connection.hasStatement(null, null, isSynonymousWith, true));
	}
	
}
