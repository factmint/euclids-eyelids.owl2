package com.factmint.graphhub.sail.owl2sail;

import java.io.IOException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.openrdf.model.ValueFactory;
import org.openrdf.model.impl.ValueFactoryImpl;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.sail.SailRepository;
import org.openrdf.repository.sail.SailRepositoryConnection;
import org.openrdf.sail.memory.MemoryStore;

import com.factmint.graphhub.sail.owl2sail.configuration.Owl2RulesConfiguration;
import com.factmint.graphhub.sail.owl2sail.sesame.Owl2Sail;

public abstract class Owl2TestBase {

	public static SailRepository testRepository;
	public static ValueFactory valueFactory = ValueFactoryImpl.getInstance();
	public static SailRepositoryConnection connection;
	
	@BeforeClass
	public static void setup () throws RepositoryException {
		Owl2Sail owl2Sail = new Owl2Sail(new MemoryStore(), true);
		
		Owl2RulesConfiguration rulesConfiguration = new Owl2RulesConfiguration();
		rulesConfiguration.getReflexivePropertyRuleConfiguration().setApplyReflexivePropertyRule(true);
		rulesConfiguration.getDomainRuleConfiguration().setApplyDomainRule(true);
		rulesConfiguration.getRangeRuleConfiguration().setApplyRangeRule(true);
		
		owl2Sail.setRulesConfiguration(rulesConfiguration);
		
		testRepository = new SailRepository(owl2Sail);
		testRepository.initialize();
	}
	
	@Before
	public void getConnection() throws RepositoryException {
		connection = testRepository.getConnection();
		connection.clear();
	}
	
	@After
	public void closeConnection() throws RepositoryException, InterruptedException {
		if (connection.isOpen()) {
			connection.rollback();
			connection.close();
		}
	}
	
	@AfterClass
	public static void cleanup() throws RepositoryException, IOException {
		testRepository.shutDown();
		testRepository = null;
	}
	
	public abstract void testCheckCleanupOnRemoval() throws RepositoryException;
	
}
