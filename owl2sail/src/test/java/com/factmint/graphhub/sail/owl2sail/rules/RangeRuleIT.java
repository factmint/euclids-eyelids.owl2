package com.factmint.graphhub.sail.owl2sail.rules;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import info.aduna.iteration.Iterations;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.Ignore;
import org.junit.Test;
import org.openrdf.model.Resource;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.vocabulary.RDF;
import org.openrdf.model.vocabulary.RDFS;
import org.openrdf.repository.RepositoryException;

import com.factmint.graphhub.sail.owl2sail.Owl2TestBase;

public class RangeRuleIT extends Owl2TestBase {

	private static final String TIGER = "http://test.owl2.com/Tiger";
	private static final String DEER = "http://test.owl2.com/Deer";
	private static final String LION = "http://test.owl2.com/Lion";
	private static final String ANIMAL = "http://test.owl2.com/Animal";
	private static final String PREY = "http://test.owl2.com/Prey";
	private static final String PREDATOR = "http://test.owl2.com/Predator";
	private static final String CARNIVORE = "http://test.owl2.com/Carnivore";
	private static final String HUNTS = "http://test.owl2.com/hunts";
	private static final String HUNTED_BY = "http://test.owl2.com/huntedBy";
	private static final String TRACKS_PREY = "http://test.owl2.com/tracksPrey";
	private static final String INDEPENDENT_RESOURCE = "http://test.owl2.com/IndependentResource";
	
	@Test
	public void testRangeRule_1() throws RepositoryException {
		Resource tigerResource = valueFactory.createURI(TIGER);
		Resource deerResource = valueFactory.createURI(DEER);
		Resource lionResource = valueFactory.createURI(LION);
		Resource huntsResource = valueFactory.createURI(HUNTS);
		Resource tracksPreyResource = valueFactory.createURI(TRACKS_PREY);
		Resource preyResource = valueFactory.createURI(PREY);
		
		connection.add(tigerResource, (URI) huntsResource, deerResource);
		assertTrue(connection.hasStatement(tigerResource, (URI) huntsResource, deerResource, false));

		connection.add(huntsResource, RDFS.RANGE, preyResource);
		assertTrue(connection.hasStatement(huntsResource, RDFS.RANGE, preyResource, false));
		assertTrue(connection.hasStatement(deerResource, RDF.TYPE, preyResource, true));
		
		connection.add(lionResource, (URI) tracksPreyResource, deerResource);
		assertTrue(connection.hasStatement(lionResource, (URI) tracksPreyResource, deerResource, false));
		connection.add(tracksPreyResource, RDFS.RANGE, preyResource);
		assertTrue(connection.hasStatement(tracksPreyResource, RDFS.RANGE, preyResource, false));		
		
		connection.remove(huntsResource, RDFS.RANGE, preyResource);
		assertFalse(connection.hasStatement(huntsResource, RDFS.RANGE, preyResource, false));
		assertTrue(connection.hasStatement(deerResource, RDF.TYPE, preyResource, true));
		
		connection.remove(tracksPreyResource, RDFS.RANGE, preyResource);
		assertFalse(connection.hasStatement(tracksPreyResource, RDFS.RANGE, preyResource, false));
		assertFalse(connection.hasStatement(deerResource, RDF.TYPE, preyResource, true));
	}
	
	@Test
	public void testRangeRule_2() throws RepositoryException {
		Resource tigerResource = valueFactory.createURI(TIGER);
		Resource deerResource = valueFactory.createURI(DEER);
		Resource lionResource = valueFactory.createURI(LION);
		Resource huntsResource = valueFactory.createURI(HUNTS);
		Resource tracksPreyResource = valueFactory.createURI(TRACKS_PREY);
		Resource preyResource = valueFactory.createURI(PREY);
		
		connection.add(huntsResource, RDFS.RANGE, preyResource);
		assertTrue(connection.hasStatement(huntsResource, RDFS.RANGE, preyResource, false));

		connection.add(tigerResource, (URI) huntsResource, deerResource);
		assertTrue(connection.hasStatement(tigerResource, (URI) huntsResource, deerResource, false));
		assertTrue(connection.hasStatement(deerResource, RDF.TYPE, preyResource, true));
		
		connection.add(tracksPreyResource, RDFS.RANGE, preyResource);
		assertTrue(connection.hasStatement(tracksPreyResource, RDFS.RANGE, preyResource, false));		
		connection.add(lionResource, (URI) tracksPreyResource, deerResource);
		assertTrue(connection.hasStatement(lionResource, (URI) tracksPreyResource, deerResource, false));
		
		connection.remove(tigerResource, (URI) huntsResource, deerResource);
		assertFalse(connection.hasStatement(tigerResource, (URI) huntsResource, deerResource, false));
		assertTrue(connection.hasStatement(deerResource, RDF.TYPE, preyResource, true));
		
		connection.remove(lionResource, (URI) tracksPreyResource, deerResource);
		assertFalse(connection.hasStatement(lionResource, (URI) tracksPreyResource, deerResource, false));
		assertFalse(connection.hasStatement(deerResource, RDF.TYPE, preyResource, true));		
	}
	
	@Ignore("There is currently no logic in place for rdf:type rule removal, as rdf:type is not yet implemented. This test will fail.")
	@Test
	public void testCheckCleanupOnRemoval() throws RepositoryException {
		Resource deerResource = valueFactory.createURI(DEER);
		Resource preyResource = valueFactory.createURI(PREY);
		Resource animalResource = valueFactory.createURI(ANIMAL);
		
		connection.add(deerResource, RDF.TYPE, preyResource);
		assertTrue(connection.hasStatement(deerResource, RDF.TYPE, preyResource, false));
		assertTrue(connection.hasStatement(deerResource, RDF.TYPE, RDFS.RESOURCE, true));
		
		connection.add(deerResource, RDF.TYPE, animalResource);
		assertTrue(connection.hasStatement(deerResource, RDF.TYPE, animalResource, false));
		
		connection.remove(deerResource, RDF.TYPE, preyResource);
		assertTrue(connection.hasStatement(deerResource, RDF.TYPE, RDFS.RESOURCE, true));
		assertFalse(connection.hasStatement(preyResource, null, null, true));
		assertFalse(connection.hasStatement(null, (URI) preyResource, null, true));
		assertFalse(connection.hasStatement(null, null, preyResource, true));
		
		connection.remove(deerResource, RDF.TYPE, animalResource);
		assertFalse(connection.hasStatement(deerResource, null, null, true));
		assertFalse(connection.hasStatement(null, (URI) deerResource, null, true));
		assertFalse(connection.hasStatement(null, null, deerResource, true));
		assertFalse(connection.hasStatement(animalResource, null, null, true));
		assertFalse(connection.hasStatement(null, (URI) animalResource, null, true));
		assertFalse(connection.hasStatement(null, null, animalResource, true));		
	}
	
	@Ignore("This test fails as empty resources are not cleared up (type Resource, etc)")
	@Test
	public void testReinferencing() throws RepositoryException {
		Resource tigerResource = valueFactory.createURI(TIGER);
		Resource deerResource = valueFactory.createURI(DEER);
		Resource lionResource = valueFactory.createURI(LION);
		Resource huntsResource = valueFactory.createURI(HUNTS);
		Resource tracksPreyResource = valueFactory.createURI(TRACKS_PREY);
		Resource preyResource = valueFactory.createURI(PREY);
		
		Resource independentResource = valueFactory.createURI(INDEPENDENT_RESOURCE);
		
		connection.add(tigerResource, (URI) huntsResource, deerResource);
		assertTrue(connection.hasStatement(tigerResource, (URI) huntsResource, deerResource, false));

		connection.add(tigerResource, (URI) huntsResource, deerResource);
		assertTrue(connection.hasStatement(tigerResource, (URI) huntsResource, deerResource, false));

		connection.add(huntsResource, RDFS.RANGE, preyResource);
		assertTrue(connection.hasStatement(huntsResource, RDFS.RANGE, preyResource, false));
		assertTrue(connection.hasStatement(deerResource, RDF.TYPE, preyResource, true));
		
		connection.add(lionResource, (URI) tracksPreyResource, deerResource);
		assertTrue(connection.hasStatement(lionResource, (URI) tracksPreyResource, deerResource, false));
		connection.add(tracksPreyResource, RDFS.RANGE, preyResource);
		assertTrue(connection.hasStatement(tracksPreyResource, RDFS.RANGE, preyResource, false));		
			
		Collection<Statement> initialStatements = new ArrayList<Statement>();
		Iterations.addAll(connection.getStatements(null, null, null, true), initialStatements);
		
		connection.add(independentResource, RDFS.RANGE, RDFS.RESOURCE);
		assertTrue(connection.hasStatement(independentResource, RDFS.RANGE, RDFS.RESOURCE, false));
		connection.remove(independentResource, RDFS.RANGE, RDFS.RESOURCE);
		assertFalse(connection.hasStatement(independentResource, RDFS.RANGE, RDFS.RESOURCE, false));
		assertFalse(connection.hasStatement(independentResource, null, null, true));
		
		Collection<Statement> postReinferencingStatements = new ArrayList<Statement>();
		Iterations.addAll(connection.getStatements(null, null, null, true), postReinferencingStatements);
		
		assertTrue(initialStatements.size() == postReinferencingStatements.size());
		assertTrue(initialStatements.containsAll(postReinferencingStatements));
	}
	
	@Test
	public void testRangeAndSubclassOfDependency1() throws RepositoryException {
		Resource tigerResource = valueFactory.createURI(TIGER);
		Resource deerResource = valueFactory.createURI(DEER);
		Resource huntedByResource = valueFactory.createURI(HUNTED_BY);
		Resource predatorResource = valueFactory.createURI(PREDATOR);
		Resource carnivoreResource = valueFactory.createURI(CARNIVORE);
		
		connection.add(deerResource, (URI) huntedByResource, tigerResource);
		assertTrue(connection.hasStatement(deerResource, (URI) huntedByResource, tigerResource, false));

		connection.add(huntedByResource, RDFS.RANGE, predatorResource);
		assertTrue(connection.hasStatement(huntedByResource, RDFS.RANGE, predatorResource, false));
		assertTrue(connection.hasStatement(tigerResource, RDF.TYPE, predatorResource, true));
		
		connection.add(carnivoreResource, RDFS.SUBCLASSOF, predatorResource);
		assertTrue(connection.hasStatement(carnivoreResource, RDFS.SUBCLASSOF, predatorResource, false));
		connection.add(tigerResource, RDF.TYPE, carnivoreResource);
		assertTrue(connection.hasStatement(tigerResource, RDF.TYPE, carnivoreResource, false));
		
		connection.remove(deerResource, (URI) huntedByResource, tigerResource);
		assertTrue(connection.hasStatement(tigerResource, RDF.TYPE, predatorResource, true));
	}
	
	@Test
	public void testRangeAndSubclassOfDependency2() throws RepositoryException {
		Resource tigerResource = valueFactory.createURI(TIGER);
		Resource deerResource = valueFactory.createURI(DEER);
		Resource huntedByResource = valueFactory.createURI(HUNTED_BY);
		Resource predatorResource = valueFactory.createURI(PREDATOR);
		Resource carnivoreResource = valueFactory.createURI(CARNIVORE);
		
		connection.add(deerResource, (URI) huntedByResource, tigerResource);
		assertTrue(connection.hasStatement(deerResource, (URI) huntedByResource, tigerResource, false));

		connection.add(huntedByResource, RDFS.RANGE, predatorResource);
		assertTrue(connection.hasStatement(huntedByResource, RDFS.RANGE, predatorResource, false));
		assertTrue(connection.hasStatement(tigerResource, RDF.TYPE, predatorResource, true));
		
		connection.add(carnivoreResource, RDFS.SUBCLASSOF, predatorResource);
		assertTrue(connection.hasStatement(carnivoreResource, RDFS.SUBCLASSOF, predatorResource, false));
		connection.add(tigerResource, RDF.TYPE, carnivoreResource);
		assertTrue(connection.hasStatement(tigerResource, RDF.TYPE, carnivoreResource, false));
		
		connection.remove(huntedByResource, RDFS.RANGE, predatorResource);
		assertTrue(connection.hasStatement(tigerResource, RDF.TYPE, predatorResource, true));
	}
	
}
