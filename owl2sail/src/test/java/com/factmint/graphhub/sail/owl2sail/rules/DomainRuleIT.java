package com.factmint.graphhub.sail.owl2sail.rules;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import info.aduna.iteration.Iterations;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.Ignore;
import org.junit.Test;
import org.openrdf.model.Resource;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.vocabulary.RDF;
import org.openrdf.model.vocabulary.RDFS;
import org.openrdf.repository.RepositoryException;

import com.factmint.graphhub.sail.owl2sail.Owl2TestBase;

public class DomainRuleIT extends Owl2TestBase {

	private static final String TIGER = "http://test.owl2.com/Tiger";
	private static final String DEER = "http://test.owl2.com/Deer";
	private static final String ANTELOPE = "http://test.owl2.com/Antelope";
	private static final String PREDATOR = "http://test.owl2.com/Predator";
	private static final String CAT = "http://test.owl2.com/Cat";
	private static final String HUNTS = "http://test.owl2.com/hunts";
	private static final String TRACKS_PREY = "http://test.owl2.com/tracksPrey";
	private static final String CARNIVORE = "http://test.owl2.com/Carnivore";
	private static final String INDEPENDENT_RESOURCE = "http://test.owl2.com/IndependentResource";
	
	@Test
	public void testDomainRule_1() throws RepositoryException {
		Resource tigerResource = valueFactory.createURI(TIGER);
		Resource deerResource = valueFactory.createURI(DEER);
		Resource antelopeResource = valueFactory.createURI(ANTELOPE);
		Resource huntsResource = valueFactory.createURI(HUNTS);
		Resource tracksPreyResource = valueFactory.createURI(TRACKS_PREY);
		Resource predatorResource = valueFactory.createURI(PREDATOR);
		
		connection.add(tigerResource, (URI) huntsResource, deerResource);
		assertTrue(connection.hasStatement(tigerResource, (URI) huntsResource, deerResource, false));

		connection.add(huntsResource, RDFS.DOMAIN, predatorResource);
		assertTrue(connection.hasStatement(huntsResource, RDFS.DOMAIN, predatorResource, false));
		assertTrue(connection.hasStatement(tigerResource, RDF.TYPE, predatorResource, true));
		
		connection.add(tigerResource, (URI) tracksPreyResource, antelopeResource);
		assertTrue(connection.hasStatement(tigerResource, (URI) tracksPreyResource, antelopeResource, false));
		connection.add(tracksPreyResource, RDFS.DOMAIN, predatorResource);
		assertTrue(connection.hasStatement(tracksPreyResource, RDFS.DOMAIN, predatorResource, false));		
		
		connection.remove(huntsResource, RDFS.DOMAIN, predatorResource);
		assertFalse(connection.hasStatement(huntsResource, RDFS.DOMAIN, predatorResource, false));
		assertTrue(connection.hasStatement(tigerResource, RDF.TYPE, predatorResource, true));
		
		connection.remove(tracksPreyResource, RDFS.DOMAIN, predatorResource);
		assertFalse(connection.hasStatement(tracksPreyResource, RDFS.DOMAIN, predatorResource, false));
		assertFalse(connection.hasStatement(tigerResource, RDF.TYPE, predatorResource, true));
	}
	
	@Test
	public void testDomainRule_2() throws RepositoryException {
		Resource tigerResource = valueFactory.createURI(TIGER);
		Resource deerResource = valueFactory.createURI(DEER);
		Resource antelopeResource = valueFactory.createURI(ANTELOPE);
		Resource huntsResource = valueFactory.createURI(HUNTS);
		Resource tracksPreyResource = valueFactory.createURI(TRACKS_PREY);
		Resource predatorResource = valueFactory.createURI(PREDATOR);

		connection.add(huntsResource, RDFS.DOMAIN, predatorResource);
		assertTrue(connection.hasStatement(huntsResource, RDFS.DOMAIN, predatorResource, false));
		
		connection.add(tigerResource, (URI) huntsResource, deerResource);
		assertTrue(connection.hasStatement(tigerResource, (URI) huntsResource, deerResource, false));
		assertTrue(connection.hasStatement(tigerResource, RDF.TYPE, predatorResource, true));
		
		connection.add(tigerResource, (URI) tracksPreyResource, antelopeResource);
		assertTrue(connection.hasStatement(tigerResource, (URI) tracksPreyResource, antelopeResource, false));
		connection.add(tracksPreyResource, RDFS.DOMAIN, predatorResource);
		assertTrue(connection.hasStatement(tracksPreyResource, RDFS.DOMAIN, predatorResource, false));	
		
		connection.remove(tigerResource, (URI) huntsResource, deerResource);
		assertFalse(connection.hasStatement(tigerResource, (URI) huntsResource, deerResource, false));
		assertTrue(connection.hasStatement(tigerResource, RDF.TYPE, predatorResource, true));
		
		connection.remove(tigerResource, (URI) tracksPreyResource, antelopeResource);
		assertFalse(connection.hasStatement(tigerResource, (URI) tracksPreyResource, antelopeResource, false));
		assertFalse(connection.hasStatement(tigerResource, RDF.TYPE, predatorResource, true));
	}
	
	@Ignore("There is currently no logic in place for rdf:type rule removal, as rdf:type is not yet implemented. This test will fail.")
	@Test
	public void testCheckCleanupOnRemoval() throws RepositoryException {
		Resource tigerResource = valueFactory.createURI(TIGER);
		Resource predatorResource = valueFactory.createURI(PREDATOR);
		Resource catResource = valueFactory.createURI(CAT);
		
		connection.add(tigerResource, RDF.TYPE, predatorResource);
		assertTrue(connection.hasStatement(tigerResource, RDF.TYPE, predatorResource, false));
		assertTrue(connection.hasStatement(tigerResource, RDF.TYPE, RDFS.RESOURCE, true));
		
		connection.add(tigerResource, RDF.TYPE, catResource);
		assertTrue(connection.hasStatement(tigerResource, RDF.TYPE, catResource, false));
		
		connection.remove(tigerResource, RDF.TYPE, predatorResource);
		assertTrue(connection.hasStatement(tigerResource, RDF.TYPE, RDFS.RESOURCE, true));
				
		assertFalse(connection.hasStatement(predatorResource, null, null, true));
		assertFalse(connection.hasStatement(null, (URI) predatorResource, null, true));
		assertFalse(connection.hasStatement(null, null, predatorResource, true));
		
		connection.remove(tigerResource, RDF.TYPE, catResource);
		assertFalse(connection.hasStatement(tigerResource, null, null, true));
		assertFalse(connection.hasStatement(null, (URI) tigerResource, null, true));
		assertFalse(connection.hasStatement(null, null, tigerResource, true));
		assertFalse(connection.hasStatement(catResource, null, null, true));
		assertFalse(connection.hasStatement(null, (URI) catResource, null, true));
		assertFalse(connection.hasStatement(null, null, catResource, true));		
	}
	
	@Test
	public void testDomainStatementRemovalCase1_1() throws RepositoryException {
		Resource tigerResource = valueFactory.createURI(TIGER);
		Resource huntsResource = valueFactory.createURI(HUNTS);
		Resource trackPreyResource = valueFactory.createURI(TRACKS_PREY);
		Resource deerResource = valueFactory.createURI(DEER);
		Resource predatorResource = valueFactory.createURI(PREDATOR);
		
		connection.add(tigerResource, (URI) huntsResource, deerResource);
		assertTrue(connection.hasStatement(tigerResource, (URI) huntsResource, deerResource, false));

		connection.add(huntsResource, RDFS.DOMAIN, predatorResource);
		assertTrue(connection.hasStatement(huntsResource, RDFS.DOMAIN, predatorResource, false));
		assertTrue(connection.hasStatement(tigerResource, RDF.TYPE, predatorResource, true));
		
		connection.add(tigerResource, (URI) trackPreyResource, deerResource);
		assertTrue(connection.hasStatement(tigerResource, (URI) trackPreyResource, deerResource, false));

		connection.add(trackPreyResource, RDFS.DOMAIN, predatorResource);
		assertTrue(connection.hasStatement(huntsResource, RDFS.DOMAIN, predatorResource, false));
		
		connection.remove(huntsResource, RDFS.DOMAIN, predatorResource);
		assertTrue(connection.hasStatement(tigerResource, RDF.TYPE, predatorResource, true));
		
		connection.remove(trackPreyResource, RDFS.DOMAIN, predatorResource);
		assertFalse(connection.hasStatement(tigerResource, RDF.TYPE, predatorResource, true));
	}
	
	@Test
	public void testDomainStatementRemovalCase1_2() throws RepositoryException {
		Resource tigerResource = valueFactory.createURI(TIGER);
		Resource huntsResource = valueFactory.createURI(HUNTS);
		Resource trackPreyResource = valueFactory.createURI(TRACKS_PREY);
		Resource deerResource = valueFactory.createURI(DEER);
		Resource predatorResource = valueFactory.createURI(PREDATOR);
		
		connection.add(tigerResource, (URI) huntsResource, deerResource);
		assertTrue(connection.hasStatement(tigerResource, (URI) huntsResource, deerResource, false));

		connection.add(huntsResource, RDFS.DOMAIN, predatorResource);
		assertTrue(connection.hasStatement(huntsResource, RDFS.DOMAIN, predatorResource, false));
		assertTrue(connection.hasStatement(tigerResource, RDF.TYPE, predatorResource, true));
		
		connection.add(tigerResource, (URI) trackPreyResource, deerResource);
		assertTrue(connection.hasStatement(tigerResource, (URI) trackPreyResource, deerResource, false));

		connection.add(trackPreyResource, RDFS.DOMAIN, predatorResource);
		assertTrue(connection.hasStatement(huntsResource, RDFS.DOMAIN, predatorResource, false));
		
		connection.remove(huntsResource, RDFS.DOMAIN, predatorResource);
		assertTrue(connection.hasStatement(tigerResource, RDF.TYPE, predatorResource, true));
		
		connection.remove(tigerResource, (URI) trackPreyResource, deerResource);
		assertFalse(connection.hasStatement(tigerResource, RDF.TYPE, predatorResource, true));
	}
	
	@Test
	public void testDomainStatementRemovalCase2_1() throws RepositoryException {
		Resource tigerResource = valueFactory.createURI(TIGER);
		Resource huntsResource = valueFactory.createURI(HUNTS);
		Resource deerResource = valueFactory.createURI(DEER);
		Resource predatorResource = valueFactory.createURI(PREDATOR);
		Resource carnivoreResource = valueFactory.createURI(CARNIVORE);
		
		connection.add(tigerResource, (URI) huntsResource, deerResource);
		assertTrue(connection.hasStatement(tigerResource, (URI) huntsResource, deerResource, false));

		connection.add(huntsResource, RDFS.DOMAIN, predatorResource);
		assertTrue(connection.hasStatement(huntsResource, RDFS.DOMAIN, predatorResource, false));
		assertTrue(connection.hasStatement(tigerResource, RDF.TYPE, predatorResource, true));
		
		connection.add(huntsResource, RDFS.DOMAIN, carnivoreResource);
		assertTrue(connection.hasStatement(huntsResource, RDFS.DOMAIN, carnivoreResource, false));
	
		connection.add(carnivoreResource, RDFS.SUBCLASSOF, predatorResource);
		assertTrue(connection.hasStatement(carnivoreResource, RDFS.SUBCLASSOF, predatorResource, false));
		
		connection.remove(huntsResource, RDFS.DOMAIN, predatorResource);
		assertTrue(connection.hasStatement(tigerResource, RDF.TYPE, predatorResource, true));
		
		connection.remove(huntsResource, RDFS.DOMAIN, carnivoreResource);
		assertFalse(connection.hasStatement(tigerResource, RDF.TYPE, predatorResource, true));
	}
	
	@Test
	public void testDomainStatementRemovalCase2_2() throws RepositoryException {
		Resource tigerResource = valueFactory.createURI(TIGER);
		Resource huntsResource = valueFactory.createURI(HUNTS);
		Resource deerResource = valueFactory.createURI(DEER);
		Resource predatorResource = valueFactory.createURI(PREDATOR);
		Resource carnivoreResource = valueFactory.createURI(CARNIVORE);
		
		connection.add(tigerResource, (URI) huntsResource, deerResource);
		assertTrue(connection.hasStatement(tigerResource, (URI) huntsResource, deerResource, false));

		connection.add(huntsResource, RDFS.DOMAIN, predatorResource);
		assertTrue(connection.hasStatement(huntsResource, RDFS.DOMAIN, predatorResource, false));
		assertTrue(connection.hasStatement(tigerResource, RDF.TYPE, predatorResource, true));
		
		connection.add(huntsResource, RDFS.DOMAIN, carnivoreResource);
		assertTrue(connection.hasStatement(huntsResource, RDFS.DOMAIN, carnivoreResource, false));
	
		connection.add(carnivoreResource, RDFS.SUBCLASSOF, predatorResource);
		assertTrue(connection.hasStatement(carnivoreResource, RDFS.SUBCLASSOF, predatorResource, false));
		
		connection.remove(huntsResource, RDFS.DOMAIN, predatorResource);
		assertTrue(connection.hasStatement(tigerResource, RDF.TYPE, predatorResource, true));
		
		connection.remove(carnivoreResource, RDFS.SUBCLASSOF, predatorResource);
		assertFalse(connection.hasStatement(tigerResource, RDF.TYPE, predatorResource, true));
	}
	
	@Test
	public void testDomainStatementRemovalCase3() throws RepositoryException {
		Resource tigerResource = valueFactory.createURI(TIGER);
		Resource huntsResource = valueFactory.createURI(HUNTS);
		Resource tracksPreyResource = valueFactory.createURI(TRACKS_PREY);
		Resource deerResource = valueFactory.createURI(DEER);
		Resource predatorResource = valueFactory.createURI(PREDATOR);
		Resource carnivoreResource = valueFactory.createURI(CARNIVORE);
		
		connection.add(tigerResource, (URI) huntsResource, deerResource);
		assertTrue(connection.hasStatement(tigerResource, (URI) huntsResource, deerResource, false));

		connection.add(huntsResource, RDFS.DOMAIN, predatorResource);
		assertTrue(connection.hasStatement(huntsResource, RDFS.DOMAIN, predatorResource, false));
		assertTrue(connection.hasStatement(tigerResource, RDF.TYPE, predatorResource, true));
		
		connection.add(tigerResource, (URI) tracksPreyResource, deerResource);
		assertTrue(connection.hasStatement(tigerResource, (URI) tracksPreyResource, deerResource, false));

		connection.add(tracksPreyResource, RDFS.DOMAIN, carnivoreResource);
		assertTrue(connection.hasStatement(tracksPreyResource, RDFS.DOMAIN, carnivoreResource, false));
		
		connection.add(carnivoreResource, RDFS.SUBCLASSOF, predatorResource);
		assertTrue(connection.hasStatement(carnivoreResource, RDFS.SUBCLASSOF, predatorResource, false));
				
		connection.remove(huntsResource, RDFS.DOMAIN, predatorResource);
		assertTrue("tiger is still a predator by tracks-domain-carnivor & carnivor-SCO-predator", connection.hasStatement(tigerResource, RDF.TYPE, predatorResource, true));
		
		connection.remove(tracksPreyResource, RDFS.DOMAIN, carnivoreResource);
		assertFalse(connection.hasStatement(tigerResource, RDF.TYPE, predatorResource, true));		
	}	
	
	@Test
	public void testDomainAndSubclassOfDependency1() throws RepositoryException {
		Resource tigerResource = valueFactory.createURI(TIGER);
		Resource huntsResource = valueFactory.createURI(HUNTS);
		Resource deerResource = valueFactory.createURI(DEER);
		Resource carnivoreResource = valueFactory.createURI(CARNIVORE);
		Resource predatorResource = valueFactory.createURI(PREDATOR);
		
		connection.add(tigerResource, (URI) huntsResource, deerResource);
		assertTrue(connection.hasStatement(tigerResource, (URI) huntsResource, deerResource, false));

		connection.add(huntsResource, RDFS.DOMAIN, predatorResource);
		assertTrue(connection.hasStatement(huntsResource, RDFS.DOMAIN, predatorResource, false));
		assertTrue(connection.hasStatement(tigerResource, RDF.TYPE, predatorResource, true));
		
		connection.add(tigerResource, RDF.TYPE, carnivoreResource);
		assertTrue(connection.hasStatement(tigerResource, RDF.TYPE, carnivoreResource, false));
		
		connection.add(carnivoreResource, RDFS.SUBCLASSOF, predatorResource);
		assertTrue(connection.hasStatement(carnivoreResource, RDFS.SUBCLASSOF, predatorResource, false));
		
		connection.remove(tigerResource, (URI) huntsResource, deerResource);
		assertTrue(connection.hasStatement(tigerResource, RDF.TYPE, predatorResource, true));
	}
	
	@Test
	public void testDomainAndSubclassOfDependency2() throws RepositoryException {
		Resource tigerResource = valueFactory.createURI(TIGER);
		Resource huntsResource = valueFactory.createURI(HUNTS);
		Resource deerResource = valueFactory.createURI(DEER);
		Resource carnivoreResource = valueFactory.createURI(CARNIVORE);
		Resource predatorResource = valueFactory.createURI(PREDATOR);
		
		connection.add(tigerResource, (URI) huntsResource, deerResource);
		assertTrue(connection.hasStatement(tigerResource, (URI) huntsResource, deerResource, false));

		connection.add(huntsResource, RDFS.DOMAIN, predatorResource);
		assertTrue(connection.hasStatement(huntsResource, RDFS.DOMAIN, predatorResource, false));
		assertTrue(connection.hasStatement(tigerResource, RDF.TYPE, predatorResource, true));
		
		connection.add(tigerResource, RDF.TYPE, carnivoreResource);
		assertTrue(connection.hasStatement(tigerResource, RDF.TYPE, carnivoreResource, false));
		
		connection.add(carnivoreResource, RDFS.SUBCLASSOF, predatorResource);
		assertTrue(connection.hasStatement(carnivoreResource, RDFS.SUBCLASSOF, predatorResource, false));
		
		connection.remove(huntsResource, RDFS.DOMAIN, predatorResource);
		assertTrue(connection.hasStatement(tigerResource, RDF.TYPE, predatorResource, true));
	}
	
	@Ignore("This test fails as empty resources are not cleared up (type Resource, etc)")
	@Test
	public void testReinferencing() throws RepositoryException {
		Resource tigerResource = valueFactory.createURI(TIGER);
		Resource deerResource = valueFactory.createURI(DEER);
		Resource antelopeResource = valueFactory.createURI(ANTELOPE);
		Resource huntsResource = valueFactory.createURI(HUNTS);
		Resource tracksPreyResource = valueFactory.createURI(TRACKS_PREY);
		Resource predatorResource = valueFactory.createURI(PREDATOR);
		
		Resource independentResource = valueFactory.createURI(INDEPENDENT_RESOURCE);
		
		connection.add(tigerResource, (URI) huntsResource, deerResource);
		assertTrue(connection.hasStatement(tigerResource, (URI) huntsResource, deerResource, false));

		connection.add(huntsResource, RDFS.DOMAIN, predatorResource);
		assertTrue(connection.hasStatement(huntsResource, RDFS.DOMAIN, predatorResource, false));
		assertTrue(connection.hasStatement(tigerResource, RDF.TYPE, predatorResource, true));
		
		connection.add(tigerResource, (URI) tracksPreyResource, antelopeResource);
		assertTrue(connection.hasStatement(tigerResource, (URI) tracksPreyResource, antelopeResource, false));
		connection.add(tracksPreyResource, RDFS.DOMAIN, predatorResource);
		assertTrue(connection.hasStatement(tracksPreyResource, RDFS.DOMAIN, predatorResource, false));		
			
		Collection<Statement> initialStatements = new ArrayList<Statement>();
		Iterations.addAll(connection.getStatements(null, null, null, true), initialStatements);
		
		connection.add(independentResource, RDFS.DOMAIN, RDFS.RESOURCE);
		assertTrue(connection.hasStatement(independentResource, RDFS.DOMAIN, RDFS.RESOURCE, false));
		connection.remove(independentResource, RDFS.DOMAIN, RDFS.RESOURCE);
		assertFalse(connection.hasStatement(independentResource, RDFS.DOMAIN, RDFS.RESOURCE, false));
		assertFalse(connection.hasStatement(independentResource, null, null, true));
		
		Collection<Statement> postReinferencingStatements = new ArrayList<Statement>();
		Iterations.addAll(connection.getStatements(null, null, null, true), postReinferencingStatements);
		
		assertEquals("The reinference should not change the size of the store", initialStatements.size(), postReinferencingStatements.size());
		assertTrue("The reinference should not change the size of the store", initialStatements.containsAll(postReinferencingStatements));
	}
	
}
