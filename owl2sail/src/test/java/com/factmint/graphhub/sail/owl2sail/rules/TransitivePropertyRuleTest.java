package com.factmint.graphhub.sail.owl2sail.rules;

import static com.factmint.graphhub.sail.owl2sail.sesame.utility.Constants.RULE_DEPENDENCIES;
import static com.factmint.graphhub.sail.owl2sail.sesame.utility.Constants.TRANSITIVE_PROPERTY_RULE;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import info.aduna.iteration.CloseableIteratorIteration;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.openrdf.model.Resource;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.ValueFactory;
import org.openrdf.model.impl.ValueFactoryImpl;
import org.openrdf.model.vocabulary.RDF;
import org.openrdf.sail.SailException;
import org.openrdf.sail.inferencer.InferencerConnection;

import com.factmint.graphhub.sail.owl2sail.axioms.OWL2;
import com.factmint.graphhub.sail.owl2sail.rules.TransitivePropertyRule;
import com.google.common.collect.Sets;

public class TransitivePropertyRuleTest {

	private static final String UK = "http://test.owl2.com/UK";
	private static final String LONDON = "http://test.owl2.com/London";
	private static final String ABBEY_ROAD = "http://test.owl2.com/AbbeyRoad";
	private static final String IS_IN = "http://test.owl2.com/isIn";
	private InferencerConnection mockConnection = mock(InferencerConnection.class);
	private ValueFactory valueFactory = ValueFactoryImpl.getInstance(); 
	
	private TransitivePropertyRule ruleUnderTest;
	
	@Before
	public void setup() {
		ruleUnderTest = new TransitivePropertyRule(mockConnection, RULE_DEPENDENCIES);
	}
	
	@Test
	public void testDependenciesOfTransitiveProperty() throws SailException {
		URI isInUri = valueFactory.createURI(IS_IN);
		Resource abbeyRoadResource = valueFactory.createURI(ABBEY_ROAD);
		Resource londonResource = valueFactory.createURI(LONDON);
		Resource ukResource = valueFactory.createURI(UK);		
		
		Set<Statement> addedStatements = Sets.newHashSet(valueFactory.createStatement(isInUri, RDF.TYPE, OWL2.TRANSITIVEPROPERTY));
		Set<String> rules = new HashSet<String>();
		rules.add(TRANSITIVE_PROPERTY_RULE);
		
		Set<Statement> statementList = new HashSet<Statement>();
		statementList.add(valueFactory.createStatement(abbeyRoadResource, isInUri, londonResource));
		Iterator<Statement> statementListIterator = statementList.iterator();
		when(mockConnection.getStatements(null, isInUri, null, true)).thenReturn(new CloseableIteratorIteration(statementListIterator));
		
		Set<Statement> statementList2 = new HashSet<Statement>();
		statementList2.add(valueFactory.createStatement(londonResource, isInUri, ukResource));
		Iterator<Statement> statementListIterator2 = statementList2.iterator();
		when(mockConnection.getStatements(londonResource, isInUri, null, true)).thenReturn(new CloseableIteratorIteration(statementListIterator2));
	
		when(mockConnection.addInferredStatement(abbeyRoadResource, isInUri, ukResource, null)).thenReturn(true);
		
		Collection<String> rulesToApplyNextIteration = ruleUnderTest.apply(addedStatements, null, rules);
		assertEquals(RULE_DEPENDENCIES.get(TRANSITIVE_PROPERTY_RULE), rulesToApplyNextIteration);
	}
	
}
