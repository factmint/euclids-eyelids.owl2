package com.factmint.graphhub.sail.owl2sail.rules;

import static com.factmint.graphhub.sail.owl2sail.sesame.utility.Constants.COMPLEMENT_OF_RULE;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openrdf.model.Resource;
import org.openrdf.model.vocabulary.RDF;
import org.openrdf.repository.RepositoryException;

import com.factmint.graphhub.sail.owl2sail.Owl2TestBase;
import com.factmint.graphhub.sail.owl2sail.axioms.OWL2;

public class ComplementOfRuleIT extends Owl2TestBase {

	private static final String MALE = "http://test.owl2.com/Male";
	private static final String FEMALE = "http://test.owl2.com/Female";
	private static final String ELLIOT = "http://test.owl2.com/Elliot";
	
	@Test(expected=RepositoryException.class)
	public void testAddingComplementOfStatement() throws RepositoryException {
		Resource maleResource = valueFactory.createURI(MALE);
		Resource femaleResource = valueFactory.createURI(FEMALE);
		Resource elliotResource = valueFactory.createURI(ELLIOT);
				
		connection.add(elliotResource, RDF.TYPE, maleResource);
		assertTrue(connection.hasStatement(elliotResource, RDF.TYPE, maleResource, false));
		
		connection.add(elliotResource, RDF.TYPE, femaleResource);
		assertTrue(connection.hasStatement(elliotResource, RDF.TYPE, femaleResource, false));
		
		try {
			connection.add(maleResource, OWL2.COMPLEMENTOF, femaleResource);
		} catch (RepositoryException e) {
			assertTrue(e.getMessage().contains(COMPLEMENT_OF_RULE));		
			throw e;
		}
	}
	
	@Test(expected=RepositoryException.class)
	public void testAddingInvalidTypeStatement() throws RepositoryException {
		Resource maleResource = valueFactory.createURI(MALE);
		Resource femaleResource = valueFactory.createURI(FEMALE);
		Resource elliotResource = valueFactory.createURI(ELLIOT);
				
		connection.add(maleResource, OWL2.COMPLEMENTOF, femaleResource);
		assertTrue(connection.hasStatement(maleResource, OWL2.COMPLEMENTOF, femaleResource, false));
		
		connection.add(elliotResource, RDF.TYPE, maleResource);
		assertTrue(connection.hasStatement(elliotResource, RDF.TYPE, maleResource, false));
		
		try {
			connection.add(elliotResource, RDF.TYPE, femaleResource);
		} catch (RepositoryException e) {
			assertTrue(e.getMessage().contains(COMPLEMENT_OF_RULE));		
			throw e;
		}
	}
	
	@Test(expected=RepositoryException.class)
	public void testAddingInvalidTypeStatement2() throws RepositoryException {
		Resource maleResource = valueFactory.createURI(MALE);
		Resource femaleResource = valueFactory.createURI(FEMALE);
		Resource elliotResource = valueFactory.createURI(ELLIOT);
				
		connection.add(maleResource, OWL2.COMPLEMENTOF, femaleResource);
		assertTrue(connection.hasStatement(maleResource, OWL2.COMPLEMENTOF, femaleResource, false));
		
		connection.add(elliotResource, RDF.TYPE, femaleResource);
		assertTrue(connection.hasStatement(elliotResource, RDF.TYPE, femaleResource, false));
		
		try {
			connection.add(elliotResource, RDF.TYPE, maleResource);
		} catch (RepositoryException e) {
			assertTrue(e.getMessage().contains(COMPLEMENT_OF_RULE));		
			throw e;
		}
	}
	
	@Override
	public void testCheckCleanupOnRemoval() throws RepositoryException {
		// This rule does not involve any statement removal logic. 
		assertTrue(true);
	}

}
