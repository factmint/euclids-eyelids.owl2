package com.factmint.graphhub.sail.owl2sail.rules;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Ignore;
import org.junit.Test;
import org.openrdf.model.Resource;
import org.openrdf.model.URI;
import org.openrdf.model.vocabulary.RDF;
import org.openrdf.model.vocabulary.RDFS;
import org.openrdf.repository.RepositoryException;

import com.factmint.graphhub.sail.owl2sail.Owl2TestBase;
import com.factmint.graphhub.sail.owl2sail.axioms.OWL2;

public class SubClassOfRuleIT extends Owl2TestBase {

	private static final String LIVING_THING = "http://test.owl2.com/LivingThing";
	private static final String ORGANISM = "http://test.owl2.com/Organism";
	private static final String ANIMAL = "http://test.owl2.com/Animal";
	private static final String MAMMAL = "http://test.owl2.com/Mammal";
	private static final String TIGER = "http://test.owl2.com/Tiger";
	private static final String LION = "http://test.owl2.com/Lion";
	private static final String DEER = "http://test.owl2.com/Deer";
	private static final String ANTELOPE = "http://test.owl2.com/Antelope";
	private static final String PREDATOR = "http://test.owl2.com/Predator";
	private static final String PREY = "http://test.owl2.com/Prey";
	private static final String HUNTS = "http://test.owl2.com/hunts";
	private static final String CARNIVORE = "http://test.owl2.com/Carnivore";
	
	@Test
	public void testTransitivity() throws RepositoryException {
		assertTrue(connection.hasStatement(RDFS.SUBCLASSOF, RDF.TYPE, OWL2.TRANSITIVEPROPERTY, true));
				
		Resource animalResource = valueFactory.createURI(ANIMAL);
		Resource mammalResource = valueFactory.createURI(MAMMAL);
		Resource tigerResource = valueFactory.createURI(TIGER);
		
		connection.add(tigerResource, RDFS.SUBCLASSOF, mammalResource);
		assertTrue(connection.hasStatement(tigerResource, RDFS.SUBCLASSOF, mammalResource, false));

		connection.add(mammalResource, RDFS.SUBCLASSOF, animalResource);
		assertTrue(connection.hasStatement(mammalResource, RDFS.SUBCLASSOF, animalResource, false));
		assertTrue(connection.hasStatement(tigerResource, RDFS.SUBCLASSOF, animalResource, true));
		
		connection.remove(mammalResource, RDFS.SUBCLASSOF, animalResource);
		assertFalse(connection.hasStatement(mammalResource, RDFS.SUBCLASSOF, animalResource, false));
		assertFalse(connection.hasStatement(tigerResource, RDFS.SUBCLASSOF, animalResource, true));
	}
	
	@Test
	public void testSubclassOfClassAxiomRule_1() throws RepositoryException {
		Resource animalResource = valueFactory.createURI(ANIMAL);
		Resource mammalResource = valueFactory.createURI(MAMMAL);
		Resource tigerResource = valueFactory.createURI(TIGER);
		
		connection.add(tigerResource, RDF.TYPE, mammalResource);
		assertTrue(connection.hasStatement(tigerResource, RDF.TYPE, mammalResource, false));

		connection.add(mammalResource, RDFS.SUBCLASSOF, animalResource);
		assertTrue(connection.hasStatement(mammalResource, RDFS.SUBCLASSOF, animalResource, false));
		assertTrue(connection.hasStatement(tigerResource, RDF.TYPE, animalResource, true));
		
		connection.remove(mammalResource, RDFS.SUBCLASSOF, animalResource);
		assertFalse(connection.hasStatement(mammalResource, RDFS.SUBCLASSOF, animalResource, false));
		assertFalse(connection.hasStatement(tigerResource, RDF.TYPE, animalResource, true));
	}
	
	@Test
	public void testSubclassOfClassAxiomRule_2() throws RepositoryException {
		Resource animalResource = valueFactory.createURI(ANIMAL);
		Resource mammalResource = valueFactory.createURI(MAMMAL);
		Resource tigerResource = valueFactory.createURI(TIGER);
		
		connection.add(mammalResource, RDFS.SUBCLASSOF, animalResource);
		assertTrue(connection.hasStatement(mammalResource, RDFS.SUBCLASSOF, animalResource, false));
		
		connection.add(tigerResource, RDF.TYPE, mammalResource);
		assertTrue(connection.hasStatement(tigerResource, RDF.TYPE, mammalResource, false));
		assertTrue(connection.hasStatement(tigerResource, RDF.TYPE, animalResource, true));
		
		connection.remove(tigerResource, RDF.TYPE, mammalResource);
		assertFalse(connection.hasStatement(tigerResource, RDF.TYPE, mammalResource, false));
		assertFalse(connection.hasStatement(tigerResource, RDF.TYPE, animalResource, true));
	}
	
	@Test
	public void testSubclassOfEquivalentClassRule() throws RepositoryException {
		Resource livingThingResource = valueFactory.createURI(LIVING_THING);
		Resource organismResource = valueFactory.createURI(ORGANISM);
		
		connection.add(livingThingResource, RDFS.SUBCLASSOF, organismResource);
		assertTrue(connection.hasStatement(livingThingResource, RDFS.SUBCLASSOF, organismResource, false));

		connection.add(organismResource, RDFS.SUBCLASSOF, livingThingResource);
		assertTrue(connection.hasStatement(organismResource, RDFS.SUBCLASSOF, livingThingResource, false));
		assertTrue(connection.hasStatement(livingThingResource, OWL2.EQUIVALENTCLASS, organismResource, true));
		assertTrue(connection.hasStatement(organismResource, OWL2.EQUIVALENTCLASS, livingThingResource, true));
		
		connection.remove(organismResource, RDFS.SUBCLASSOF, livingThingResource);
		assertFalse(connection.hasStatement(organismResource, RDFS.SUBCLASSOF, livingThingResource, false));
		assertFalse(connection.hasStatement(livingThingResource, OWL2.EQUIVALENTCLASS, organismResource, true));
		assertFalse(connection.hasStatement(organismResource, OWL2.EQUIVALENTCLASS, livingThingResource, true));
	}
	
	@Test
	public void testSubclassOfDomainRule_1() throws RepositoryException {
		Resource huntsResource = valueFactory.createURI(HUNTS);
		Resource predatorResource = valueFactory.createURI(PREDATOR);
		Resource tigerResource = valueFactory.createURI(TIGER);
		Resource lionResource = valueFactory.createURI(LION);
		
		connection.add(tigerResource, RDFS.SUBCLASSOF, predatorResource);
		assertTrue(connection.hasStatement(tigerResource, RDFS.SUBCLASSOF, predatorResource, false));
		connection.add(huntsResource, RDFS.DOMAIN, tigerResource);
		assertTrue(connection.hasStatement(huntsResource, RDFS.DOMAIN, tigerResource, false));
		assertTrue(connection.hasStatement(huntsResource, RDFS.DOMAIN, predatorResource, true));
	
		connection.add(lionResource, RDFS.SUBCLASSOF, predatorResource);
		assertTrue(connection.hasStatement(lionResource, RDFS.SUBCLASSOF, predatorResource, false));
		connection.add(huntsResource, RDFS.DOMAIN, lionResource);
		assertTrue(connection.hasStatement(huntsResource, RDFS.DOMAIN, lionResource, false));
		
		connection.remove(huntsResource, RDFS.DOMAIN, tigerResource);
		assertFalse(connection.hasStatement(huntsResource, RDFS.DOMAIN, tigerResource, false));
		assertTrue(connection.hasStatement(huntsResource, RDFS.DOMAIN, predatorResource, true));
		
		connection.remove(huntsResource, RDFS.DOMAIN, lionResource);
		assertFalse(connection.hasStatement(huntsResource, RDFS.DOMAIN, lionResource, false));
		assertFalse(connection.hasStatement(huntsResource, RDFS.DOMAIN, predatorResource, true));		
	}
	
	@Test
	public void testSubclassOfDomainRule_2() throws RepositoryException {
		Resource huntsResource = valueFactory.createURI(HUNTS);
		Resource predatorResource = valueFactory.createURI(PREDATOR);
		Resource tigerResource = valueFactory.createURI(TIGER);
		Resource lionResource = valueFactory.createURI(LION);
		
		connection.add(huntsResource, RDFS.DOMAIN, tigerResource);
		assertTrue(connection.hasStatement(huntsResource, RDFS.DOMAIN, tigerResource, false));
		connection.add(tigerResource, RDFS.SUBCLASSOF, predatorResource);
		assertTrue(connection.hasStatement(tigerResource, RDFS.SUBCLASSOF, predatorResource, false));
		assertTrue(connection.hasStatement(huntsResource, RDFS.DOMAIN, predatorResource, true));
	
		connection.add(huntsResource, RDFS.DOMAIN, lionResource);
		assertTrue(connection.hasStatement(huntsResource, RDFS.DOMAIN, lionResource, false));
		connection.add(lionResource, RDFS.SUBCLASSOF, predatorResource);
		assertTrue(connection.hasStatement(lionResource, RDFS.SUBCLASSOF, predatorResource, false));
		
		connection.remove(tigerResource, RDFS.SUBCLASSOF, predatorResource);
		assertFalse(connection.hasStatement(tigerResource, RDFS.SUBCLASSOF, predatorResource, false));
		assertTrue(connection.hasStatement(huntsResource, RDFS.DOMAIN, predatorResource, true));
		
		connection.remove(lionResource, RDFS.SUBCLASSOF, predatorResource);
		assertFalse(connection.hasStatement(lionResource, RDFS.SUBCLASSOF, predatorResource, false));
		assertFalse(connection.hasStatement(huntsResource, RDFS.DOMAIN, predatorResource, true));		
	}
	
	@Test
	public void testSubclassOfRangeRule_1() throws RepositoryException {
		Resource huntsResource = valueFactory.createURI(HUNTS);
		Resource preyResource = valueFactory.createURI(PREY);
		Resource deerResource = valueFactory.createURI(DEER);
		Resource antelopeResource = valueFactory.createURI(ANTELOPE);
		
		connection.add(deerResource, RDFS.SUBCLASSOF, preyResource);
		assertTrue(connection.hasStatement(deerResource, RDFS.SUBCLASSOF, preyResource, false));
		connection.add(huntsResource, RDFS.RANGE, deerResource);
		assertTrue(connection.hasStatement(huntsResource, RDFS.RANGE, deerResource, false));
		assertTrue(connection.hasStatement(huntsResource, RDFS.RANGE, preyResource, true));
	
		connection.add(antelopeResource, RDFS.SUBCLASSOF, preyResource);
		assertTrue(connection.hasStatement(antelopeResource, RDFS.SUBCLASSOF, preyResource, false));
		connection.add(huntsResource, RDFS.RANGE, antelopeResource);
		assertTrue(connection.hasStatement(huntsResource, RDFS.RANGE, antelopeResource, false));
		
		connection.remove(huntsResource, RDFS.RANGE, deerResource);
		assertFalse(connection.hasStatement(huntsResource, RDFS.RANGE, deerResource, false));
		assertTrue(connection.hasStatement(huntsResource, RDFS.RANGE, preyResource, true));
		
		connection.remove(huntsResource, RDFS.RANGE, antelopeResource);
		assertFalse(connection.hasStatement(huntsResource, RDFS.RANGE, antelopeResource, false));
		assertFalse(connection.hasStatement(huntsResource, RDFS.RANGE, preyResource, true));		
	}
	
	@Test
	public void testSubclassOfRangeRule_2() throws RepositoryException {
		Resource huntsResource = valueFactory.createURI(HUNTS);
		Resource preyResource = valueFactory.createURI(PREY);
		Resource deerResource = valueFactory.createURI(DEER);
		Resource antelopeResource = valueFactory.createURI(ANTELOPE);
		
		connection.add(huntsResource, RDFS.RANGE, deerResource);
		assertTrue(connection.hasStatement(huntsResource, RDFS.RANGE, deerResource, false));
		connection.add(deerResource, RDFS.SUBCLASSOF, preyResource);
		assertTrue(connection.hasStatement(deerResource, RDFS.SUBCLASSOF, preyResource, false));
		assertTrue(connection.hasStatement(huntsResource, RDFS.RANGE, preyResource, true));
	
		connection.add(huntsResource, RDFS.RANGE, antelopeResource);
		assertTrue(connection.hasStatement(huntsResource, RDFS.RANGE, antelopeResource, false));
		connection.add(antelopeResource, RDFS.SUBCLASSOF, preyResource);
		assertTrue(connection.hasStatement(antelopeResource, RDFS.SUBCLASSOF, preyResource, false));
		
		connection.remove(deerResource, RDFS.SUBCLASSOF, preyResource);
		assertFalse(connection.hasStatement(deerResource, RDFS.SUBCLASSOF, preyResource, false));
		assertTrue(connection.hasStatement(huntsResource, RDFS.RANGE, preyResource, true));
		
		connection.remove(antelopeResource, RDFS.SUBCLASSOF, preyResource);
		assertFalse(connection.hasStatement(antelopeResource, RDFS.SUBCLASSOF, preyResource, false));
		assertFalse(connection.hasStatement(huntsResource, RDFS.RANGE, preyResource, true));		
	}
	
	@Ignore("There is currently no logic in place for rdf:type rule removal, as rdf:type is not yet implemented. This test will fail.")
	@Test
	public void testCheckCleanupOnRemoval() throws RepositoryException {
		Resource animalResource = valueFactory.createURI(ANIMAL);
		Resource mammalResource = valueFactory.createURI(MAMMAL);
		Resource tigerResource = valueFactory.createURI(TIGER);
		
		connection.add(tigerResource, RDF.TYPE, mammalResource);
		assertTrue(connection.hasStatement(tigerResource, RDF.TYPE, mammalResource, false));

		connection.add(mammalResource, RDFS.SUBCLASSOF, animalResource);
		assertTrue(connection.hasStatement(mammalResource, RDFS.SUBCLASSOF, animalResource, false));
		assertTrue(connection.hasStatement(tigerResource, RDF.TYPE, animalResource, true));
		
		connection.remove(mammalResource, RDFS.SUBCLASSOF, animalResource);
		connection.remove(tigerResource, RDF.TYPE, mammalResource);
		
		assertFalse(connection.hasStatement(mammalResource, null, null, true));
		assertFalse(connection.hasStatement(null, (URI) mammalResource, null, true));
		assertFalse(connection.hasStatement(null, null, mammalResource, true));
		
		assertFalse(connection.hasStatement(tigerResource, null, null, true));
		assertFalse(connection.hasStatement(null, (URI) tigerResource, null, true));
		assertFalse(connection.hasStatement(null, null, tigerResource, true));
		
		assertFalse(connection.hasStatement(animalResource, null, null, true));
		assertFalse(connection.hasStatement(null, (URI) animalResource, null, true));
		assertFalse(connection.hasStatement(null, null, animalResource, true));
	}
	
	@Test
	public void testSubclassOfAndDomainDependency1() throws RepositoryException {
		Resource tigerResource = valueFactory.createURI(TIGER);
		Resource huntsResource = valueFactory.createURI(HUNTS);
		Resource deerResource = valueFactory.createURI(DEER);
		Resource carnivoreResource = valueFactory.createURI(CARNIVORE);
		Resource predatorResource = valueFactory.createURI(PREDATOR);
		
		connection.add(tigerResource, (URI) huntsResource, deerResource);
		assertTrue(connection.hasStatement(tigerResource, (URI) huntsResource, deerResource, false));

		connection.add(huntsResource, RDFS.DOMAIN, predatorResource);
		assertTrue(connection.hasStatement(huntsResource, RDFS.DOMAIN, predatorResource, false));
		assertTrue(connection.hasStatement(tigerResource, RDF.TYPE, predatorResource, true));
		
		connection.add(tigerResource, RDF.TYPE, carnivoreResource);
		assertTrue(connection.hasStatement(tigerResource, RDF.TYPE, carnivoreResource, false));
		
		connection.add(carnivoreResource, RDFS.SUBCLASSOF, predatorResource);
		assertTrue(connection.hasStatement(carnivoreResource, RDFS.SUBCLASSOF, predatorResource, false));
		
		connection.remove(tigerResource, RDF.TYPE, carnivoreResource);
		assertTrue(connection.hasStatement(tigerResource, RDF.TYPE, predatorResource, true));
	}
	
	@Test
	public void testSubclassOfAndDomainDependency2() throws RepositoryException {
		Resource tigerResource = valueFactory.createURI(TIGER);
		Resource huntsResource = valueFactory.createURI(HUNTS);
		Resource deerResource = valueFactory.createURI(DEER);
		Resource carnivoreResource = valueFactory.createURI(CARNIVORE);
		Resource predatorResource = valueFactory.createURI(PREDATOR);
		
		connection.add(tigerResource, (URI) huntsResource, deerResource);
		assertTrue(connection.hasStatement(tigerResource, (URI) huntsResource, deerResource, false));

		connection.add(huntsResource, RDFS.DOMAIN, predatorResource);
		assertTrue(connection.hasStatement(huntsResource, RDFS.DOMAIN, predatorResource, false));
		assertTrue(connection.hasStatement(tigerResource, RDF.TYPE, predatorResource, true));
		
		connection.add(tigerResource, RDF.TYPE, carnivoreResource);
		assertTrue(connection.hasStatement(tigerResource, RDF.TYPE, carnivoreResource, false));
		
		connection.add(carnivoreResource, RDFS.SUBCLASSOF, predatorResource);
		assertTrue(connection.hasStatement(carnivoreResource, RDFS.SUBCLASSOF, predatorResource, false));
		
		connection.remove(carnivoreResource, RDFS.SUBCLASSOF, predatorResource);
		assertTrue(connection.hasStatement(tigerResource, RDF.TYPE, predatorResource, true));
	}
	
}
